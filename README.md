
# NEWSHORE-IBE-UX statics pages 
Workflow: gulp/nunjucks/jquery/sass
Tasks runner file: gulpfile.js

To run serve, at the root, run:
    `npm install` (just the first time)
    `gulp serve`


# Folders structure

# CORE
- assets
    - css
    - fonts
    - imgs
    - js
    - sass
- pages
- views
    - layouts
    - pages
    - partials
data.json

* At ´core/views/partials´ has the main core html Corporative and IBE files.
* These files are shared/used by all client's project. 
* Change at ´core/views/partials´ will affect all projects, so changes must be discussed with the team before start to develop it.



# Branches flow
master
    ´´ develop
    ´´ Tickets branches



´develop´ branch
-------------------------------
* Will be the main core branch, where all projects must be update. Project' branches must be updated by this: develop --> project branch
* To merge project branch --> develop, project branch must be stable and if there were core files changes, all projects webs must be checked.


# Documentation
https://newshore-product.atlassian.net/wiki/spaces/NSUX/pages/613711895/IBE+Frontend+Customization

# Heroku deploy (first steps, preparing environment)
1. Create Heroku Account
2. Download and install the Heroku Toolbelt
3. Run heroku login in your terminal or command prompt and fill in your Heroku credentials
4. Use heroku git:remote -a yourapp if you have "correct access issue"
5. Add repo to heroku `git:remote -a yourapp` ex `git remote add origin ssh://git@example.com:1234/myRepo.git`
5. More info in https://blog.teamtreehouse.com/deploy-static-site-he roku

# Heroku deploy (way of deploying)
1. Go to deploy branch.
2. Pull al changes from develop branch `git pull origin develop` command into console.
3. Compile all HTML and CSS files with `gulp compile-to-pro`.
4. Push all changes (compiled files) in deploy branch.
5. Run `git push --force heroku deploy:master` to deploy static files.
6. Go to http://newshore-ibe.herokuapp.com and login.
7. TIP #1: remember add "urlExtension": ".php", property into core/src/data.json file to add php auth logic.
8. TIP #2: if you whant to deploy from specific branch, use `git push --force heroku deploy:master` command.
9. TIP #3: Heroku server is case sentitive.

# PDF Generation
1. `gulp paranair-pdf-serve` , use paranair or specific project name.
2. Pul all HTML into this website: https://www.nrecosite.com/pdf_generator_net.aspx.
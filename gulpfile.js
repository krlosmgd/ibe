'use strict';

const gulp = require('gulp-param')(require('gulp'), process.argv);
const fs = require('fs');

// templates
const nunjucks = require('gulp-nunjucks-render'); // template engine
const plumber = require('gulp-plumber'); // continue server when nunjucks has errors
const data = require('gulp-data'); // appends dummy data to templates engine
// var mergeJSON = require("merge-json") ; // combine JSON files

// styles
const sass = require('gulp-sass'); // compiles SASS
const postcss = require('gulp-postcss'); // pipe CSS through several plugins, but parse CSS only once
const minifyCss = require('gulp-minify-css'); // minifies css
const autoprefixer = require('autoprefixer'); // add -webkit -ms, etc .. prefix to support old browsers
const sourcemaps = require('gulp-sourcemaps'); // generate .map file to debug sass on browser
const rename = require('gulp-rename'); // renames files
const replace = require('gulp-replace'); // rename inner documments
const toJSON = require('gulp-js-to-json');

// server
const browserSync = require('browser-sync').create();

// helpers
const clean = require('gulp-clean');

// put here all css pugins to compile
const postCssPlugins = [
    autoprefixer({
        grid: true, // add IE10 grid support
        browsers: ['>1%'], // automatic Can I Use API support
    })
];

// set all projects configutation
const projects = [
    {
        name: 'core', // set always at first project please !
        templates: {
            dist: 'core/dist',
            src: 'core/src/pages/**/*.+(html|njk)',
            data: './core/src/data.json',
            dataJS: './core/src/data.js',
            dataJSDest: './core/src/',
            componentsFolder: 'core/src/views',
            componentsSrc: 'core/src/views/**/*.+(html|njk)'
        },
        styles: {
            dist: 'core/src/assets/css/',
            src: 'core/src/assets/**/*.scss',
            mainFile: 'core/src/assets/sass/styles.scss',
            srcMD: 'core/src/assets/sass/_md/**/*.scss',
            mainFileMD: 'core/src/assets/sass/_md/md-styles.scss'

        }
    },
    {
        name: 'newshore',
        templates: {
            dist: 'airlines/Newshore/dist',
            src: 'airlines/Newshore/src/pages/**/*.+(html|njk)',
            srcViews: 'airlines/Newshore/src/views/**/*.+(html|njk)',
            data: './airlines/Newshore/src/data.json',
            dataJS: './airlines/Newshore/src/data.js',
            dataJSDest: './airlines/Newshore/src/',
            componentsFolder: 'airlines/Newshore/src/views',
            componentsSrc: 'airlines/Newshore/src/views/**/*.+(html|njk)'
        },
        styles: {
            dist: 'airlines/Newshore/src/assets/css/',
            src: 'airlines/Newshore/src/assets/**/*.scss',
            mainFile: 'airlines/Newshore/src/assets/sass/styles.scss',
            distPdf: 'airlines/Newshore/src/views/pdf/',
            srcPdf: 'airlines/Newshore/src/assets/pdf-sass/styles.scss',
            mainFilePdf: 'airlines/Newshore/src/assets/pdf-sass/styles.scss',
            srcMD: 'airlines/Newshore/src/assets/sass/_md/**/*.scss',
            mainFileMD: 'airlines/Newshore/src/assets/sass/_md/md-styles-theme.scss'
        }
    },
    {
        name: 'hk',
        templates: {
            dist: 'airlines/HKE/dist',
            src: 'airlines/HKE/src/pages/**/*.+(html|njk)',
            srcViews: 'airlines/HKE/src/views/**/*.+(html|njk)',
            data: './airlines/HKE/src/data.json',
            dataJS: './airlines/HKE/src/data.js',
            dataJSDest: './airlines/HKE/src/',
            componentsFolder: 'airlines/HKE/src/views',
            componentsSrc: 'airlines/HKE/src/views/**/*.+(html|njk)',
        },
        styles: {
            dist: 'airlines/HKE/src/assets/css/',
            src: 'airlines/HKE/src/assets/**/*.scss',
            mainFile: 'airlines/HKE/src/assets/sass/styles.scss',
            distPdf: 'airlines/HKE/src/views/pdf/',
            srcPdf: 'airlines/HKE/src/assets/pdf-sass/styles.scss',
            mainFilePdf: 'airlines/HKE/src/assets/pdf-sass/styles.scss',
            srcMD: 'airlines/HKE/src/assets/sass/_md/**/*.scss',
            mainFileMD: 'airlines/HKE/src/assets/sass/_md/md-styles-theme.scss',
        }
    },
    {
        name: 'paranair',
        templates: {
            dist: 'airlines/Paranair/dist',
            src: 'airlines/Paranair/src/pages/**/*.+(html|njk)',
            srcViews: 'airlines/Paranair/src/views/**/*.+(html|njk)',
            data: './airlines/Paranair/src/data.json',
            dataJS: './airlines/Paranair/src/data.js',
            dataJSDest: './airlines/Paranair/src/',
            componentsFolder: 'airlines/Paranair/src/views',
            componentsSrc: 'airlines/Paranair/src/views/**/*.+(html|njk)',
        },
        styles: {
            dist: 'airlines/Paranair/src/assets/css/',
            src: 'airlines/Paranair/src/assets/**/*.scss',
            mainFile: 'airlines/Paranair/src/assets/sass/styles.scss',
            distPdf: 'airlines/Paranair/src/views/pdf/',
            srcPdf: 'airlines/Paranair/src/assets/pdf-sass/**/*.scss',
            mainFilePdf: 'airlines/Paranair/src/assets/pdf-sass/styles.scss',
            srcMD: 'airlines/Paranair/src/assets/sass/_md/**/*.scss',
            mainFileMD: 'airlines/Paranair/src/assets/sass/_md/md-styles-theme.scss',
        }
    },
    {
        name: 'ceiba',
        templates: {
            dist: 'airlines/Ceiba/dist',
            src: 'airlines/Ceiba/src/pages/**/*.+(html|njk)',
            srcViews: 'airlines/Ceiba/src/views/**/*.+(html|njk)',
            data: './airlines/Ceiba/src/data.json',
            dataJS: './airlines/Ceiba/src/data.js',
            dataJSDest: './airlines/Ceiba/src/',
            componentsFolder: 'airlines/Ceiba/src/views',
            componentsSrc: 'airlines/Ceiba/src/views/**/*.+(html|njk)'
        },
        styles: {
            dist: 'airlines/Ceiba/src/assets/css/',
            src: 'airlines/Ceiba/src/assets/**/*.scss',
            mainFile: 'airlines/Ceiba/src/assets/sass/styles.scss',
            distPdf: 'airlines/Ceiba/src/views/pdf/',
            srcPdf: 'airlines/Ceiba/src/assets/pdf-sass/styles.scss',
            mainFilePdf: 'airlines/Ceiba/src/assets/pdf-sass/styles.scss',
            srcMD: 'airlines/Ceiba/src/assets/sass/_md/**/*.scss',
            mainFileMD: 'airlines/Ceiba/src/assets/sass/_md/md-styles-theme.scss'
        }
    },
    {
        name: 'pobeda',
        templates: {
            dist: 'airlines/Pobeda/dist',
            src: 'airlines/Pobeda/src/pages/**/*.+(html|njk)',
            srcViews: 'airlines/Pobeda/src/views/**/*.+(html|njk)',
            data: './airlines/Pobeda/src/data.json',
            dataJS: './airlines/Pobeda/src/data.js',
            dataJSDest: './airlines/Pobeda/src/',
            componentsFolder: 'airlines/Pobeda/src/views',
            componentsSrc: 'airlines/Pobeda/src/views/**/*.+(html|njk)'
        },
        styles: {
            dist: 'airlines/Pobeda/src/assets/css/',
            src: 'airlines/Pobeda/src/assets/**/*.scss',
            mainFile: 'airlines/Pobeda/src/assets/sass/styles.scss',
            distPdf: 'airlines/Pobeda/src/views/pdf/',
            srcPdf: 'airlines/Pobeda/src/assets/pdf-sass/styles.scss',
            mainFilePdf: 'airlines/Pobeda/src/assets/pdf-sass/styles.scss',
            srcMD: 'airlines/Pobeda/src/assets/sass/_md/**/*.scss',
            mainFileMD: 'airlines/Pobeda/src/assets/sass/_md/md-styles-theme.scss'
        }
    },
    {
        name: 'jambojet',
        templates: {
            dist: 'airlines/Jambojet/dist',
            src: 'airlines/Jambojet/src/pages/**/*.+(html|njk)',
            srcViews: 'airlines/Jambojet/src/views/**/*.+(html|njk)',
            data: './airlines/Jambojet/src/data.json',
            dataJS: './airlines/Jambojet/src/data.js',
            dataJSDest: './airlines/Jambojet/src/',
            componentsFolder: 'airlines/Jambojet/src/views',
            componentsSrc: 'airlines/Jambojet/src/views/**/*.+(html|njk)',
        },
        styles: {
            dist: 'airlines/Jambojet/src/assets/css/',
            src: 'airlines/Jambojet/src/assets/**/*.scss',
            mainFile: 'airlines/Jambojet/src/assets/sass/styles.scss',
            distPdf: 'airlines/Jambojet/src/views/pdf/',
            srcPdf: 'airlines/Jambojet/src/assets/pdf-sass/styles.scss',
            mainFilePdf: 'airlines/Jambojet/src/assets/pdf-sass/styles.scss',
            srcMD: 'airlines/Jambojet/src/assets/sass/_md/**/*.scss',
            mainFileMD: 'airlines/Jambojet/src/assets/sass/_md/md-styles-theme.scss'
        }
    },
    {
        name: 'evelop',
        templates: {
            dist: 'airlines/Evelop/dist',
            src: 'airlines/Evelop/src/pages/**/*.+(html|njk)',
            srcViews: 'airlines/Evelop/src/views/**/*.+(html|njk)',
            data: './airlines/Evelop/src/data.json',
            dataJS: './airlines/Evelop/src/data.js',
            dataJSDest: './airlines/Evelop/src/',
            componentsFolder: 'airlines/Evelop/src/views',
            componentsSrc: 'airlines/Evelop/src/views/**/*.+(html|njk)',
        },
        styles: {
            dist: 'airlines/Evelop/src/assets/css/',
            src: 'airlines/Evelop/src/assets/**/*.scss',
            mainFile: 'airlines/Evelop/src/assets/sass/styles.scss',
            distPdf: 'airlines/Evelop/src/views/pdf/',
            srcPdf: 'airlines/Evelop/src/assets/pdf-sass/styles.scss',
            mainFilePdf: 'airlines/Evelop/src/assets/pdf-sass/styles.scss',
            srcMD: 'airlines/Evelop/src/assets/sass/_md/**/*.scss',
            mainFileMD: 'airlines/Evelop/src/assets/sass/_md/md-styles-theme.scss'
        }
    }
    // {
    //     name: 'sky',
    //     templates: {
    //         dist: 'airlines/SKY/dist',
    //         src: 'airlines/SKY/src/pages/**/*.+(html|njk)',
    //         srcViews: 'airlines/SKY/src/views/**/*.+(html|njk)',
    //         data: './airlines/SKY/src/data.json',
    //         dataJS: './airlines/SKY/src/data.js',
    //         dataJSDest: './airlines/SKY/src/',
    //         componentsFolder: 'airlines/SKY/src/views',
    //         componentsSrc: 'airlines/SKY/src/views/**/*.+(html|njk)',
    //     },
    //     styles: {
    //         dist: 'airlines/SKY/src/assets/css/',
    //         src: 'airlines/SKY/src/assets/**/*.scss',
    //         mainFile: 'airlines/SKY/src/assets/sass/styles.scss',
    //         distPdf: 'airlines/SKY/src/views/pdf/',
    //         srcPdf: 'airlines/SKY/src/assets/pdf-sass/styles.scss',
    //         mainFilePdf: 'airlines/SKY/src/assets/pdf-sass/styles.scss',
    //         srcMD: 'airlines/SKY/src/assets/sass/_md/**/*.scss',
    //         mainFileMD: 'airlines/SKY/src/assets/sass/_md/md-styles-theme.scss',
    //     }
    // }
];

// loop each project config
projects.forEach((project, index) => {
    // compile json files from js data
    gulp.task(`${project.name}-js2json`, function () {
        return gulp.src(project.templates.dataJS, {
                read: false
            })
            .pipe(toJSON())
            .pipe(gulp.dest(project.templates.dataJSDest));
    });

    // compile css styles from sass files
    gulp.task(`${project.name}-sass`, function () {
        return gulp.src(project.styles.mainFile)
            .pipe(sourcemaps.init())
            .pipe(sass({
                style: 'nested',
            }).on('error', sass.logError))
            .pipe(postcss(postCssPlugins))
            .pipe(gulp.dest(project.styles.dist))
            .pipe(minifyCss())
            .pipe(rename({
                basename: 'styles',
                extname: '.min.css'
            }))
            .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest(project.styles.dist))
            .pipe(browserSync.stream()) // auto-inject into browsers
    });

    // compile _md css styles from sass files
    gulp.task(`${project.name}-sass-md`, function () {
        return gulp.src(project.styles.mainFileMD)
            .pipe(sourcemaps.init())
            .pipe(sass({
                style: 'nested',
            }).on('error', sass.logError))
            .pipe(postcss(postCssPlugins))
            .pipe(minifyCss())
            .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest(project.styles.dist))
            .pipe(browserSync.stream()) // auto-inject into browsers
    });

    // compile html static pages form nunjucks files
    gulp.task(`${project.name}-render`, function () {
        return gulp.src(project.templates.src) // Gets .html and .njk files in pages
            .pipe(plumber()) // continue server when nunjucks has errors
            .pipe(data(() => {
                if (index === 0) {
                    return JSON.parse(fs.readFileSync(project.templates.data))
                } else {
                    return mergeJSON(
                        JSON.parse(fs.readFileSync(projects[0].templates.data)), // hot reload JSON projects data
                        JSON.parse(fs.readFileSync(project.templates.data))
                    )
                }
            }))
            .pipe(nunjucks({
                path: [ // Renders templates / components from core and project with nunjucks
                    project.templates.componentsFolder,
                    projects[0].templates.componentsFolder,
                ]
            }))
            .pipe(gulp.dest(project.templates.dist)) // Output files in app folder
            .pipe(browserSync.stream()) // auto-inject into browsers to hot reload
    });

    // compile php static pages form nunjucks files
    gulp.task(`${project.name}-render-to-pro`, function () {
        return gulp.src(project.templates.src) // Gets .html and .njk files in pages
            .pipe(plumber()) // continue server when nunjucks has errors
            .pipe(data(() => mergeJSON(
                require(projects[0].templates.data), // file to rewrite
                require(project.templates.data), // main data file
            )))
            .pipe(nunjucks({
                path: [ // Renders templates / components from core and project with nunjucks
                    project.templates.componentsFolder,
                    projects[0].templates.componentsFolder,
                ]
            }))
            .pipe(rename({
                extname: '.php'
            })) // rename al .html files to .php files
            .pipe(replace('.html', '.php')) // refactor all '.html' text to '.php'
            .pipe(gulp.dest(project.templates.dist)) // Output files in app folder
            .pipe(browserSync.stream()) // auto-inject into browsers to hot reload
    });

    // launch DEV server
    gulp.task(`${project.name}-serve`, [
        `${project.name}-js2json`,
        `${project.name}-sass`,
        `${projects[0].name}-sass-md`,
        `${project.name}-sass-md`,
        `${project.name}-render`,
    ], function () {
        browserSync.init({
            startPath: project.templates.dist,
            server: {
                baseDir: '.'
            },
            // plugins: [
            //     {
            //         module: "bs-html-injector",
            //         options: {
            //             files: [project.templates.dist]
            //         }
            //     }
            // ]
        });
        gulp.watch(project.templates.dataJS, [`${project.name}-js2json`, `${project.name}-render`]);
        gulp.watch(project.templates.src, [`${project.name}-render`]);
        gulp.watch(project.templates.componentsSrc, [`${project.name}-render`]);
        gulp.watch(project.styles.src, [`${project.name}-sass`]);
        gulp.watch(project.styles.srcMD, [`${project.name}-sass-md`]);
        gulp.watch(projects[0].templates.dataJS, [`${project.name}-render`]); // core base components
        gulp.watch(projects[0].templates.componentsSrc, [`${project.name}-render`]); // core base components
        gulp.watch(projects[0].styles.src, [`${project.name}-sass`]);
        gulp.watch(projects[0].styles.srcMD, [`${project.name}-sass-md`]);
    });
});

// Launch all projects on DEV server
let tasks = []; // all task
let templateTasks = []; // all templeta engine tasks
let projectStyleTasks = []; // all projects style task (no core, no md styles)
projects.forEach((project, index) => {
    tasks.push(`${project.name}-js2json`);
    tasks.push(`${project.name}-sass`);
    tasks.push(`${project.name}-sass-md`);
    tasks.push(`${project.name}-render`);
    templateTasks.push(`${project.name}-render`);
    if (index > 0) {
        projectStyleTasks.push(`${project.name}-sass`);
    }
});

gulp.task('serve', tasks, function () {
    browserSync.init({
        startPath: projects[0].templates.dist,
        server: {
            baseDir: '.'
        }
    });
    projects.forEach((project, index) => {
        gulp.watch(project.templates.dataJS, [`${project.name}-js2json`]);
        gulp.watch(project.templates.data, [`${project.name}-render`]);
        gulp.watch(project.templates.src, [`${project.name}-render`]);
        gulp.watch(project.templates.componentsSrc, [`${project.name}-render`]);
        gulp.watch(project.styles.src, [`${project.name}-sass`]);
        gulp.watch(project.styles.srcMD, [`${project.name}-sass-md`]);
        if (index !== 0) {
            gulp.watch(projects[0].templates.dataJS, [`${project.name}-js2json`]);
            gulp.watch(projects[0].templates.data, [`${project.name}-render`]);
            gulp.watch(projects[0].templates.src, [`${project.name}-render`]);
            gulp.watch(projects[0].templates.componentsSrc, [`${project.name}-render`]);
            gulp.watch(projects[0].styles.src, [`${project.name}-sass`]);
            gulp.watch(projects[0].styles.srcMD, [`${project.name}-sass-md`]);
        }
    });
});

// ---------------------------------------
// to compile php in production in
// ---------------------------------------

let compileAllTasks = []; // all task
let compileAllTemplateTasks = []; // all templeta engine tasks
let compileAllProjectStyleTasks = []; // all projects style task (no core, no md styles)
projects.forEach((project, index) => {
    compileAllTasks.push(`${project.name}-js2json`);
    compileAllTasks.push(`${project.name}-sass`);
    compileAllTasks.push(`${project.name}-sass-md`);
    compileAllTasks.push(`${project.name}-render-to-pro`);
    compileAllTemplateTasks.push(`${project.name}-render-to-pro`);
    if (index > 0) {
        compileAllProjectStyleTasks.push(`${project.name}-sass`);
    }
});

gulp.task('compile-to-pro', compileAllTasks, function () {
    browserSync.init({
        startPath: projects[0].templates.dist,
        server: {
            baseDir: '.'
        }
    });
    projects.forEach((project, index) => {
        gulp.watch(project.templates.src, [`${project.name}-render-to-pro`]);
        gulp.watch(project.templates.componentsSrc, [`${project.name}-render-to-pro`]);
        gulp.watch(project.styles.src, [`${project.name}-sass`]);
        gulp.watch(project.styles.srcMD, [`${project.name}-sass-md`]);
        if (index === 0) {
            gulp.watch(project.templates.componentsSrc, compileAllTemplateTasks);
            gulp.watch(project.styles.src, compileAllProjectStyleTasks);
        }
    });
});

// -------------------------
// PDF GENERATOR
// -------------------------

projects.forEach(project => {
    // compile css styles from sass pdf files
    gulp.task(`${project.name}-sass-pdf`, function () {
        return gulp.src(project.styles.mainFilePdf)
            .pipe(sourcemaps.init())
            .pipe(sass({
                style: 'nested',
            }).on('error', sass.logError))
            .pipe(postcss(postCssPlugins))
            .pipe(minifyCss())
            .pipe(rename({
                basename: 'styles-pdf',
                extname: '.min.css'
            }))
            .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest(project.styles.distPdf))
            .pipe(browserSync.stream()) // auto-inject into browsers
    });

    // compile html static pages form nunjucks files
    gulp.task(`${project.name}-render-pdf`, function () {
        return gulp.src(project.templates.src) // Gets .html and .njk files in pages
            .pipe(plumber()) // continue server when nunjucks has errors
            .pipe(data(() => mergeJSON( // combine two JSON files ovewriting
                require(projects[0].templates.data), // file to rewrite
                require(project.templates.data), // main data file
            )))
            .pipe(nunjucks({
                path: [ // Renders templates / components from core and project with nunjucks
                    project.templates.componentsSrc,
                    projects[0].templates.componentsFolder,
                ]
            }))
            .pipe(gulp.dest(project.templates.dist)) // Output files in app folder
            .pipe(browserSync.stream()) // auto-inject into browsers to hot reload
    });

    // launch DEV server
    gulp.task(`${project.name}-serve-pdf`, [
        `${project.name}-js2json`,
        `${project.name}-sass-pdf`,
        `${project.name}-render-pdf`
    ], function () {
        browserSync.init({
            startPath: project.templates.dist,
            server: {
                baseDir: '.'
            }
        });
        // template
        gulp.watch(project.templates.srcViews, [`${project.name}-render-pdf`]);
        gulp.watch(project.templates.src, [`${project.name}-render-pdf`]);
        gulp.watch(projects[0].templates.componentsSrc, [`${project.name}-render`]); // core base layouts and components
        // styles
        gulp.watch(project.styles.src, [`${project.name}-sass`]);
        gulp.watch(project.styles.srcPdf, [`${project.name}-sass-pdf`]);
        gulp.watch(projects[0].styles.src, [`${project.name}-sass-pdf`]); // core base styles
    });
});

/* FUNCTIONS */
var jsonC = {}.constructor;

var isJSON = function (json) {
    if (json && json.constructor === jsonC) {
        return true;
    } else {
        return false;
    }
}

var mergeJSON = function (json1, json2) {
    var result = null;
    if (isJSON(json2)) {
        result = {};
        if (isJSON(json1)) {
            for (var key in json1) {
                result[key] = json1[key];
            }
        }
        for (var key in json2) {
            if (typeof result[key] === "object" && typeof json2 === "object") {
                result[key] = mergeJSON(result[key], json2[key]);
            } else {
                result[key] = json2[key];
            }
        }
    } else {
        result = json2;
    }

    return result;
}

// else if(Array.isArray(json1) && Array.isArray(json2)){
//     result = json1 ;

//     // for(var i = 0; i < json2.length; i++){
//     // 	if(result.indexOf(json2[i]) === -1){
//     // 		result[result.length] = json2[i] ;
//     // 	}
//     // }

//     for(var i = 0; i < json2.length; i++){
//         // if(result.indexOf(json2[i]) === -1){
//         // 	result[result.length] = json2[i] ;
//         // }

//         if (typeof result[i] === "object" && typeof json2[i] === "object"){
//             result[i] = mergeJSON(result[i], json2[i]);
//         } else {
//             result[i] = json2[i];
//         }
//     }
// }
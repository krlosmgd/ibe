<?php
session_start();
if (isset($_POST['username']) && isset($_POST['password'])) {
    if ($_POST['username'] == 'newshore' && $_POST['password'] == 'ibenewshore671ux') {
        $_SESSION['user'] = 'Newshore';
        header('Location: core/dist/');
    }
    if ($_POST['username'] == 'hke' && $_POST['password'] == '1asia2HK3Express') {
        $_SESSION['user'] = 'HKE';
        header('Location: airlines/HKE/dist/');
    }
    if ($_POST['username'] == 'paranair' && $_POST['password'] == '65paranair32') {
        $_SESSION['user'] = 'Paranair';
        header('Location: airlines/Paranair/dist/');
    }
    if ($_POST['username'] == 'ceiba' && $_POST['password'] == '1ecuatorialceiba2') {
        $_SESSION['user'] = 'Ceiba';
        header('Location: airlines/Ceiba/dist/');
    }
    if ($_POST['username'] == 'pobeda' && $_POST['password'] == '1bluepobeda') {
        $_SESSION['user'] = 'Pobeda';
        header('Location: airlines/Pobeda/dist/');
    }
    if ($_POST['username'] == 'jambojet' && $_POST['password'] == '4jambo77jet') {
        $_SESSION['user'] = 'Jambojet';
        header('Location: airlines/Jambojet/dist/');
    }
    if ($_POST['username'] == 'sky' && $_POST['password'] == '98skyproject') {
        $_SESSION['user'] = 'Sky';
        header('Location: airlines/SKY/dist/');
    }
}
?>
<html lang="en" class=" flexbox objectfit object-fit no-touchevents" data-whatinput="mouse" data-whatintent="mouse">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0"-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="">
    <meta name="author" content="">

    <!-- <meta name="msapplication-TileColor" content="#0037a2"> -->
    <meta name="theme-color" content="#ffffff">

    <!-- <link rel="icon" type="image/png" href="../src/assets/imgs/favicons/favicon-16x16.png"> -->
    <title>Newshore | Frontend CSS</title>

    <!-- @start: PROJECT CSS -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700&amp;amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="./core/src/assets/css/styles.min.css">
    <!-- @end: PROJECT CSS -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <!-- @start: CSS EXTERNAL -->

    <!-- @END: CSS EXTERNAL -->
</head>

<body class="home-page">
    <a href="#maincontent" class="hide-visually">Skip to main content</a>

    <div class="page-wrap">
        <main id="maincontent">
            <div class="content-wrap" data-target="contentWrap">
                <div class="container-wrap">
                    <div class="grid">
                        <div class="grid col-12">
                            <form class="login-form" action="login.php" method="POST">
                                <h1 class="login-form_brand">
                                    <a class="logo">
                                        <img src="./core/src/assets/imgs/logo.svg" alt="Paranair">
                                    </a>
                                </h1>

                                <div class="ui-form_group">
                                    <div class="ui-input-container has-placeholder">
                                        <label class="ui-label" for="username">
                                            Username
                                        </label>
                                        <div class="ui-input_wrap">
                                            <input type="text" class="ui-input" name="username" id="username"
                                                aria-invalid="false">
                                        </div>
                                    </div>
                                </div>
                                <div class="ui-form_group">
                                    <div class="ui-input-container has-placeholder">
                                        <label class="ui-label" for="password">
                                            Password
                                        </label>
                                        <div class="ui-input_wrap">
                                            <input type="password" class="ui-input" name="password" id="password"
                                                aria-invalid="false">
                                        </div>
                                    </div>
                                </div>
                                <div class="ui-form_group">
                                    <input class="login-form_button button" type="submit" value="login">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ui-overlay"></div>
        </main>
    </div>

    <script src="./core/src/assets/js/libs/modernizr-custom.js"></script>
    <script src="./core/src/assets/js/libs/jquery-3.3.1.min.js"></script>
    <script src="./core/src/assets/js/common.scripts.js"></script>
</body>

</html>
---------------------------------
# UX Frontend documentation
---------------------------------

## Flight Search

---------------------------------

CORE (assets/sass/components/IBE/flight-search)

### _flight-search.scss
The base scss for the component. Has all imported files: variables and mixins.
This is called directly from project's _import.scss

### _flight-search-vars.scss
The base component variables. When starting a new project, this files should be copied to the project folder.
This file must have all posible variables of the component.

### 





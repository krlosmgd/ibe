---------------------------------
# UX Frontend documentation
---------------------------------

## NG Datepicker
https://ng-bootstrap.github.io/#/components/datepicker/overview 

---------------------------------

CORE folder: assets/sass/ui-elements/datepicker

### _ng-datepicker.scss
The base scss for the component. Has all imported files: variables and mixins.
This is called directly from project's _import.scss

### _ng-datepicker-vars.scss
The base component variables. When starting a new project, this files should be copied to the project folder.
This file must have all posible variables of the component.

### 





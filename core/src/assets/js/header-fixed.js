
/**
 * 
 * @param {number} [stickyMainHeader] position in window of element
 */
function headerFixed(stickyMainHeader) {
    var body = $("body");
    if (body.hasClass('fixed-header')) {
        if ($(window).scrollTop() > 0) {
            body.addClass('is-fixedheader');
        } else {
            body.removeClass('is-fixedheader');
        }
    }
}

/**
 * 
 * @param {number} [stickyMainHeader] // measure to add class is-fixedheader
 */
function mainHeaderSearchFixed(stickyMainHeader) {

    if (stickyMainHeader > 0) {
        stickyCombinedBar = searchHeaderFixed(stickyMainHeader); // only if the function is active for add class fixed in header
    } else {
        stickyCombinedBar = searchHeaderFixed(); // calculate scroll top, search fixed
    }
    return stickyCombinedBar;
}

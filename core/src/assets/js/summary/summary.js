// variables
var body = $('body');
var summaryIsOpen = false;
var summary = {
	trigger: $('.summary_trigger'),
	closeButton: $('.summary_collapse_header .close'),
	summary: $('.summary'),
	header: $('.summary_header'),
	notification: $('[data-target="#notification"]'),
	summaryCollapse: '.summary_collapse',
	summaryNotification: $('.summary_notification'),
	notificationText: $('.summary_notification_text'),
};

/**
 *
 * @param {number} [timeout = 600] timeout class animating
 *
 */
function summaryCollapse(timeout) {
	timeout = timeout || 1000;
	summary.trigger.click(function(event) {
		if (!summaryIsOpen) {
			$(this).attr('aria-expanded', true);
			body.removeClass('search-opened');
			body.addClass('summary-opened overlay-in');
			var summaryCollapse = $(this)
				.closest(summary.header)
				.siblings(summary.summaryCollapse);
			summaryCollapse.addClass('animating');
			// remove class animating of summary_collapse, present for 1seg
			setTimeout(function() {
				summaryCollapse.removeClass('animating');
			}, timeout);

			summaryIsOpen = true;
		} else {
			summary.close();
			summaryIsOpen = false;
		}
	});
	summary.closeButton.click(function() {
		summary.close();
	});
	$('.summary_close_btn').click(function() {
		summary.close();
	});
	summary.close = function() {
		summary.trigger.attr('aria-expanded', false);
		body.removeClass('summary-opened overlay-in');
		summaryIsOpen = false;
	};
}

function overlaySummaryClose() {
	$('.ui-overlay').click(function() {
		if ($('.summary-opened.overlay-in').length) {
			// validate is summary opened
			summary.trigger.attr('aria-expanded', false);
			body.removeClass('summary-opened overlay-in');
			summaryIsOpen = false;
		}
	});
}

/**
 *
 * @param {number} [timeout = 2000] timeout remove class show-notification
 * @param {number} [timeoutNoti = 400] timeout show class show-notification
 */
var notiSetTimeOutAddClass, notiSetTimeOutRemoveClass, notiSetTimeOutHide;
function notification(timeout, timeoutNoti) {
	timeout = timeout | 2000;
	timeoutNoti = timeoutNoti | 400;
	summary.summaryNotification.hide(); // hide div notification
	summary.notification.click(function() {

		clearTimeout(notiSetTimeOutAddClass); 
		clearTimeout(notiSetTimeOutRemoveClass); 
		clearTimeout(notiSetTimeOutHide);
		
		if ($(this).attr('class') == 'tripselector_modifier_button') {
			handleShowNotification(timeout, timeoutNoti);
			summary.notificationText.text('Removido del carrito');
		} else {
			handleShowNotification(timeout, timeoutNoti);
			summary.notificationText.text('Añadido al carro');
		}
	});
}

function handleShowNotification(timeout, timeoutNoti) {
	summary.summaryNotification.show();
	notiSetTimeOutAddClass = setTimeout(function() {
		summary.summary.addClass('show-notification');
	}, timeoutNoti);

	notiSetTimeOutRemoveClass = setTimeout(function() {
		summary.summary.removeClass('show-notification');
	}, timeout);

	notiSetTimeOutHide = setTimeout(function() {
		summary.summaryNotification.hide();
	}, timeout + 500);
}

$(document).ready(function () {

    $('pre code').each(function(i, block) {
        hljs.highlightBlock(block);
    });

    //## Open static html menu
    var menuIsOpen = false;
    var demoMenuTrigger = $('[data-action=openDemoMenu]');
    var demoMenu = $('[data-target=demoMenu]');
    demoMenuTrigger.on('click',function(){
        $('body').toggleClass('opened-menu');
    });
});

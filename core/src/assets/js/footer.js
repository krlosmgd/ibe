//## keep footer at bootom page calculating its height
function footerAtBottom() {
    var footer = $('[data-target="footer"]');
    var footerHeight = footer.outerHeight();
    var contentWrapper = $('[data-target="contentWrap"]');
    contentWrapper.css('padding-bottom', footerHeight);
}
$(document).ready(function () {
    // ------------------------------------------------
    // Accessible Tabs
    // ------------------------------------------------

    // For reference
    var keys = {
        end: 35,
        home: 36,
        left: 37,
        up: 38,
        right: 39,
        down: 40,
        delete: 46
    };

    var tabsList = $('[role="tablist"]');
    var tabs = $('[role="tab"]');
    var tabPanels = $('[role="tabpanel"]');

    tabs.on('click', function() {
        var thisTab = $(this),
            thisTabPanelId = thisTab.attr('aria-controls'),
            thisTabPanel = $('#' + thisTabPanelId),
            thisTabSiblings = $(this).siblings(),
            thisTabContainer = $(this).parent(),
            thisTabPanelSiblings = thisTabContainer.next().children('[role="tabpanel"]');

        // Unselect all the tabs
        thisTabSiblings.attr('aria-selected', 'false').removeClass('active');

        // Select this tab
        thisTab.attr('aria-selected', 'true').addClass('active');

        // Hide all the tab panels
        thisTabPanelSiblings.removeClass('show');

        // Show this tab panel
        thisTabPanel.addClass('show');
    });

    // Add keys to click events
    //## left/right to navigate between tabs
    //## enter to show tab content
    tabs.on('keydown', function(e) {
        var thisTab = $(this);

        // 'left/right' arrow to navigate between tabs
        if (e.which == 37) {
            var prevTab = thisTab.prev();
            thisTab.attr('tabindex', '-1');
            prevTab.focus();
            prevTab.removeAttr('tabindex');
        }
        else if (e.which == 39) {
            var nextTab = thisTab.next();
            thisTab.attr('tabindex', '-1');
            nextTab.focus();
            nextTab.removeAttr('tabindex');
        }

        // 'enter' to show tab content
        if(e.which == 13) {
          thisTab.click();
        }
    });
});

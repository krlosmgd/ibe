$(document).ready(function () {
    // ------------------------------------------------
    // Accessible dropdown lists
    // ------------------------------------------------
    var dropdownOpened = false;
    var currentSelection = 0;
    $(window).click(function(e) {
        // When clicking outside closes all dropdowns
        $('.ui-dropdown [role="listbox"]').attr('aria-hidden','true');
        $('.ui-dropdown [aria-expanded]').attr('aria-expanded','false');
        dropdownOpened = false;
    });
    $('.ui-dropdown [aria-expanded]').on('keyup click', function(e) {
        if (e.keyCode === 9) {
            // Open dropdown list
            $(this).next().attr('aria-hidden','false');
            $(this).attr('aria-expanded','true');
            dropdownOpened = true;
        }
        // If clicking
        if (e.type === 'click') {
            // If dropdown list is already open
            if ($(this).attr('aria-expanded') == 'true') {
                // close it
                $(this).next().attr('aria-hidden','true');
                $(this).attr('aria-expanded','false');
                dropdownOpened = false;
            // If dropdown list is closed
            } else {
                // close all others
                $('.ui-dropdown [role="listbox"]').attr('aria-hidden','true');
                $('.ui-dropdown [aria-expanded]').not(this).attr('aria-expanded','false');
                // open this one
                $(this).next().attr('aria-hidden','false');
                $(this).attr('aria-expanded','true');
                dropdownOpened = true;
            }
            return false;
        } // end click event
    });

    // If key is pressed while on the last link in a dropdown list
    $('.ui-dropdown [role="listbox"] li:last-child a').on('keydown', function(e) {
        // If tabbing out of the last link in a dropdown list
        // AND NOT tabbing into another dropdown list
        if ((e.keyCode == 9 || e.keyCode == 40) && $(this).parent('li').next().length == 0) {
            // Close this dropdown list
            $(this).parent('li').parent('ul').attr('aria-hidden','true');
            $(this).parent('li').parent('ul').prev().attr('aria-expanded','false');
            dropdownOpened = false;
        }
    });
    // ************************************************
});

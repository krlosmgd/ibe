// variables
var W = $(window).outerWidth();
var body = $("body");
var searchFlight = {
    combinedSummary: $(".combined_summary_wrapper"),
    multiplePanel: $(".multiple-panel_content"),
    mainHeader: $(".main-header"),
    class: "search-fixed",
    searchOpen: '.search_tt.show'
};

/**
 * 
 * @param {number} [headerHeight] when the header is fixed in home
 */
function searchHeaderFixed(headerHeight) {
	headerHeight = headerHeight || searchFlight.mainHeader.height();
	body.is('.home-page.fixed-header') ? (headerHeight = headerHeight) : (headerHeight = 0);
	var stickyCombinedBar = 0;
	if (searchFlight.combinedSummary.length > 0) {
		stickyCombinedBar = searchFlight.combinedSummary.offset().top;
	} else if (searchFlight.multiplePanel.length > 0) {
        stickyCombinedBar = searchFlight.multiplePanel.offset().top - headerHeight;
	}
	return stickyCombinedBar;
}

/**
 * 
 * @param {number} [stickyCombinedBar] position in window of element
 */
function searchFixed(stickyCombinedBar) {
    if (!body.is('.ibe-page.fixed-header') && stickyCombinedBar != 0) {
        if ($(window).scrollTop() > stickyCombinedBar) {
            body.addClass(searchFlight.class);
        } else {
            body.removeClass(searchFlight.class);
        }
    }
}

// ----------------------------------------------------
//## MAIN SEARCH 
// ----------------------------------------------------

/**
 * Simulate search flight behavior
 * @param {boolean} emitOverlay 
 * 
 * @param {number} [windowsWidthForSearch] // width of the window -> execute function when focus search with scroll just on small devices
 * 
 * @param {number} [limitScrollTopSearch] // limit scroll top search -> when the options are opened
 */
function searchInteraction(emitOverlay, windowsWidthForSearch, limitScrollTopSearch) {
    emitOverlay = emitOverlay || false;
    windowsWidthForSearch = windowsWidthForSearch | 640;
    limitScrollTopSearch = limitScrollTopSearch | -220;
    var searchRouteTrigger = $('.route .ui-input-multiline_group .ui-input');
    var searchDateTrigger = $('.dates .ui-input-multiline_group .ui-input');
    // var searchRouteTriggerButton = $('.route .ui-input-multiline_group button.ui-input');
    // var searchDateTriggerButton = $('.dates .ui-input-multiline_group button.ui-input');
    var searchContainer = $('[data-target="searchContainer"]');
    var searchOptionsList = $('.search_tt');
    var searchOptionLink = '.search_tt_menu li a';
    var calendarDay = '.datepicker .custom-day';

    var searchTypologyTrigger = $('.typology .ui-input');

    var dataSearch = {
        isActive: true
    }

    // hide selected buttons to show inputs
    $('.ui-input-multiline button.ui-input').hide();

    // events
    var currentClickedIndex;
    $('.searchflight .ui-input').on('click', function () {
        // set data
        dataSearch.isActive = true;

        // close others dropdowns
        var clickedIndex = $('.searchflight .ui-input').index(this);
        if (currentClickedIndex !== clickedIndex) {
            searchOptionsList.removeClass('show');
            currentClickedIndex = clickedIndex;
        }

        // set clases
        $('body').addClass('search-opened');

        // simulate overlay when searchflight is active
        if (emitOverlay) {
            // $('body').addClass('overlay-in');
            $('.ui-overlay, .search_tt_close-btn').click(function () {
                if (dataSearch.isActive) {
                    dataSearch.isActive = false;
                    $(' #searchOutbound_options, #searchReturn_options, #searchDatesOutbound_options, #typologyList, #paxTypologyID').collapse('hide')
                    $('body').removeClass('search-opened');
                }
                // remove is-focused states in search flight
                $('.searchflight .ui-input-multiline_group').removeClass('is-focused');
            });
        } else { // 
            // [TODO] add next input step when selec values
        }

        // focus search with scroll just on small devices
        if (W <= windowsWidthForSearch) {
            setScrollTo(searchFlight.multiplePanel);
        } else {
            // After Stuff
            var scrollTop = $(window).scrollTop(),
                elementOffset = searchFlight.multiplePanel.offset().top,
                distance = (elementOffset - scrollTop);

            var headerHeight = searchFlight.mainHeader.outerHeight();

            if (distance > headerHeight) {
                if ($(this).parents().find('.ui-input-container.is-focused').parent('.dates').attr('class') == 'dates') {
                    if (!body.hasClass('search-fixed')) {
                        setScrollTo(searchFlight.multiplePanel);
                    }
                } else {
                    setTimeout(function () {
                        var validateElem = validateElementInWindow(searchFlight.searchOpen);
                        var searchItemCollapse = $(searchFlight.searchOpen).outerHeight() + 78;
                        var windowHeight = $(window).height() - headerHeight;

                        if (!(validateElem[0] > validateElem[1])) { //if it is false, it is not in the windows is limit
                            if (searchItemCollapse >= windowHeight) {
                                setScrollTo(searchFlight.multiplePanel);
                            } else {
                                setScrollTo(searchFlight.multiplePanel, limitScrollTopSearch);
                            }
                        }
                    }, 500);
                }
            }

        }
    });

    // route
    searchRouteTrigger.on('click', function () {
        var OpenedRoute = false;
        var searchListId = $(this).data('target');
        var searchListTarget = $(searchListId);

        searchTypologyTrigger.attr('aria-expanded', false);

        // check if options list is already opened
        if (searchListTarget.is(':visible')) {
            searchOptionsList.not(searchListId).removeClass('show');
            $('.ui-input-multiline_group').removeClass('is-focused');
            searchContainer.removeClass('routelist-open');
            var OpenedRoute = false;
        } else {
            searchOptionsList.not(searchListId).removeClass('show');
            $('.ui-input-multiline_group').removeClass('is-focused');
            $(this).closest('.ui-input-multiline_group').addClass('is-focused');
            searchContainer.addClass('routelist-open');
            var OpenedRoute = true;
        }

        // reset ui-input default focus behaviour
        if ($(this).is(':focus')) {
            var inputContainer = $(this).closest('.ui-input-container');
            inputContainer.removeClass('is-focused');
        }
    });

    // date
    searchDateTrigger.on('click', function () {
        var OpenedDate = false;
        var searchListId = $(this).data('target');
        var searchListTarget = $(searchListId);

        searchTypologyTrigger.attr('aria-expanded', false);

        // check if options list is already opened
        if (searchListTarget.is(':visible')) {
            searchContainer.removeClass('datelist-open');
            $('.ui-input-multiline_group').removeClass('is-focused');
            var OpenedDate = false;
        } else {
            searchContainer.addClass('datelist-open');
            $('.ui-input-multiline_group').removeClass('is-focused');
            $(this).closest('.ui-input-multiline_group').addClass('is-focused');
            var OpenedDate = true;
        }
        // var searchGroup = $(this).closest('.ui-input-multiline_group').attr('data-target');
        // var searchInput = $('[data-target="' + searchGroup + '"]').find('input');
        // searchInput.closest('.ui-input-multiline_group').toggleClass('is-focused');
    });

    //## selecting and option from the list
    $(searchOptionLink + ',' + calendarDay).on('click', function () {
        var searchOptionParentId = $(this).parents('.search_tt').attr('id');
        var searchGroupId = searchOptionParentId.replace('_options', '');
        var searchGroup = $('[data-target="' + searchGroupId + '"]');

        $(searchDateTrigger).closest('.ui-input-container').addClass('has-value');
        $('#' + searchOptionParentId).removeClass('show');
        searchGroup.removeClass('is-focused');
        searchGroup.find('button').show();
        searchGroup.find('button').attr('aria-expanded', false);
        searchGroup.find('input').hide();

        var nextSearchGroup = searchGroup.next('.ui-input-multiline_group');
        if (nextSearchGroup.length === 1) {
            var nextSearchGroupId = nextSearchGroup.data('target')
            nextSearchGroup.addClass('is-focused');
            nextSearchGroup.find('button').hide();
            nextSearchGroup.find('input').show();
            nextSearchGroup.find('input').focus();
        } else {

        }

        // show return options list
        $('#' + nextSearchGroupId + '_options').addClass('show');

        // validate fields outbound and inbound
        if (!$(this).hasClass('custom-day')) {
            var searchOutbound_input = $("input[data-target='#searchOutbound_options']").is(':visible');
            var searchReturn_input = $("input[data-target='#searchReturn_options']").is(':visible');

            if (!searchOutbound_input && !searchReturn_input) { // open searchDate
                searchDateTrigger.click();
            }
        }

        return false;
    });

    // typology
    searchTypologyTrigger.on('click', function () {
        $('.ui-input-multiline_group').removeClass('is-focused');
    });

    // options - close button
    $('.search_tt_close-btn').on('click', function () {
        searchOptionsList.removeClass('show');
        $('.ui-input-multiline_group').removeClass('is-focused');
        searchContainer.removeClass('routelist-open datelist-open');
        searchTypologyTrigger.attr('aria-expanded', false);
        setTimeout(function () {
            body.removeClass('search-opened');
        }, 200);
        var OpenedRoute = false;
    });

    // ----------------------------------------------------
    //## PROMOCODE 
    // ----------------------------------------------------
    $('[data-target="promocodeForm"]').hide();
    $('[data-target="promocodeLink"]').on('click', function () {
        $(this).hide();
        $('[data-target="promocodeForm"]').show();
        return false;
    });

}

/**
 * If the element was passed from the window
 * 
 * @param {element} [elem] // element DOM
 * 
 */
function validateElementInWindow (elem) {
    var elementTop = $(elem).offset().top;
    var elementHeight = $(elem).height();
    var windowHeight = $(window).height();
    var windowTop = $(window).scrollTop();
    return [(windowHeight + windowTop), (elementTop + elementHeight)];
}
// Variables

var dataScrollTop = {
    button: $('.btn-scroll-back')
};

function calScrollTopButton() {
    if (dataScrollTop.button.length) {
        $(window).on( 'scroll', function(){
            if (
                document.body.scrollTop > 20 ||
                document.documentElement.scrollTop > 20
            ) {
                dataScrollTop.button.addClass("show");
            } else {
                dataScrollTop.button.removeClass("show");
            }
        });
    }
}

function scrollTop() {
    if (dataScrollTop.button.length) {
        //click on the button, scroll to the top of the document
        dataScrollTop.button.on("click", function () {
            $("html, body").animate({ scrollTop: 0 }, "slow");
        });
    }
}


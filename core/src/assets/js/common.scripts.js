var W = $(window).outerWidth();
$(document).ready(function () {
    /*if (Modernizr.touch) {
      alert('Touch Screen');
    } else {
      alert('No Touch Screen');
    }*/

    var W = $(window).outerWidth();
    var body = $("body");

    // For reference
    var keys = {
        enter: 13,
        end: 35,
        home: 36,
        left: 37,
        up: 38,
        right: 39,
        down: 40,
        delete: 46
    };

    // ----------------------------------------------------
    //## IMAGES
    // ----------------------------------------------------
    //## css3 object fit fallback old browser 
    if (!Modernizr.objectfit) {
        $('.js-objectfit-helper').each(function () {
            var $container = $(this),
                imgUrl = $container.find('img').prop('src');
            if (imgUrl) {
                $container
                    .css('backgroundImage', 'url(' + imgUrl + ')')
                    .addClass('objectfit-helper');
            }
        });
    }
    //## Responsive images 
    //   (take <img src to use as background - used on full screen width banners)
    function responsiveBakcgroundImages() {
        var $srcSets = $('[data-target=rpImages]').children();
        var $rpImageLoad = $('[data-target=rpImages]').children('img');
        $srcSets.each(function () {
            var $currImg = $(this);

            var img = $currImg.get(0);
            var src = img.currentSrc ? img.currentSrc : img.src;
            $currImg.closest('[data-target=rpImagesBg]').css('background-image', "url('" + src + "')");
            $srcSets = $srcSets.not($currImg);

            $currImg.remove();
        });
    }
    responsiveBakcgroundImages();


    // ----------------------------------------------------
    //## FOOTER
    // ----------------------------------------------------
    //## keep footer at bootom page calculating its height
    footerAtBottom();


    // ----------------------------------------------------
    //## ALERT FLAGS
    // ----------------------------------------------------
    //## alert flag close button
    if ($('.alertflag').length > 0) {
        $('.alertflag .close').on('click', function () {
            $(this).closest('.alertflag').removeClass('in').delay(540).queue(function () {
                $(this).closest('.alertflag').hide();
            });
        });
    }


    // ----------------------------------------------------
    //## CHECKBOXS 
    // ----------------------------------------------------
    //## give it 'is-checked'class - angular issue
    $('.switch input').on('change', function () {
        console.log('clicked');
        if ($(this).is(':checked')) {
            $(this).prop('checked', true).attr('checked', 'checked');
            $(this).addClass('is-checked');
        } else {
            $(this).prop('checked', false).removeAttr('checked', 'checked');
            $(this).removeClass('is-checked');
        }
    });
    // switch slider
    $('.switch-slider label').on('click', function () {
        console.log('clicked');
        // remove reset all that are not this
        $('.switch-slider label').not(this).removeClass('is-checked');
        $('.switch-slider label').children('input').not(this).removeClass('is-checked');
        $('.switch-slider label').children('input').not(this).prop('checked', false).removeAttr('checked');
        // add to this
        $(this).addClass('is-checked');
        $(this).children('input').prop('checked', true).attr('checked', 'checked');
        $(this).children('input').addClass('is-checked');
    });


    // ----------------------------------------------------
    //## FORMS CONTROL
    // ----------------------------------------------------
    /* SIMULATE ANGULAR INPUT STATES */
    forms();

    // ----------------------------------------------------
    //## TOOLTIPS
    // ----------------------------------------------------
    $('[data-toggle="tooltip"]').tooltip({
        trigger: 'hover'
    });
    // $('[data-toggle="tooltip"]').tooltip('show');
    $('[data-toggle="popover"]').tooltip({
        trigger: 'click'
    });


    // ----------------------------------------------------
    //## TRIP SELECTOR
    // ----------------------------------------------------
    //## fares selection with tabs (at responsive)
    // REVIEW
    // var mobileFaresTabsElem = $('.flights_fares_tabs');
    // function mobileFaresTabs() {
    //     console.log('menor', W);
    //     var W = $(window).width();
    //     if (W <= 753) {
    //         $('.flights_fares_item:first-child').addClass('show');
    //         $('.flights_fares_tabs li:first-child').addClass('active');
    //     }
    // }
    // if (mobileFaresTabsElem.length > 0) {
    //     mobileFaresTabs();
    // }

    //## fares selection with select flight modifier
    // var flightselectModifierElem = $('.flightselect_modifier');
    // function flightselectModifier() {

    // }
    // if (mobileFaresTabsElem.length > 0) {
    //     flightselectModifier();
    // }


    // ----------------------------------------------------
    //## MEALS
    // ----------------------------------------------------
    //## carousel REVIEW
    // function responsiveMealsCarousel() {
    //     var mealsCarousel = $('.service_meals .carousel');
    //     var mealsCarouselItem = $('.service_meals .carousel .carousel_item');
    //     mealsCarouselItem.hide();
    //     if (W < 480) {
    //         $('.service_meals .carousel_item:lt(1)').show();
    //     }
    //     if (W >= 480) {
    //         $('.service_meals .carousel_item:lt(2)').show();
    //     }
    //     if (W >= 768) {
    //         $('.service_meals .carousel_item:lt(3)').show();
    //     }
    //     if (W >= 1020) {
    //         $('.service_meals .carousel_item:lt(4)').show();
    //     }

    // }
    // responsiveMealsCarousel();

    //## show voucher list
    var voucherRetrieveButton = $('.payment_partials .partials_voucher_form .button');
    $('.payment_partials .payment-summary').hide();
    $('.payment_partials .partials_voucher').hide();

    $('.payment_partials_trigger').on('click', function () {
        $('.payment_partials .payment-summary').hide();
        $('.payment_partials').removeClass('nopay');
    });
    voucherRetrieveButton.on('click', function () {
        $('.payment_partials .partials_voucher_form').hide();
        $('.payment_partials .partials_voucher').show();
        $('.payment_partials .payment-summary').show();
        $('.payment_partials').addClass('nopay');
    });

    //## End show voucher list
    //## show point slider
    $('.payment_partials .partial_points').hide();
    $('.payment_partials .partials_points_signin .button').on('click', function () {
        $('.payment_partials .partials_points_signin').hide();
        $('.payment_partials .partial_points').show();
    });
    //## End show point slider

    //## simple show-hide
    var isExpanded = false;
    var expanderTrigger = $('[data-expand="expander-trigger"]');
    var expanderDiv = $('[data-toggle="expander"]');
    expanderDiv.hide();

    function expanderShowHide() {
        var W = $(window).width();
        if (W <= 640) {
            expanderTrigger.addClass('collapsed');
            expanderDiv.hide();
            isExpanded = false;
        }
        if (W > 640) {
            expanderTrigger.removeClass('collapsed');
            expanderDiv.show();
            isExpanded = true;
        }

        expanderTrigger.on('click', function () {
            var expanderTargetId = $(this).data('target');
            var expanderTarget = $(expanderTargetId);
            if (expanderTarget.is(':visible')) {
                $(this).addClass('collapsed');
                expanderTarget.hide();
                isExpanded = false;
                return false;
            } else {
                $(this).removeClass('collapsed');
                expanderTarget.show();
                isExpanded = true;
                return false;
            }
        });
    }
    expanderShowHide();

    function sportsMoreInfo() {
        var W = $(window).width();
        if (W > 640) {
            $('.moreinfo_trigger').addClass('collapsed');
            $('.moreinfo').show();
            isExpanded = true;
        }
    }
    sportsMoreInfo();

    // POPOVERS
    var dataTogglePopover = $("[data-toggle='popover']");
    if (dataTogglePopover.length > 0) {
        dataTogglePopover.each(function (index, element) {
            var data = $(element).data();
            if (data.target) {
                var contentElementId = data.target;
                var contentHtml = $(contentElementId).html();
                data.content = contentHtml;
            }
            $(element).popover(data);
        });
    }

    // MMB
    $('.mmb_change-flight .tripselector_inbound').hide();
    $('.mmb_change-flight .route .ui-input-multiline input.ui-input').hide();
    $('.mmb_change-flight .route .ui-input-multiline button.ui-input').show();
    $('.mmb_change-flight .route .ui-input-multiline_group .ui-input').on('click', function () {
        return false;
    });
    $('.mmb_change-flight .dates .ui-input-multiline_group input').on('click', function () {
        searchContainer.removeClass('datelist-open');
    });

    //## EVENT: ON RESIZE
    $(window).on('resize', function () {
        var W = $(window).width();
        //openSummaryMobile();
        //openDetailsProtection();
        agentPopoverClose();
        expanderShowHide();
        sportsMoreInfo();
        // responsiveMealsCarousel();
        // headerFixed(); // use navResponsiveBottom.js file
        footerAtBottom();
        // headerResponsiveNav();
        // headerResponsiveSubNav();
        //footerNavCollapse();
        responsiveBakcgroundImages();
        //multipanel();

        // if (mobileFaresTabsElem.length > 0) {
        //     mobileFaresTabs();
        // }
        // if (W < 768) {
        //     body.removeClass('overlay-in').removeClass('overlay-in-md').removeClass('overlay-in-sm');
        // }
    });

    

    //## U-Connect show - hide
    $('.u-connect_footer .plus-icon').click(function () {
        var target = $('.u-connect_footer');
        var toggleClass = 'expanded';
        if (target.hasClass(toggleClass)) {
            target.removeClass(toggleClass)
        } else {
            target.addClass(toggleClass);
        }
    });

    //## Payment method show tabs change
    let tabs = [];
    $('.payment_methods > .tabs-pill > li > a').each(function (index, value) {
        tabs.push($(value).attr('href'));
    });
    tabs.map(function (value, index, array) {
        $('a[href="' + value + '"]').click(function () {
            $(this).removeClass('collapsed');
            const otherTabs = array.filter(function (otherTabsValue) {
                return otherTabsValue !== value;
            });
            otherTabs.map(function (otherTabsValue) {
                $('a[href="' + otherTabsValue + '"]').addClass('collapsed');
                $(otherTabsValue).removeClass('show');
            });
        });
    });

    // Seat Map
    $('.seat_details').hide();

    /* MMB */
    // ----------------------------------------------------

    // ## cancel flight
    $('.mmb_change-flight .route .ui-input-container').addClass('is-readonly');

    /* WCI */
    // ----------------------------------------------------
    // ## simulate *ngIf in checkin_selection checkedin
    //$('.checkin_selection.checkedin input').remove();


    /* Agency cabinet */
    $('.header_links_item.link-agent .header_link').click(function () {
        $('body').toggleClass('overlay-in-header');
    });

    function agentPopoverClose() {
        $('.header_links_item.link-agent .iconlink-close').click(function () {
            $('body').removeClass('overlay-in-header');
            $('.ui-dropdown [role="listbox"]').attr('aria-hidden', 'true');
            $('.ui-dropdown [aria-expanded]').attr('aria-expanded', 'false');
            dropdownOpened = false;
        });
    }
    agentPopoverClose();

    // ## toggle change password modal
    $('.ap_account_management .iconlink-password').click(function () {
        $('#change-password_modal').modal('show');
        return false;
    });

    /* U-FLY */
    // ## shows forgot password modal
    $('.forgot-password_form .btn-action').click(function () {
        $('#forgot-password_modal').modal('show');
        return false;
    });


    // // ## toggle footer nav on mobile
    // function footerNavCollapse() {
    //     var W = $(window).width();
    //     if (W <= 480) {
    //         $(".main-footer_nav nav ul").hide();
    //         $('.main-footer_nav_title a').on('click', function() {
    //             $(this).attr('aria-expanded') == 'true' ?  $(this).attr('aria-expanded', false) : $(this).attr('aria-expanded', true);
    //             $(this).removeClass('collapsed');
    //             $(this).parent().siblings().css({'max-height':'inherit', 'overflow-y': 'auto', 'transition': 'inherit'});
    //             $(this).parent().siblings().slideToggle();
    //         });
    //     } else {
    //         $(".main-footer_nav nav ul").show();
    //         $('.main-footer_nav_title a').attr('aria-expanded', true).addClass('collapsed');
    //     }
    // }
    // footerNavCollapse();

    // ## popover corporative home login
    // $('.main-header .main-header_menu .hasPopover a').on('click', function() {
    //     var e_ui_dropdown = $(this).siblings().children();
    //     e_ui_dropdown.toggle();
    //     if( e_ui_dropdown.attr('aria-hidden') ) {
    //         e_ui_dropdown.removeAttr('aria-hidden');
    //     } else {
    //         e_ui_dropdown.attr('aria-hidden', false);
    //     }
    // });
    // ## login user popover
    // $('.main-header_nav-secondary .nav_item').click(function() {
    //     $('body').toggleClass('overlay-in-header');
    // });
    $('#optionsmenuPopover').hide();
    $('[aria-controls="optionsmenuPopover"], #optionsmenuPopover .iconlink-close').click(function () {
        $('body').toggleClass('overlay-in-header');
        if ($('body').hasClass('overlay-in-header')) {
            $('#optionsmenuPopover').show();
        } else {
            $('#optionsmenuPopover').hide();
        }
    });
    // ## banner corporative home
    $('.main-banner_pager_item').on('click', function () {
        if (!$(this).hasClass("control-pause")) {
            var index = $(this).index();
            var main_banner_item = $('.main-banner .main-banner_item');
            $(this).siblings().removeClass('active');
            $(this).addClass('active');
            main_banner_item.removeClass('active');
            main_banner_item.eq(index).addClass('active');
        }
    });

    // ## Multiple panel collapse
    function multipanel() {
        var W = $(window).width();
        var multipanelTrigger = $('.multiple-panel_collapse_trigger');
        var multipanelCollapseDiv = multipanelTrigger.siblings();
        var multipanelOpenedByClass = $('.multiple-panel_item.show');
        var multipanelOpen = true;

        if (W <= 768) {
            multipanelTrigger.attr('aria-expanded', false);
            multipanelTrigger.siblings().hide();
            multipanelOpenedByClass.find(multipanelTrigger).attr('aria-expanded', true);
            multipanelOpenedByClass.find('.multiple-panel_item_content').show();

            multipanelTrigger.on('click', function () {
                var thisMultipanelCollapseDiv = $(this).siblings();
                if (thisMultipanelCollapseDiv.is(':visible')) {
                    //## if this panel is open close all others
                    multipanelTrigger.attr('aria-expanded', false);
                    multipanelCollapseDiv.slideUp();
                    var multipanelOpen = false;
                } else {
                    //## if this panel is close close all others and open this
                    multipanelTrigger.attr('aria-expanded', false);
                    multipanelCollapseDiv.slideUp();
                    $(this).attr('aria-expanded', true);
                    $(this).siblings().slideToggle();
                    var multipanelOpen = true;
                }
            });
        } else {
            var multipanelOpen = false;
            multipanelTrigger.siblings().show();
            multipanelTrigger.attr('aria-expanded', true);
        }
    }
    //multipanel();

    // NAV
    // $('.nav_item_link').on('keyup click', function(e) {
    //     if (e.keyCode === 13) {
    //         $(this).attr('aria-expanded', 'true');
    //         $(this).next('.main-header_subnav').attr('aria-hidden', 'false');
    //     }
    // });
    // SUBNAV
    // function headerResponsiveNav() {
    //     // main nav
    //     var navTrigger = $('.main-header_nav-primary .nav_item_link');
    //     var W = $(window).width();
    //     navTrigger.on('click', function() {
    //         if (W <= 768) {
    //             if ($(this).attr('aria-expanded') == 'true') {
    //                 //## if nav is already open, close all
    //                 navTrigger.attr('aria-expanded', false);
    //                 navTrigger.next().attr('aria-hidden', true);
    //                 setTimeout(function(){
    //                     $('body').removeClass('overlay-in-md');
    //                 }, 140);
    //             } else {
    //                 //## if nav not open, close all, open this
    //                 navTrigger.attr('aria-expanded', false);
    //                 navTrigger.next().attr('aria-hidden', true);

    //                 $(this).attr('aria-expanded', true);
    //                 $(this).next().attr('aria-hidden', false);

    //                 $('body').addClass('overlay-in-md');
    //             }
    //         } else {
    //             navTrigger.attr('aria-expanded', false);
    //             navTrigger.next().attr('aria-hidden', true);
    //             $('body').removeClass('overlay-in-md');
    //         }
    //     });
    // }
    // function headerResponsiveSubNav() {
    //     // main subnav
    //     var subnavTrigger = $('.main-header_subnav .main-header_subnav_title a');
    //     var subnavOpened = false;
    //     var W = $(window).width();
    //     subnavTrigger.on('click', function() {
    //         if (W <= 768) {
    //             if ($(this).attr('aria-expanded') == 'true') {
    //                 //## if subnav is already open, close all
    //                 subnavTrigger.attr('aria-expanded', false).addClass('collapsed');
    //                 subnavTrigger.parent().next().attr('aria-hidden', true);
    //             } else {
    //                 //## if subnav not open, close all, open this
    //                 subnavTrigger.attr('aria-expanded', false).addClass('collapsed');
    //                 subnavTrigger.parent().next().attr('aria-hidden', true);

    //                 $(this).attr('aria-expanded', true).removeClass('collapsed');
    //                 $(this).parent().next().attr('aria-hidden', false);
    //             }
    //         } else {
    //             subnavTrigger.attr('aria-expanded', false).addClass('collapsed');
    //             subnavTrigger.parent().next().attr('aria-hidden', true);
    //         }
    //     });
    // }
    // headerResponsiveNav();
    // headerResponsiveSubNav();

    // Scroll the element with id="example" into the visible area of the browser window:
    var links = document.querySelectorAll('a[href*="#"]');
    // Array.from(links).forEach((link) => {
    //     link.addEventListener('click', function (event) {
    //         event.preventDefault();            
    //         var idlink = link.getAttribute('href').split("#").pop();     
    //         var elmt = document.getElementById(idlink); 
    //         if(elmt != null) {
    //             elmt = elmt.getBoundingClientRect();
    //             window.scroll({
    //                 top: elmt.top + window.scrollY - 60,
    //                 behavior: 'smooth',
    //                 left: 0,
    //             });  
    //         }                    
    //     });
    // });

    // SCROLL TOP
    scrollTop();
    calScrollTopButton();
});

$(window).on('load', function () {
    if ($("#loader").length) {
        $(".page-loader").fadeOut();
        $(".loader").fadeOut();
    }
});

/**
 * set page scroll to specific html element
 * @param {element} element reference html element to set scroll
 * @param {number} extraLength extra lenght in pixels to set scroll
 * @param {number} animationInterval milliseconds animation interval
 * @param {string} animationEasing type of animation (swing, linear, ...)
 */
function setScrollTo(element, extraLength, animationInterval, animationEasing) {
    extraLength = extraLength || 0;
    animationInterval = animationInterval || 400;
    animationEasing = animationEasing || 'swing';
    $([document.documentElement, document.body]).animate({
        scrollTop: element.offset().top + extraLength
    }, animationInterval, animationEasing);
}
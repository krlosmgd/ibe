// Variables

function webSearch() {
    var webSearch = $('.web-search');
    var webSearchTrigger = $('.web-search_trigger_button');
    var webSearchCloseTrigger = $('.web-search_cancel-button');
    webSearchTrigger.click(function() {
        webSearch.addClass('is-open')
    });
    webSearchCloseTrigger.click(function() {
        webSearch.removeClass('is-open')
    });
}


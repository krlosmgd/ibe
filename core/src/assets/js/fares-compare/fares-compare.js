/**
 * add is-active class to compare-fares compoennt
 */
function compareFaresTrigger() {
    // variables
    var W = $(window).outerWidth();
    var body = $("body");
    var trigger = $('.fares-compare_trigger');
    var wrap = $('.fares-compare');
    var closeButton = $('.fares-compare_close-button');

    // events
    if (W < 768) {
        trigger.click(function () {
            wrap.addClass('is-active');
        })
        closeButton.click(function () {
            wrap.removeClass('is-active');
        });
    } else {
        wrap.removeClass('is-active');
    }

}
$(window).on('resize', function () {
    compareFaresTrigger();
});
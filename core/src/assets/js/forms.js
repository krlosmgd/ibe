// Variables
var dataForm = {
    input: $('.ui-input'),
    inputContainer: $('.ui-input-container'),
    status: {
        focused: "is-focused",
        value: "has-value",
        readonly: "is-readonly",
        disabled: "is-disabled"
    }
}

function forms() {
    dataForm.input.focus(function () {
        var inputContainer = $(this).closest(dataForm.inputContainer);

        if (inputContainer.hasClass(dataForm.status.readonly) || inputContainer.hasClass(dataForm.status.disabled))
            return
        inputContainer.addClass(dataForm.status.focused);
    });
    dataForm.input.focusout(function () {
        var inputContainer = $(this).closest(dataForm.inputContainer);

        // remove is-focused state
        inputContainer.removeClass(dataForm.status.focused);

        // add or remove has-value state
        if ($(this).val().length !== 0) {
            inputContainer.addClass(dataForm.status.value);
        } else {
            inputContainer.removeClass(dataForm.status.value);
        }
    });
}


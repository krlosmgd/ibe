
// variables
var outboundFlightCurrentIndex = -1;
var inboundFlightCurrentIndex = -1;
var dataFares = {
    outbound: 'tripselector_outbound',
    inbound: 'tripselector_inbound',
    fareCloseBtn: $('.flights_fares_button-close'),
    htmlBody: $('html,body'),
    combinedSummary: $('.combined_summary_wrapper')
}

function faresSelectionCollapse() {
    // close and open fares flight
    dataFares.fareCloseBtn.click(function () {
        if (Boolean($(this).attr('aria-expanded'))) {
            $(this).closest('.flight').removeClass('is-open');
        }
    })
    // open fares by flight
    $('.price-rate').click(function () {
        flightValidate($(this));
    })
}

/**
 * 
 * @param {element} [elm] even button current flight
 * 
 */
function flightValidate(elm, withoutScroll) {
    withoutScroll = withoutScroll || false;
    if (Boolean(elm.attr('aria-expanded'))) {

        // if the flight is outbound or inbound
        if (elm.closest('.flight').closest('.'+dataFares.outbound).length != 0) {
            // if flight is outbound
            if (outboundFlightCurrentIndex != -1) {
                // validate the fligh by journey
                flightByJourney(elm.closest('.flight'), dataFares.outbound, outboundFlightCurrentIndex);
            }
            outboundFlightCurrentIndex = elm.closest('.flight').index();
        } else {
            // if flight is inbound
            if (inboundFlightCurrentIndex != -1) {
                flightByJourney(elm.closest('.flight'), dataFares.inbound, inboundFlightCurrentIndex );
            }
            inboundFlightCurrentIndex = elm.closest('.flight').index();
        }

        if(!withoutScroll) {
            scrollGoTo(elm.closest('.flight'));
        } 

        if (elm.closest('.flight').hasClass('is-open')) {
            elm.closest('.flight').removeClass('is-open');
        } else {
            elm.closest('.flight').addClass('is-open');
        }
    }
}

/**
 * 
 * @param {element} [elm] current flight
 * 
 * @param {string} [journey] flight type Ej: outbound and inbound
 * 
 * @param {number} [index] flight index for journey
 * 
 */
function flightByJourney(elm, journey, index) {
    if (elm.index() != index) {
        // if the flight it is different the from current, close fares
        flightRemoveStatus(journey, index, elm);
    } 
}

/**
 * 
 * @param {string} [journey] flight type Ej: outbound and inbound
 * 
 * @param {number} [index] flight index for journey
 * 
 * @param {element} [flight] current flight
 * 
 */
function flightRemoveStatus(journey, index, flight) {
    $('.' + journey + ' .flight').eq(index).removeClass('is-open');
    $('.' + journey + ' .flight').eq(index).find('.collapse').removeAttr('style');
    $('.' + journey + ' .flight').eq(index).find('.collapse').removeClass('show');
    $('.' + journey + ' .flight').eq(index).find('.price-rate').attr('aria-expanded', false);
    $('.' + journey + ' .flight').eq(index).find('.flights_fares_button-close').attr('aria-expanded', false);
    $('.' + journey + ' .flight').eq(index).find('.collapse').collapse('hide');
}

/**
 * 
 * @param {element} [elm] go to element
 * 
 * @param {number} [aux] add or subtract position in scroll
 * 
 * @param {boolean} [elmOnly=false] when the element is own
 */
function scrollGoTo(elm, aux, elmOnly) {
    aux = aux || 0;
    elmOnly = elmOnly || false;
    if (elmOnly) { 
        var elm = elm;
    } else {
        var elm = elm.offset().top -
        (dataFares.combinedSummary.height() + aux);
    }
    dataFares.htmlBody.animate(
        {
            scrollTop: elm
        }, "slow"
    );
}
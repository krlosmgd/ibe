
// variables
var outboundFarePrevious = '';
var inboundFarePrevious = '';
var outboundFlight, outboundFare, outboundPriceRate;
var inboundFlight, inboundFare, inboundPriceRate;
var dataTripSelector = {
    headerContainer: $(".header_container"),
    outboundClass: '.tripselector_outbound',
    inboundClass: '.tripselector_inbound',
    statusFlight: 'is-selected',
    selected: 'selected',
    animationClass: 'animating',
    timeoutClass: 600,
    timeoutCurrentFlight: 200
    
}
function tripSelector() {
    // create event of each fare
    $('.flight').on('click', '.flights_fares_item', function () {

        // current flight
        var flight = $(this).closest('.flight');

        // current fare
        var fare = $(this);

        // element price-rate of current flight
        var priceRate = flight.find('.price-rate');

        // validate the journey type for add class to other elements
        // outbound
        if (flight.closest(dataTripSelector.outboundClass).length != 0) {
            outboundFlight = flight
            outboundFare = fare;
            outboundPriceRate = priceRate;
            var farePrevious = flightSelected(flight, fare, priceRate, dataTripSelector.outboundClass, outboundFarePrevious);
            outboundFarePrevious = farePrevious;

        } else { // inbound
            inboundFlight = flight
            inboundFare = fare;
            inboundPriceRate = priceRate;
            var farePrevious = flightSelected(flight, fare, priceRate, dataTripSelector.inboundClass, inboundFarePrevious);
            inboundFarePrevious = farePrevious;
        }
    });

    // reset all flight outbound BUTTON MODIFIER flight
    $(dataTripSelector.outboundClass).on('click', '.tripselector_modifier_button button', function () {
        $(dataTripSelector.outboundClass).removeClass(dataTripSelector.statusFlight);
        outboundFlight.removeClass(dataTripSelector.statusFlight);
        outboundPriceRate.removeClass(dataTripSelector.selected + ' ' + outboundFare);

        setTimeout(function () {
            // outboundPriceRate.click(); // open current flight with fare selected
            scrollGoTo($(dataTripSelector.outboundClass), 10); // go to list flights
            outboundFare.removeClass(dataTripSelector.selected); // reset selected fare of current flight
        }, dataTripSelector.timeoutCurrentFlight);

        outboundPriceRate.prop('disabled', false);
    })

    // reset all flight inbound BUTTON MODIFIER flight
    $(dataTripSelector.inboundClass).on('click', '.tripselector_modifier_button button', function () {
        $(dataTripSelector.inboundClass).removeClass(dataTripSelector.statusFlight);
        inboundFlight.removeClass(dataTripSelector.statusFlight);
        inboundPriceRate.removeClass(dataTripSelector.selected + ' ' + inboundFare);
        
        setTimeout(function () {
            // inboundPriceRate.click(); // open current flight with fare selected
            scrollGoTo($(dataTripSelector.inboundClass), 10); // go to list flights
            inboundFare.removeClass(dataTripSelector.selected); // reset selected fare of current flight
        }, dataTripSelector.timeoutCurrentFlight);

        inboundPriceRate.prop('disabled', false);
    })
}

/**
 * 
 * @param {element} [flight] current flight
 * 
 * @param {element} [fare] current fare
 * 
 * @param {element} [pricerate] button of current flight
 * 
 * @param {string} [journey] flight type Ej: outbound and inbound
 * 
 * @param {string} [farePrevius] previous fare flight
 * 
 */
function flightSelected(flight, fare, priceRate, journey, farePrevious) {

    // add class is-select to flight
    flight.addClass(dataTripSelector.statusFlight);

    // add class animating to flight
    flight.addClass(dataTripSelector.animationClass);

    // remove class animating of flight, present for 0.6seg
    setTimeout(function () {
        flight.removeClass(dataTripSelector.animationClass);
    }, dataTripSelector.timeoutClass)

    flight.closest(journey).addClass(dataTripSelector.statusFlight + ' ' + dataTripSelector.animationClass);

    // remove selected class of flight rates different from the current flight
    flight.siblings().find('.flights_fares_item').removeClass(dataTripSelector.selected);

    // remove class sibligs current fare
    fare.siblings().removeClass('selected');

    // add class selected current fare
    fare.addClass(dataTripSelector.selected);

    // get name string fare
    var fareText = fare.attr('class').split(' ').filter(/./.test, /fare/)[1];

    // validate and delete fare previous
    if (farePrevious != '') {
        if (farePrevious != fareText) {
            priceRate.removeClass(farePrevious);
        }
    }
    // save fare previous
    farePrevious = fareText;
    // add class selected to button
    priceRate.addClass(dataTripSelector.selected + ' ' + fareText);

    // execute event close fares
    priceRate.closest('.rowFlight').siblings('.rowFares').children('.collapse').collapse('hide');
    flightValidate(priceRate, true);

    // scroll to top Journeys
    scrollGoTo(dataTripSelector.headerContainer.height(), 0, true);
    
    // disabled button price rate
    priceRate.prop('disabled', true);

    return farePrevious;

}

/**
 * 
 * @param {number} [outerWidth] outerWidth window
 * 
 * @param {number} [maxWidth=768] maxWidth to generate script action
 * 
 */
function weekSelector(outerWidth, maxWidth) {
    maxWidth = maxWidth || 640;
    if ($('.week').length > 0) {
        $('.week').each(function () {
            var weekText = $(this).text();
            if (outerWidth < maxWidth) {
                len = $(this).text().replace(/ /g, '').length;
                $(this).text($(this).text().substr(0, 3));
            } else {
                $(this).text(weekText);
            }
        });
    }
}
/**
 * add is-active class to compare-fares component
 */

function connectionsModal() {

    //## variables
    var W = $(window).outerWidth();
    var body = $("body");
    var trigger = $('.connection_link_wrap');
    var wrap = $('.connection_popover');
    var closeButton = $('.connection_popover_close-button');
    var button = $('.connection_link');

    // popover events
    //## works on hover (css) and clic
    trigger.on('click', function () {
        var popover = $(this).next('.connection_popover');
        if (popover.hasClass('is-active')) {
            popover.removeClass('is-active');
        } else {
            popover.addClass('is-active');
        }
        // if (W <= 768) {
        //     body.addClass(overlayClass);
        // }
    });
    //## when clicking an then hover another link, hide the last opened popover
    trigger.on('mouseover', function () {
        var popover = $(this).next('.connection_popover.is-active');
        $('.connection_popover').not(popover).removeClass('is-active');
    });
    //## whem click out trigger link hide popover
    button.blur(function () {
        if (W > 768) {
            var popover = $(this).parent().parent().find('.connection_popover');
            popover.removeClass('is-active');
        }
    });
    //## close button
    closeButton.click(function () {
        wrap.removeClass('is-active');
        // body.removeClass(overlayClass);
    });
    //## reset on resize
    $(window).on('resize', function () {
        if (W > 768) {
            button.blur();
        }
    });
}
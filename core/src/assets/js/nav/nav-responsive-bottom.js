var navData = {
    body: $('body'),
    overlay: $('.ui-overlay'),
    subnavs: $('.main-header_nav-primary .main-header_subnav'),
    mobileButtons: $('.main-header_nav-primary .nav_item_link'),
    closeButton: $('.main-header_subnav-button-close'),
    mainHeader: $('.main-header')
}

/**
 *
 * @param {number} [minWidth=768] min minWidth to generate script action
 */
function navResponsiveBottom(minWidth) {
	// only in
	minWidth = minWidth || 768;
	if (W <= minWidth) {
		// variables
		var body = navData.body;
		var overlay = navData.overlay;
		var subnavs = navData.subnavs;
		var mobileButtons = navData.mobileButtons;
		var closeButton = navData.closeButton;

		// --------------------------------------------------------
		// ++ call center
		// --------------------------------------------------------
		var callCenter = $('.call-center');
		$('.call-center_trigger_button').click(function() {
			$('.nav_item').removeClass('active');
			if (callCenter.hasClass('is-active')) {
				body.removeClass('overlay-in-md');
				callCenter.removeClass('is-active');
			} else {
				callCenter.addClass('is-active');
			}
		});
		$('.call-center_content_head_close-button').click(function() {
			callCenter.removeClass('is-active');
			body.removeClass('overlay-in-md');
		});

		// --------------------------------------------------------
		// ++ level 01
		// --------------------------------------------------------

		// + events

		mobileButtons.click(function() {
			// variables
			var subnav = $(this)
				.parent()
				.find('.main-header_subnav');

			// remove call center is-open state
			callCenter.removeClass('is-active');

			// confirm if is a active button
			if (
				$(this)
					.parent()
					.hasClass('active')
			) {
				// remove active state and hide subnav
				body.removeClass('overlay-in-md');
				$(this)
					.parent()
					.removeClass('active');
			} else {
				// clean active state in li
				mobileButtons.parent().removeClass('active');
				// add active class to clicked button
				$(this)
					.parent()
					.addClass('active');
			}

			// toggle subnav
			if (subnav.attr('aria-hidden') === 'true') {
				// if is hidden
				body.addClass('overlay-in-md');
				subnavs.attr('aria-hidden', true);
				subnav.attr('aria-hidden', false);
			} else {
				subnav.attr('aria-hidden', true);
			}
		});

		overlay.click(function() {
			closeMenu();
		});

		closeButton.click(function() {
			closeMenu();
		});

		// --------------------------------------------------------
		// ++ level 02
		// --------------------------------------------------------

		var mobileButtonsLevel2 = $('.subnav_item_n2');
		var level02Heights = [];

		// get each subnav height
		mobileButtonsLevel2.each(function(index) {
			var subnav = $(this).next();

			subnav.css('height', 'auto');
			level02Heights[index] = subnav.outerHeight() + 'px';
			subnav.css({
				height: '0',
			});
		});

		// toggle subnavs
		mobileButtonsLevel2.click(function() {
			var subnav = $(this).next();
			var index = mobileButtonsLevel2.index(this);

			if ($(this).hasClass('collapsed')) {
				$(this).removeClass('collapsed');
			} else{
				$(this).addClass('collapsed');
			}

			var heightToSet = subnav.css('height') === '0px' ? level02Heights[index] : 0;

			subnav.animate(
				{
					height: heightToSet,
					opacity: 1,
				},
				100
			);
		});

		// --------------------------------------------------------
		// ++ level 03
		// --------------------------------------------------------

		var mobileButtonsLevel3 = $('.subnav_item_n3');
		var level03Heights = [];

		// get each subnav height
		mobileButtonsLevel3.each(function(index) {
			var subnav = $(this).next();

			subnav.css('height', 'auto');
			level03Heights[index] = subnav.outerHeight() + 'px';
			subnav.css({
				height: '0',
			});
		});

		// toggle subnavs
		mobileButtonsLevel3.click(function() {
			var subnav = $(this).next();
			var index = mobileButtonsLevel3.index(this);

			var heightToSet = subnav.css('height') === '0px' ? level03Heights[index] : 0;

			subnav.animate(
				{
					height: heightToSet,
					opacity: 1,
				},
				100
			);
		});
	}

	// functions
	function closeMenu() {
		body.removeClass('overlay-in-md');
		subnavs.attr('aria-hidden', true);
		mobileButtons.parent().removeClass('active');
	}
}


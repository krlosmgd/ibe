module.exports = {
  site_title: 'HK IBE [HTML]',
  demo_imgs_path: '../src/assets/imgs/',
  core_assets_path: '../../../core/src/assets/',
  project_assets_path: '../src/assets/',
  logo_img: 'logo.svg',
  logo_negative_img: 'logo-neg.svg',
  currency: 'HKD',
  weightmeasure: 'Kg',
  promoLabelDefault: 'Promo',
  formRequiredSymbol: '*',
  formRequiredText: '* Indicates a mandatory field',
  mainHeaderMenu: [{
      text: 'Plan',
      submenus: [{
          items: [{
              title: 'Flight with us',
              url: '#',
            },
            {
              title: 'Safety',
              url: '#',
            },
            {
              title: 'Our Fleet',
              url: '#',
            },
            {
              title: 'Our loyalty program',
              url: '#',
            },
            {
              title: 'Group travel',
              url: '#',
            },
          ],
        },
        {
          items: [{
            title: 'Book',
            submenuList: [{
                text: 'Booking via our website',
                url: '#',
              },
              {
                text: 'U-Fly pass',
                url: '#',
              },
              {
                text: 'Baggage',
                url: '#',
              },
              {
                text: 'U-First',
                url: '#',
              },
              {
                text: 'U-connect',
                url: '#',
              },
              {
                text: 'Other fees',
                url: '#',
              },
              {
                text: 'Fuel Surcharge',
                url: '#',
              },
              {
                text: 'Government imposed fees',
                url: '#',
              },
            ],
          }, ],
        },
        {
          items: [{
            title: 'Travel information',
            submenuList: [{
                text: 'Baggage guide',
                submenuListN4: [{
                    text: 'Checked Baggage',
                    url: '#',
                  },
                  {
                    text: 'Carry on Baggage',
                    url: '#',
                  },
                  {
                    text: 'Oversized baggage / sports equipment',
                    url: '#',
                  },
                  {
                    text: 'Smart baggage',
                    url: '#',
                  },
                ],
              },
              {
                text: 'Inflight entertainment',
                url: '#',
              },
              {
                text: 'Special Assistance',
                url: '#',
              },
              {
                text: 'Mainland connection',
                url: '#',
              },
              {
                text: 'Travel documents',
                url: '#',
              },
            ],
          }, ],
        },
        {
          items: [{
            title: 'Special offers',
            submenuList: [{
                text: 'Inflight specials',
                url: '#',
              },
              {
                text: 'Duty Free',
                url: '#',
              },
              {
                text: 'Travel Insurance',
                url: '#',
              },
              {
                text: 'Activities',
                url: '#',
              },
              {
                text: 'Car rentals',
                url: '#',
              },
              {
                text: 'Hotel offers',
                url: '#',
              },
              {
                text: 'Package offers',
                url: '#',
              },
            ],
          }, ],
        },
      ],
    },
    {
      text: 'Your trips',
      submenus: [{
          items: [{
              title: 'Manage your booking',
              url: '#',
            },
            {
              title: 'Check-in',
              submenuList: [{
                  text: 'Online Check-in',
                  url: '#',
                },
                {
                  text: 'Airport Check-in',
                  url: '#',
                },
                {
                  text: 'Intown Check-in at Hong Kong',
                  url: '#',
                },
                {
                  text: 'Check-in at PRD / Macau',
                  url: '#',
                },
              ],
            },
          ],
        },
        {
          items: [{
            title: 'Important travel notice',
            url: '#',
          }, ],
        },
      ],
    },
    {
      text: 'Explore',
      url: '#',
      submenus: [{
          items: [{
            title: 'Route map',
            url: '#',
          }, ],
        },
        {
          isLargeColumn: true,
          items: [{
            title: 'Destinations',
            hasColumns: true,
            submenuList: [
              [{
                  text: 'Hong Kong SAR',
                  submenuListN4: [{
                    text: 'Hong Kong',
                    url: '#',
                  }, ],
                },
                {
                  text: 'Korea',
                  submenuListN4: [{
                      text: 'Seoul',
                      url: '#',
                    },
                    {
                      text: 'Busan',
                      url: '#',
                    },
                    {
                      text: 'Jeju',
                      url: '#',
                    },
                  ],
                },
              ],
              [{
                  text: 'U.S. Territories',
                  submenuListN4: [{
                    text: 'Saipan',
                    url: '#',
                  }, ],
                },
                {
                  text: 'Japan',
                  submenuListN4: [{
                      text: 'Tokyo',
                      url: '#',
                    },
                    {
                      text: 'Osaka',
                      url: '#',
                    },
                    {
                      text: 'Nagoya',
                      url: '#',
                    },
                    {
                      text: 'Hiroshima',
                      url: '#',
                    },
                    {
                      text: 'Takamatsu',
                      url: '#',
                    },
                    {
                      text: 'Fukuoka',
                      url: '#',
                    },
                    {
                      text: 'Kagoshima',
                      url: '#',
                    },
                    {
                      text: 'Kumamoto',
                      url: '#',
                    },
                    {
                      text: 'Ishigaki',
                      url: '#',
                    },
                  ],
                },
              ],
              [{
                  text: 'Cambodia',
                  submenuListN4: [{
                    text: 'Siem Reap',
                    url: '#',
                  }, ],
                },
                {
                  text: 'China',
                  submenuListN4: [{
                      text: 'Kunming',
                      url: '#',
                    },
                    {
                      text: 'Ningbo',
                      url: '#',
                    },
                  ],
                },
              ],
              [{
                  text: 'Thailand',
                  submenuListN4: [{
                      text: 'Chiang Mai',
                      url: '#',
                    },
                    {
                      text: 'Chiang Rai',
                      url: '#',
                    },
                    {
                      text: 'Phuket',
                      url: '#',
                    },
                  ],
                },
                {
                  text: 'Vietnam',
                  submenuListN4: [{
                      text: 'Da Nang',
                      url: '#',
                    },
                    {
                      text: 'Nha Trang',
                      url: '#',
                    },
                  ],
                },
              ],
              [{
                text: 'Taiwan',
                submenuListN4: [{
                    text: 'Taichung',
                    url: '#',
                  },
                  {
                    text: 'Hualien',
                    url: '#',
                  },
                ],
              }, ],
            ],
          }, ],
        },
      ],
    },
  ],
  bookingSteps: [{
      stepNumber: '1',
      stepLabel: 'Select Flight',
      stepUrl: '',
    },
    {
      stepNumber: '2',
      stepLabel: 'Guest details',
      stepUrl: '',
    },
    {
      stepNumber: '3',
      stepLabel: 'Add extras',
      stepUrl: '',
    },
    {
      stepNumber: '4',
      stepLabel: 'Seat selection',
      stepUrl: '',
    },
    {
      stepNumber: '5',
      stepLabel: 'Payment &amp; Confirmation',
      stepUrl: '',
    },
  ],
  months: [{
      month: 'December',
      year: '2017',
    },
    {
      month: 'January',
      year: '2018',
      selected: true,
    },
    {
      month: 'February',
      year: '2018',
    },
    {
      month: 'March',
      year: '2018',
    },
    {
      month: 'April',
      year: '2018',
    },
    {
      month: 'May',
      year: '2018',
    },
    {
      month: 'June',
      year: '2018',
    },
    {
      month: 'July',
      year: '2018',
    },
    {
      month: 'August',
      year: '2018',
    },
    {
      month: 'September',
      year: '2018',
    },
  ],
  days: [{
      day: '24 DEC',
      week: 'Sunday',
      price: '477',
    },
    {
      day: '25 DEC',
      week: 'Monday',
      price: '11,490',
      selected: true,
    },
    {
      day: '26 DEC',
      week: 'Tuesday',
      price: '326',
    },
    {
      day: '27 DEC',
      week: 'Wednesday',
      price: '11,490',
    },
    {
      day: '28 DEC',
      week: 'Thursday',
      price: '14,490',
      bestprice: true,
    },
    {
      day: '29 DEC',
      week: 'Friday',
      price: '477',
    },
    {
      day: '30 DEC',
      week: 'Saturday',
      price: '477',
    },
    {
      day: '31 DEC',
      week: 'Sunday',
      price: '477',
    },
    {
      day: '01 Jan',
      week: 'Monday',
      price: '477',
    },
    {
      day: '02 Jan',
      week: 'Tuesday',
      price: '477',
    },
    {
      day: '03 Jan',
      week: 'Wednesday',
      price: '477',
    },
  ],
  journeys: [{
      'class': 'outbound',
      way: 'Departure',
      headerImgSrc: '../../App_Plugins/IBE/assets/img/selectflight/banner-departure.jpg',
      departureAirport: 'Hong Kong',
      departureAirportCode: 'HKG',
      departureAirportTerminal: 'Terminal 4',
      arrivalAirport: 'Kunming',
      arrivalAirportCode: 'KMD',
      arrivalAirportTerminal: 'Terminal 1',
      date: 'Sat, 16 Dec 2017',
      flightNumber: [
        'UO 1252',
        'UO 1278',
      ],
    },
    {
      'class': 'inbound',
      way: 'Return',
      headerImgSrc: '../../App_Plugins/IBE/assets/img/selectflight/banner-return.jpg',
      departureAirport: 'Kunming',
      departureAirportCode: 'KMD',
      departureAirportTerminal: 'Terminal 2',
      arrivalAirport: 'Hong Kong',
      arrivalAirportCode: 'HKG',
      arrivalAirportTerminal: 'Terminal 1',
      date: 'Sat, 16 Dec 2017',
      flightNumber: [
        'UO 1252',
        'UO 1278',
      ],
    },
  ],
  flightCodeTimeSeparator: '|',
  flightCaption: 'Flight',
  flights: [{
      departureTime: '10:30',
      departureAirport: 'Hong Kong',
      departureAirportCode: 'HKG',
      departureAirportTerminal: 'Terminal 2',
      duration: '2:10h',
      stop: '2 stop',
      arrivalTime: '14:00',
      arrivalAirport: 'Kunming',
      arrivalAirportCode: 'KMD',
      arrivalAirportTerminal: 'Terminal 2',
      priceFrom: '325',
      connection: false,
      fareSelected: false,
      unavailable: true,
    },
    {
      departureTime: '07:40',
      departureAirport: 'Hong Kong',
      departureAirportCode: 'HKG',
      departureAirportTerminal: 'Terminal 2',
      duration: '2:30h',
      stop: '1 stop',
      arrivalTime: '10:10',
      arrivalAirport: 'Kunming',
      arrivalAirportCode: 'KMD',
      arrivalAirportTerminal: 'Terminal 2',
      priceFrom: '1245',
      connection: true,
      fareSelected: true,
      fareSelectedName: 'Fun+',
    },
    {
      departureTime: '14:30',
      departureAirport: 'Hong Kong',
      departureAirportCode: 'HKG',
      duration: '2:00h',
      stop: '1 stop',
      arrivalTime: '16:00',
      arrivalAirport: 'Kunming',
      arrivalAirportCode: 'KMD',
      priceFrom: '325',
      connection: false,
      fareSelected: false,
    },
  ],
  passengers: [{
      label: 'Guest 1:',
      gender: 'Mr.',
      firstName: 'John',
      lastName: 'Smith Danes Wilson',
      type: 'Adult',
      seat: '8F',
      seatStatus: '',
      checkedIn: true,
    },
    {
      label: 'Guest 2:',
      gender: 'Mrs.',
      firstName: 'Jane',
      lastName: 'Doe',
      type: 'Adult',
      seat: '',
      seatStatus: 'selected',
      checkedIn: false,
      hasInfoButton: true,
    },
    {
      label: 'Passenger 3:',
      gender: 'Mr.',
      firstName: 'Richard',
      lastName: 'Doe',
      type: 'Infant',
      seat: '3G',
      seatStatus: '',
      checkedIn: false,
      email: 'richard.roe@hkexpress.com',
      signUpReference: 'W4PZ7C',
      departure: '4540002144',
    },
  ],
  fares: [{
      pos: '1',
      name: 'Fun',
      isPromo: true,
      price: '325',
      fareDescription: [{
          icon: 'boarding-pass',
          description: 'Fare difference and change fee applies',
        },
        {
          icon: 'baggage-v',
          description: 'Carry-on (0kg) / Check-in (25kg)',
        },
        {
          icon: 'reward-u',
          description: '10 reward-U points for HKD 1 spent',
        },
      ],
    },
    {
      pos: '2',
      name: 'Fun +',
      isPromo: false,
      price: '758',
      fareDescription: [{
          icon: 'boarding-pass',
          description: 'Fare difference and change fee applies',
        },
        {
          icon: 'baggage-v',
          description: 'Carry-on (7kg) / Check-in (25kg)',
        },
        {
          icon: 'reward-u',
          description: '10 reward-U points for HKD 1 spent',
        },
        {
          icon: 'seat-airplane',
          description: 'Up-front Seat',
        },
      ],
    },
    {
      pos: '3',
      name: 'U-biz',
      isPromo: false,
      price: '999,437',
      fareDescription: [{
          icon: 'boarding-pass',
          description: 'Changes up to 3 hours before departure',
        },
        {
          icon: 'baggage-v',
          description: 'Carry-on (7kg) / Check-in (30kg)',
          note: '* Please note size and weight limitations: <a href=\'#\' target=\'_blank\'>Checked baggage</a> | <a href=\'#\' target=\'_blank\'>Carry-on baggage</a>',
        },
        {
          icon: 'reward-u',
          description: '20 reward-U points for HKD 1 spent',
        },
        {
          icon: 'seat-airplane',
          description: 'Sweet Seat',
        },
        {
          icon: 'passenger-first',
          description: 'U-First Priority Check in and Pre-boarding',
        },
        {
          icon: 'refunds',
          description: 'Refunds*',
          note: '* <a href=\'#\' target=\'_blank\'>Terms and conditions</a> apply',
        },
        {
          icon: 'couch',
          description: 'Premium Lounge at Hong Kong Airport',
        },
        {
          icon: 'baggage-conveyor',
          description: 'Priority Baggage Retrieval',
        },
      ],
    },
  ],
  seats: [{
      type: '',
      name: 'Standard Seat',
      price: 'HKD 35',
      description: 'For safety reasons, the seatbacks in row 11 do not recline.',
    },
    {
      type: 'upfront',
      name: 'Upfront Seat',
      price: 'HKD 75',
      description: '',
    },
    {
      type: 'xlarge',
      name: 'Sweet Seat',
      price: 'HKD 160 <span class=\'rp\'>(25% off airport rate)</span>',
      description: 'Extra legroom. For safety reasons, the seatbacks in row 12 do not recline.',
    },
    {
      type: 'unavailable',
      name: 'Unavailable Seat',
      price: '',
      description: '',
    },
    {
      type: 'selected',
      name: 'Selected Seat',
      price: '',
      description: '',
    },
  ],
  baggages: [{
      title: 'Departing Baggage',
      type: [{
          price: '229',
          size: '20',
          promo: true,
        },
        {
          price: '360',
          size: '25',
          promo: true,
        },
        {
          price: '365',
          size: '30',
          promo: true,
        },
      ],
    },
    {
      title: 'Returning Baggagee',
      type: [{
        price: '365',
        size: '30',
        promo: false,
      }, ],
    },
  ],
  baggagesOW: [{
    title: '',
    type: [{
        price: '229',
        size: '20',
        promo: true,
      },
      {
        price: '360',
        size: '25',
        promo: true,
      },
      {
        price: '365',
        size: '30',
        promo: true,
      },
    ],
  }, ],
  specialBaggages: [{
      title: 'Departing Oversized Baggage',
      type: [{
          price: '229',
          size: '20',
          promo: true,
        },
        {
          price: '360',
          size: '25',
          promo: true,
        },
        {
          price: '365',
          size: '30',
          promo: true,
        },
      ],
    },
    {
      title: 'Returning Oversized Baggage',
      type: [{
        price: '365',
        size: '30',
        promo: false,
      }, ],
    },
  ],
  specialBaggagesOW: [{
    type: [{
        price: '229',
        size: '20',
        promo: true,
      },
      {
        price: '360',
        size: '25',
        promo: true,
      },
      {
        price: '365',
        size: '30',
        promo: true,
      },
    ],
  }, ],
  meals: [{
      name: 'Thai Red Curry Chicken with Rice Thai Red Curry Chicken with Rice',
      image: 'meal_01.png',
      price: '70',
    },
    {
      name: 'Japanese Beef Bento',
      image: 'meal_02.png',
      price: '105',
    },
    {
      name: 'Dim Sum Set',
      image: 'meal_03.png',
      price: '70',
      active: true,
    },
    {
      name: 'Evian Natural Mineral Water 330ml',
      image: '',
      price: '15',
    },
  ],
  searchResultConnector: 'to',
  searchResultHasTypology: true,
  editSearchButtonText: 'Change search',
  summaryData: [{
    hasPriceResume: true,
    fareByFlight: false,
    moreDetailsCollapsible: true,
    moreDetailsStyle: 'byjourney',
    summaryCollapseTitle: 'Resumen',
    totalCostValue: '2,544,00',
    totalCostLabel: 'Total cost',
    legalTerms: '* 유류할증료와 세금을 포함한 총 운임으로 구매 시점과 환율에 따라 변동될 수 있습니다. * *위탁 수하물, 기내식, 유아 동반, 좌석 업그레이드 등 항공권 구매 이외 모든 보조 상품은 구매 후 취소 및 환불이 불가하오니 이점 유의해주시기 바랍니다.',
  }, ],
  sitemapIBE: [{
      section: 'Booking flow',
      pages: [{
          label: 'Select flight',
          url: 'BF-select_flight',
          notAvailable: false,
        },
        {
          label: 'Select flight (one way)',
          url: 'BF-select_flight_OW',
          notAvailable: false,
        },
        {
          label: 'Select flight (multicity)',
          url: 'BF-select_flight_multicity',
          notAvailable: false,
        },
        {
          label: 'Passenger data',
          url: 'BF-passenger_data',
          notAvailable: false,
        },
        {
          label: 'Passenger data',
          url: 'BF-passenger_data',
          notAvailable: false,
        },
        {
          label: 'Add extras',
          url: 'BF-services_add',
          notAvailable: false,
        },
        {
          label: 'Add extras (one way)',
          url: 'BF-services_add_oneway',
          notAvailable: false,
        },
        {
          label: 'Seat selection',
          url: 'BF-seat_selection',
          notAvailable: false,
        },
        {
          label: 'Payment',
          url: 'BF-payment',
          notAvailable: false,
        },
        {
          label: 'Payment confirmation',
          url: 'BF-payment_confirmation',
          notAvailable: false,
        },
        {
          label: 'Payment confirmation (one way)',
          url: 'BF-payment_confirmation_OW',
          notAvailable: false,
        },
        {
          label: 'Payment confirmation (QR)',
          url: 'BF-payment_confirmation_QR',
          notAvailable: false,
        },
      ],
    },
    {
      section: 'Manage my Booking',
      pages: [{
          label: 'Login',
          url: 'BF-MMB_login',
          notAvailable: false,
        },
        {
          label: 'Home',
          url: 'BF-MMB_home',
          notAvailable: false,
        },
        {
          label: 'Cancel Flight',
          url: 'BF-MMB_cancel_flight',
          notAvailable: false,
        },
        {
          label: 'Change Flight',
          url: 'BF-MMB_change_flight',
          notAvailable: false,
        },
        {
          label: 'Guest details',
          url: 'BF-MMB_passenger_data',
          notAvailable: false,
        },
      ],
    },
    {
      section: 'Check in',
      pages: [{
          label: 'Login',
          url: 'BF-WCI_login',
          notAvailable: false,
        },
        {
          label: 'Home',
          url: 'BF-WCI_home',
          notAvailable: false,
        },
        {
          label: 'Pre confirmation',
          url: 'BF-WCI_pre-confirmation',
          notAvailable: false,
        },
      ],
    },
    {
      section: 'Agent cabinet',
      pages: [{
          label: 'Login',
          url: 'AC_login',
          notAvailable: false,
        },
        {
          label: 'Home',
          url: 'AC_home',
          notAvailable: false,
        },
        {
          label: 'Account management',
          url: 'AC_account_management',
          notAvailable: false,
        },
        {
          label: 'MMB',
          url: 'AC_mmb',
          notAvailable: false,
        },
        {
          label: 'Refunds',
          url: 'AC_refunds',
          notAvailable: false,
        },
        {
          label: 'Payment',
          url: 'AC-payment',
          notAvailable: false,
        },
        {
          label: 'Payment hold proceed',
          url: 'AC_payment_hold_proceed',
          notAvailable: false,
        },
      ],
    },
    {
      section: 'U-fly',
      pages: [{
          label: 'Login',
          url: 'U-FLY_login',
          notAvailable: false,
        },
        {
          label: 'Forgot password',
          url: 'U-FLY_forgot_password',
          notAvailable: false,
        },
        {
          label: 'Sign up',
          url: 'U-FLY_sign_up',
          notAvailable: false,
        },
        {
          label: 'Confirmation',
          url: 'U-FLY_confirmation',
          notAvailable: false,
        },
        {
          label: 'Profile update',
          url: 'U-FLY_profile_update',
          notAvailable: false,
        },
      ],
    },
    {
      section: 'Error',
      pages: [{
        label: 'Error 404',
        url: '404',
        notAvailable: false,
      }, ],
    },
  ],
  sitemapCorp: [{
      section: 'Pages',
      pages: [{
          label: 'Home',
          url: 'CORP-index',
          notAvailable: false,
        },
        {
          label: 'Sign up',
          url: 'CORP-sign_up',
          notAvailable: false,
        },
        {
          label: 'Sign in',
          url: 'CORP-sign_in',
          notAvailable: false,
        },
        {
          label: 'Article with image list',
          url: 'CORP-article_with_img_list',
          notAvailable: false,
        },
        {
          label: 'Article with table and note',
          url: 'CORP-article_with_table_note',
          notAvailable: false,
        },
        {
          label: 'Article with complex table',
          url: 'CORP-article_with_complex_table',
          notAvailable: false,
        },
        {
          label: 'Article with anchors',
          url: 'CORP-fast_navigation_anchors',
          notAvailable: false,
        },
        {
          label: 'Destinations guide',
          url: 'CORP-destination-guides',
          notAvailable: false,
        },
        {
          label: 'Destination guide (city)',
          url: 'CORP-destination_guides_city',
          notAvailable: false,
        },
        {
          label: 'News list',
          url: 'CORP-news_list',
          notAvailable: false,
        },
        {
          label: 'News detail',
          url: 'CORP-news_detail',
          notAvailable: false,
        },
      ],
    },
    {
      section: 'Landings',
      pages: [{
        label: 'Landing page 01',
        url: 'CORP-LANDING-01',
        notAvailable: false,
      }, ],
    },
    {
      section: 'Components (HTML)',
      pages: [{
          label: 'Header (home)',
          url: 'COMP-CORP-header-home',
          notAvailable: false,
        },
        {
          label: 'Header (inner pages)',
          url: 'COMP-CORP-header-page',
          notAvailable: false,
        },
        {
          label: 'Main banner',
          url: 'COMP-CORP-main_banner',
          notAvailable: false,
        },
        {
          label: 'Multiple panel',
          url: 'COMP-CORP-multiple-panel',
          notAvailable: false,
        },
        {
          label: 'Destinations offers',
          url: 'COMP-CORP-destinations-offers',
          notAvailable: false,
        },
        {
          label: 'Main offers',
          url: 'COMP-CORP-main-offers',
          notAvailable: false,
        },
        {
          label: 'Footer',
          url: 'COMP-CORP-main_footer',
          notAvailable: false,
        },
        {
          label: 'Sign in',
          url: 'COMP-CORP-sign-in',
          notAvailable: false,
        },
      ],
    },
    {
      section: 'UI-Elements (HTML)',
      pages: [{
          label: 'Accordion',
          url: 'COMP-CORP-accordion',
          notAvailable: false,
        },
        {
          label: 'Headings',
          url: 'COMP-CORP-headings',
          notAvailable: false,
        },
        {
          label: 'Paragraphs',
          url: 'COMP-CORP-paragraphs',
          notAvailable: false,
        },
        {
          label: 'Tables',
          url: 'COMP-CORP-tables',
          notAvailable: false,
        },
        {
          label: 'Lists',
          url: 'COMP-CORP-lists',
          notAvailable: false,
        },
        {
          label: 'Images',
          url: 'COMP-CORP-image',
          notAvailable: false,
        },
        {
          label: 'Pagination',
          url: 'COMP-CORP-pagination',
          notAvailable: false,
        },
      ],
    },
  ],
  sitemapMD: [{
      section: 'Components (HTML)',
      pages: [{
          label: 'Flight search',
          url: 'COMP-flight_search',
          notAvailable: false,
        },
        {
          label: 'Booking steps',
          url: 'COMP-booking_steps',
          notAvailable: false,
        },
        {
          label: 'Summary',
          url: 'COMP-summary',
          notAvailable: false,
        },
        {
          label: 'Footer (ibe)',
          url: 'COMP-footer',
          notAvailable: false,
        },
        {
          label: 'Payment details table',
          url: 'COMP-payment-detail-tables',
          notAvailable: false,
        },
      ],
    },
    {
      section: 'UI-Elements (HTML)',
      pages: [{
          label: 'Grid',
          url: 'COMP-grid',
          notAvailable: false,
        },
        {
          label: 'Colors',
          url: 'COMP-colors.html',
          notAvailable: false,
        },
        {
          label: 'Font icons',
          url: '../src/assets/fonts/icons/demo',
          notAvailable: false,
        },
        {
          label: 'Headings (h1,h2,h3...)',
          url: 'COMP-headings',
          notAvailable: false,
        },
        {
          label: 'Buttons',
          url: 'COMP-buttons',
          notAvailable: false,
        },
        {
          label: 'Form elements',
          url: 'COMP-form_elements',
          notAvailable: false,
        },
        {
          label: 'Modals',
          url: 'COMP-modals',
          notAvailable: false,
        },
      ],
    },
  ],
}
// local project script to simulate Angular functions
$(document).ready(function () {
    var W = $(window).outerWidth();
    var body = $('body');
    var stickyMainHeader, stickyCombinedBar;


    // imports
    // ----------------------------------------------------
    //## SEARCH 
    // ----------------------------------------------------
    // searchInteraction(true, 640, -220);
    stickyMainHeader = 120; // add class is-fixedheader main header
    // navResponsiveBottom();
    // weekSelector(W, 640)
    // faresSelectionCollapse();
    // tripSelector();
    // summaryCollapse();
    // overlaySummaryClose();
    // notification(2600, 540);
    // compareFaresTrigger();
    // connectionsModal();
    // webSearch();

    // search and header fixed
    stickyCombinedBar = mainHeaderSearchFixed(stickyMainHeader);
    $(window).scroll(function () {
        // fixed combined bar (summary) on scroll
        // searchFixed(stickyCombinedBar);
        // fixed main header on scroll
        // headerFixed(stickyMainHeader);
    });

    // temporal mobile multiple panel
    var multipanelTrigger = $('.multiple-panel_collapse_trigger');
    multipanelTrigger.on('click', function () {
        if ($(this).attr('aria-expanded') === 'false') {
            multipanelTrigger.attr('aria-expanded', false);
            $(this).attr('aria-expanded', true);
        } else {
            $(this).attr('aria-expanded', false);
        }
    });
});
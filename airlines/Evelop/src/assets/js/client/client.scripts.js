(function() {
    if (sessionStorage.fonts) {
        document.documentElement.classList.add('wf-active');
    }
})();

WebFontConfig = {
    google: {
        families: ['Montserrat:300,400,600,700']
    },
    active: function() {
        sessionStorage.fonts = true;
    }
};

(function(d) {
    var wf = d.createElement('script'), s = d.scripts[0];
    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
    wf.async = true;
    s.parentNode.insertBefore(wf, s);
})(document);
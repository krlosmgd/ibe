// local project script to simulate Angular functions
$(document).ready(function () {
    var W = $(window).outerWidth();
    var body = $('body');
    var stickyMainHeader, stickyCombinedBar;

    // ----------------------------------------------------
    //## SEARCH 
    // ----------------------------------------------------
    searchInteraction();
    stickyMainHeader = 10; // add class is-fixedheader main header

    /*** SUMMARY ***/
    // ------------------------------------
    // show/hide summary (sliding from right side)
    navResponsiveBottom();
    weekSelector(W, 640);
    faresSelectionCollapse();
    tripSelector();
    summaryCollapse();
    overlaySummaryClose();

    // search and header fixed
    stickyCombinedBar = mainHeaderSearchFixed(stickyMainHeader);
    $(window).scroll(function () {
        // fixed combined bar (summary) on scroll
        searchFixed(stickyCombinedBar);
        // efect fixed main header on scroll
        headerFixed(stickyMainHeader);
    });


    // TO REVIEW: 
    // ABREVIATE DAY SELECTOR SLIDER WEEK NAME
    if ($('.week').length > 0) {
        $('.week').each(function () {
            var weekText = $(this).text();
            if (W < 640) {
                len = $(this).text().replace(/ /g, '').length;
                $(this).text($(this).text().substr(0, 3));
            } else {
                $(this).text(weekText);
            }
        });
    }
});
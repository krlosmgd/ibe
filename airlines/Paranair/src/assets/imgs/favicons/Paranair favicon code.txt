https://realfavicongenerator.net

1. Download your package:
2. Extract this package in the root of your web site. If your site is http://www.example.com, you should be able to access a file named http://www.example.com/favicon.ico.
3. Insert the following code in the <head> section of your pages:


<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#cd1025">
<meta name="msapplication-TileColor" content="#0037a2">
<meta name="theme-color" content="#ffffff">

4. Optional - Once your web site is deployed, check your favicon.
https://realfavicongenerator.net/favicon_checker#.Wp5ulGY-_OQ

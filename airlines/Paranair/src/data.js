const data = {
  projectName: 'Paranair',
  site_title: 'Paranair [HTML]',
  demo_imgs_path: '../src/assets/imgs/',
  core_assets_path: '../../../core/src/assets/',
  project_assets_path: '../src/assets/',
  logo_img: 'logo.svg',
  logo_negative_img: 'logo-neg.svg',
  pathFont: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700&amp;subset=latin-ext',
  currencyPosition: 'left',
  SHARED: {
    currency: 'PYG',
    weightmeasure: 'Kg',
    promoLabelDefault: 'Promo',
    formRequiredSymbol: '*',
    formRequiredText: '* Campos obligatorios',
    from: 'Desde',
    day: 'día',
    nextFlight: 'Siguiente vuelo',
    flightNotAvailable: 'El vuelo no está disponible.<br/>El horario de salida y el de regreso no coinciden.',
  },
  showUserAccount: false,
  header: {
    headerRight: {
      show: true,
      items: [
        {
          dataType: 'languages',
        },
      ],
    },
    nav: {
      itemsAlign: 'left',
      items: [
        {
          caption: 'Reservar',
        },
        {
          caption: 'Gestionar',
        },
        {
          caption: 'Información',
        },
      ],
    },
  },
  navFixed: {
    ibe: false,
    corporate: false
  },
  footer: {
    logo: {
      isNeg: true,
    },
  },
  multipleSearch: {
    show: false,
    active: 1,
    items: [
      {
        link: '',
        label: 'Search for flights',
        icon: 'icon--flights',
        controls: 'multipleSearchFlights_tab',
        id: 'multipleSearchFlights',
        tabindex: -1,
        contentInclude: 'partials/components/IBE/flight-search/_flight-search.njk',
      },
      {
        link: {
          url: '',
          title: 'Open in new window',
          target: '_blank',
        },
        label: 'Search for Hotels',
        icon: 'icon--hotels',
        controls: 'multipleSearchHotels_tab',
        id: 'multipleSearchHotels',
        tabindex: -1,
      },
      {
        link: {
          url: '',
          title: '',
        },
        label: 'Search for Flights and Hotels',
        icon: 'icon--flightshotels',
        controls: 'multipleSearchHotelsFlights_tab',
        id: 'multipleSearchHotelsFlights',
        tabindex: -1,
      },
    ],
  },
  bookingSteps: [
    {
      stepNumber: '1',
      stepLabel: 'Vuelo',
      stepUrl: '',
    },
    {
      stepNumber: '2',
      stepLabel: 'Pasajeros',
      stepUrl: '',
    },
    {
      stepNumber: '3',
      stepLabel: 'Pago',
      stepUrl: '',
    },
  ],
  multipleCalendarOptions: false,
  months: [
    {
      month: 'Diciembre',
      year: '2017',
    },
    {
      month: 'Enero',
      year: '2018',
      selected: true,
    },
    {
      month: 'Febrero',
      year: '2018',
    },
    {
      month: 'Marzo',
      year: '2018',
    },
    {
      month: 'Abril',
      year: '2018',
    },
    {
      month: 'Mayo',
      year: '2018',
    },
    {
      month: 'Junio',
      year: '2018',
    },
    {
      month: 'Julio',
      year: '2018',
    },
    {
      month: 'Agosto',
      year: '2018',
    },
    {
      month: 'Septiembre',
      year: '2018',
    },
  ],
  days: [
    {
      day: '24 AGO',
      week: 'Domingo',
      price: '',
    },
    {
      day: '25 AGO',
      week: 'Lunes',
      price: '',
      selected: true,
    },
    {
      day: '26 AGO',
      week: 'Martes',
      price: '',
    },
    {
      day: '27 AGO',
      week: 'Miércoles',
      price: '',
    },
    {
      day: '28 AGO',
      week: 'Jueves',
      price: '',
      bestprice: true,
    },
    {
      day: '29 AGO',
      week: 'Viernes',
      price: '',
    },
    {
      day: '30 AGO',
      week: 'Sábado',
      price: '',
    },
    {
      day: '31 AGO',
      week: 'Domingo',
      price: '',
    },
    {
      day: '01 Jan',
      week: 'Lunes',
      price: '',
    },
    {
      day: '02 Jan',
      week: 'Martes',
      price: '',
    },
    {
      day: '03 Jan',
      week: 'Miércoles',
      price: '',
    },
  ],
  journeys: [
    {
      'class': 'outbound',
      way: 'Ida',
      headerConnector: '-',
      headerImgSrc: '',
      departureAirport: 'Asunción',
      departureAirportCode: 'ASU',
      departureAirportTerminal: 'Terminal 1',
      arrivalAirport: 'Ciudad del Este',
      arrivalAirportCode: 'AGT',
      arrivalAirportTerminal: 'Terminal 4',
      date: {
        week: 'miércoles',
        weekAbbr: 'Mie',
        day: '8',
        month: 'Agosto',
        monthAbbr: 'Ago',
        year: '2018',
      },
      duration: '2:10h',
      //## connections
      /*
        nonstop: flight from A to B without stops
        direct: flight from A to B with at least one stop but not flight changes
        connecting: flight with stop and flight number change
        stopover: flight with a technical stop (ex.: to refuel)
      */
      connections: {
        extraDay: '+1',
        nonstop: {
          style: '',
          text: '',
        },
        direct: {
          style: 'modal',
          text: '1 stop',
        },
        connecting: {
          style: '',
          text: '',
        },
        stopover: {
          style: 'tooltip',
          text: '1 stop + 1 technical stop',
        },
      },
      flightNumbers: [
        {
          number: '809',
          operator: 'Z8',
        },
        {
          number: '816',
          operator: 'Z8',
        },
      ],
      fare: 1,
      fareSelectedName: 'SuperParanair',
    },
    {
      'class': 'inbound',
      way: 'Vuelta',
      headerConnector: '-',
      headerImgSrc: '',
      departureAirport: 'Ciudad del Este',
      departureAirportCode: 'AGT',
      departureAirportTerminal: 'Terminal 2',
      arrivalAirport: 'Asunción',
      arrivalAirportCode: 'ASU',
      arrivalAirportTerminal: 'Terminal 4',
      date: {
        week: 'miércoles',
        weekAbbr: 'Mie',
        day: '15',
        month: 'Ago',
        year: '2018',
      },
      // connections: {
      //   extraDay: '',
      //   nonstop: {
      //     style: '',
      //     text: '',
      //   },
      //   direct: {
      //     style: '',
      //     text: '',
      //   },
      //   connecting: {
      //     style: '',
      //     text: '',
      //   },
      //   stopover: {
      //     style: '',
      //     text: '',
      //   },
      // },
      flightNumbers: [
        {
          number: '811',
          operator: 'Z8',
        },
      ],
      fare: 3,
      fareSelectedName: 'Paranair plus',
    },
  ],
  flightCodeTimeSeparator: '',
  flightCaption: 'Vuelo',
  flightLabelOutbound: 'Salida',
  flightLabelInbound: 'Llegada',
  flightExtraDaysPosition: 'left',
  flightSelectedFareButtonSelectable: true,
  flightScale: {
    title: 'Detalle del vuelo',
    subtitle: 'Jueves 06 Septiembre 2018',
    timeTotalText: 'Tiempo total del viaje:',
    timeTotal: '14h 00m',
    departureTime: '10:30',
    departureAirport: 'Asunción',
    departureAirportCode: 'ASU',
    departureDuration: '2h 20m',
    flightNumberDeparture: 'Vuelo Z8 809',
    scales: [
      {
        title: 'Argentina',
        time: '12:00',
        departureAirportCode: 'AR',
        transit: {
          time: '10h 10m',
          title: 'Transito en ciudad del Este',
        },
      },
      {
        title: 'Argentina',
        time: '22:20',
        departureAirportCode: 'AR',
        isFinalizeFlight: true,
      },
    ],
    arrivalTime: '14:00',
    arrivalAirport: 'Ciudad del Este',
    arrivalAirportCode: 'AGT',
    arrivalDuration: '1h 30m',
    flightNumberArrival: 'Vuelo Z8 811',
  },
  flights: {
    items: [
      {
        departureTime: '10:34',
        departureAirport: 'Asunción',
        departureAirportCode: 'ASU',
        departureAirportTerminal: 'Terminal 2',
        duration: '2:10h',
        extraDay: '',
        connections: {
          nonstop: {
            style: '',
            text: '',
          },
          direct: {
            style: 'modal',
            text: 'Direct',
          },
          connecting: {
            style: 'tooltip',
            text: '1 parada',
          },
          stopover: {
            style: '',
            text: '',
          },
        },
        arrivalTime: '14:00',
        arrivalAirport: 'Ciudad del Este',
        arrivalAirportCode: 'AGT',
        arrivalAirportTerminal: 'Terminal 2',
        priceFrom: '1.115.000',
        connection: false,
        fareSelected: false,
        fareSelectedName: 'Paranair Óptima',
        fareType: 'fare2',
        hasPromo: false,
        unavailable: true,
      },
      {
        departureTime: '11:40',
        departureAirport: 'Asunción',
        departureAirportCode: 'ASU',
        departureAirportTerminal: 'Terminal 2',
        duration: '0:40h',
        //## connections 
        /*
          nonstop: flight from A to B without stops
          direct: flight from A to B with at least one stop but not flight changes
          connecting: flight with stop and flight number change
          stopover: flight with a technical stop (ex.: to refuel)
        */
        extraDay: '',
        connections: {
          nonstop: {
            style: '',
            text: '',
          },
          direct: {
            style: 'modal',
            text: '1 stop',
          },
          connecting: {
            style: '',
            text: '',
          },
          stopover: {
            style: 'tooltip',
            text: '1 stop + 1 technical stop',
          },
        },
        arrivalTime: '12:20',
        arrivalAirport: 'Ciudad del Este',
        arrivalAirportCode: 'AGT',
        arrivalAirportTerminal: 'Terminal 2',
        priceFrom: '1.115.000',
        connection: true,
        fareSelected: false,
        fareSelectedName: 'Paranair Plus',
        fareType: 'fare3',
        hasPromo: false,
      },
      {
        departureTime: '14:30',
        departureAirport: 'Asunción',
        departureAirportCode: 'ASU',
        duration: '2:00h',
        extraDay: '+1',
        connections: {
          nonstop: {
            style: '',
            text: '',
          },
          direct: {
            style: '',
            text: '',
          },
          connecting: {
            style: 'modal',
            text: '1 Stop',
          },
          stopover: {
            style: '',
            text: '',
          },
        },
        arrivalTime: '16:00',
        arrivalAirport: 'Ciudad del Este',
        arrivalAirportCode: 'AGT',
        priceFrom: '1.115.000',
        connection: true,
        fareSelected: false,
        fareSelectedName: 'Paranair Óptima',
        fareType: 'fare2',
        hasPromo: false,
      },
      {
        departureTime: '11:40',
        departureAirport: 'Asunción',
        departureAirportCode: 'ASU',
        departureAirportTerminal: 'Terminal 2',
        duration: '0:40h',
        extraDay: '+1',
        connections: {
          nonstop: {
            style: '',
            text: '',
          },
          direct: {
            style: '',
            text: '',
          },
          connecting: {
            style: 'modal',
            text: '1 Stop',
          },
          stopover: {
            style: '',
            text: '',
          },
        },
        arrivalTime: '12:00',
        arrivalAirport: 'Ciudad del Este',
        arrivalAirportCode: 'AGT',
        arrivalAirportTerminal: 'Terminal 2',
        priceFrom: '1.115.000',
        connection: true,
        fareSelected: false,
        fareSelectedName: 'Paranair Plus',
        fareType: 'fare3',
        hasPromo: false,
      },
    ],
  },
  paxSelector: {
    title: 'Pasajeros',
    label: 'Asiento',
    nextButton: '',
  },
  passengersData: {
    passengers: [
      {
        label: 'Adulto 1',
        gender: '',
        firstName: 'Jorge',
        lastName: 'García',
        type: 'Adulto',
        seat: '4B',
        seatStatus: '',
        checkedIn: true,
        hasInfoButton: false,
      },
      {
        label: 'Niño 1',
        gender: '',
        firstName: 'Carmen',
        lastName: 'María Galeano',
        type: 'Niño',
        seat: '4A',
        seatStatus: 'selected',
        statusInput: 'has-error',
        messageInput: 'Error message',
        checkedIn: false,
        hasInfoButton: false,
      },
    ],
  },
  seatmap: {
    seatsHasPopover: true,
    letters: [
      {
        letter: 'A',
      },
      {
        letter: 'C',
      },
      {
        letter: '',
      },
      {
        letter: 'D',
      },
      {
        letter: 'F',
      },
    ],
    seats: [
      {
        seats_group: [
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '1',
          },
          {
            status: 'empty',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'empty',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '2',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '3',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '4',
          },
          {
            status: 'current',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'selected',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '5',
          },
          {
            status: 'selected',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'selected',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '6',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '7',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        hasExitLeft: true,
        hasExitRight: true,
        seats_group: [
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '8',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '9',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '10',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '11',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '12',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '13',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '14',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
    ],
  },
  seatsLegend: {
    hasResponsiveModal: false,
    desc: {
      title: '',
    },
    footnote: '',
    items: [
      {
        type: '',
        text: 'Disponible',
        price: '',
        description: '',
      },
      {
        type: 'exit',
        text: 'Salida de emergencia',
        price: '',
        description: '',
      },
      {
        type: 'unavailable',
        text: 'No Disponible',
        price: '',
        description: '',
      },
      {
        type: 'selected',
        text: 'Seleccionado',
        price: '',
        description: '',
      },
    ],
  },
  fares: {
    faresLayoutTabs: false,
    faresClickUrl: '',
    localText: 'Doméstico',
    internationalText: 'Internacional',
    notApplyFareText: 'Sin cargo',
    fareText: 'Cargo',
    currency: 'USD',
    compareFares: false,
    items: [
      {
        pos: '1',
        name: 'SuperParanair',
        isPromo: false,
        price: '325',
        unavailable: true,
        fareButtonText: 'Agotado',
        unavailable: true,
        farePrice: '',
        fareDescription: [
          {
            icon: '',
            description: 'Equipaje de mano: <strong>1 bulto</strong>',
          },
          {
            icon: '',
            description: 'Equipaje en bodega: <strong>23 kg</strong>',
          },
          {
            icon: '',
            description: 'Elección de asiento',
          },
          {
            icon: '',
            description: 'Cargo por Cambio',
            subItems: {
              items: [
                {
                  description: '<strong>USD 50</strong> (Doméstico)',
                },
                {
                  description: '<strong>USD 100</strong> (Internacional)',
                },
              ],
            },
          },
          {
            icon: '',
            description: 'Cargo por No Show',
            subItems: {
              items: [
                {
                  description: '<strong>USD 70</strong> (Doméstico)',
                },
                {
                  description: '<strong>USD 120</strong> (Internacional)',
                },
              ],
            },
          },
          {
            icon: '',
            description: 'Devolución <strong>USD 150</strong>',
          },
        ],
      },
      {
        pos: '2',
        name: 'Paranair Óptima',
        isPromo: false,
        price: '758',
        unavailable: true,
        fareButtonText: 'Deshabilitado',
        farePrice: '',
        fareDescription: [
          {
            icon: '',
            description: 'Equipaje de mano: <strong>1 bulto</strong>',
          },
          {
            icon: '',
            description: 'Equipaje en bodega: <strong>23 kg</strong>',
          },
          {
            icon: '',
            description: 'Elección de asiento',
          },
          {
            icon: '',
            description: 'Cargo por Cambio',
            subItems: {
              items: [
                {
                  description: '<strong>USD 50</strong> (Doméstico)',
                },
                {
                  description: '<strong>USD 60</strong> (Internacional)',
                },
              ],
            },
          },
          {
            icon: '',
            description: 'Cargo por No Show',
            subItems: {
              items: [
                {
                  description: '<strong>USD 50</strong> (Doméstico)',
                },
                {
                  description: '<strong>USD 80</strong> (Internacional)',
                },
              ],
            },
          },
          {
            icon: '',
            description: 'Devoluciónv <strong>USD 100</strong>',
          },
        ],
      },
      {
        pos: '3',
        name: 'Paranair Plus',
        isPromo: false,
        price: '2.252.750',
        unavailable: false,
        fareButtonText: '',
        farePrice: '2.252.750',
        fareDescription: [
          {
            icon: '',
            description: 'Equipaje de mano:  <strong>1 bulto</strong>',
          },
          {
            icon: '',
            description: 'Equipaje en bodega: <strong>23 kg</strong>',
          },
          {
            icon: '',
            description: 'Elección de asiento',
          },
          {
            icon: '',
            description: '<strong>Sin cargo</strong> por Cambio',
          },
          {
            icon: '',
            description: '<strong>Sin cargo</strong> por No Show',
          },
          {
            icon: '',
            description: 'Devolución <strong>USD 50</strong>',
          },
        ],
      },
    ],
  },
  baggages: [
    {
      title: 'Departing Baggage',
      type: [
        {
          price: '229',
          size: '20',
          promo: true,
        },
        {
          price: '360',
          size: '25',
          promo: true,
        },
        {
          price: '365',
          size: '30',
          promo: true,
        },
      ],
    },
    {
      title: 'Returning Baggagee',
      type: [
        {
          price: '365',
          size: '30',
          promo: false,
        },
      ],
    },
  ],
  baggagesOW: [
    {
      title: '',
      type: [
        {
          price: '229',
          size: '20',
          promo: true,
        },
        {
          price: '360',
          size: '25',
          promo: true,
        },
        {
          price: '365',
          size: '30',
          promo: true,
        },
      ],
    },
  ],
  specialBaggages: [
    {
      title: 'Departing Oversized Baggage',
      type: [
        {
          price: '229',
          size: '20',
          promo: true,
        },
        {
          price: '360',
          size: '25',
          promo: true,
        },
        {
          price: '365',
          size: '30',
          promo: true,
        },
      ],
    },
    {
      title: 'Returning Oversized Baggage',
      type: [
        {
          price: '365',
          size: '30',
          promo: false,
        },
      ],
    },
  ],
  specialBaggagesOW: [
    {
      type: [
        {
          price: '229',
          size: '20',
          promo: true,
        },
        {
          price: '360',
          size: '25',
          promo: true,
        },
        {
          price: '365',
          size: '30',
          promo: true,
        },
      ],
    },
  ],
  meals: [
    {
      name: 'Thai Red Curry Chicken with Rice Thai Red Curry Chicken with Rice',
      image: 'meal_01.png',
      price: '70',
    },
    {
      name: 'Japanese Beef Bento',
      image: 'meal_02.png',
      price: '105',
    },
    {
      name: 'Dim Sum Set',
      image: 'meal_03.png',
      price: '70',
      active: true,
    },
    {
      name: 'Evian Natural Mineral Water 330ml',
      image: '',
      price: '15',
    },
  ],
  searchResultConnector: '-',
  searchResultHasTypology: false,
  editSearchButtonText: 'Editar',
  summaryData: [
    {
      //## connection position
      /*
        1 - right side of flight number
        2 - center below flight connector icon
      */
      connectionsPosition: 1,
      isCollapse: false,
      hasPriceResume: false,
      hasButtonContinue: false, // button continue next step
      fareByFlight: true,
      showBottomTotal: true,
      showBottomTotalLabel: 'Total',
      buttonClose: 'Cerrar',
      moreDetailsCollapsible: false,
      moreDetailsStyle: 'simple',
      summaryCollapseTitle: 'Resumen',
      totalCostValue: '1.115.000,00',
      totalCostLabel: '',
      legalTerms: '',
    },
  ],
  summaryTypology: {
    showTitle: false,
    isCollapsible: true,
    totalPrice: '1.115.000,00',
    resumePrices: [
      [
        {
          label: '1 x Tarifa adulto',
          price: 'PYG 1.000.000',
        },
        {
          label: '1 x Tarifa niño',
          price: 'PYG 1.000.000',
        },
        {
          label: '1 x Tarifa bebé',
          price: 'PYG 1.000.000',
        },
      ],
      [
        {
          label: '1 x Tasas y/o impuestos adulto',
          price: 'PYG 1.000.000',
        },
        {
          label: '1 x Tasas y/o impuestos niño',
          price: 'PYG 115.000',
        },
        {
          label: '1 x Tasas y/o impuestos bebé',
          price: 'PYG 115.000',
        },
      ],
    ],
  },
  summaryDetailServices: {
    caption: 'Summary details - services',
    thead: {
      ths: [
        {
          title: 'Servicios',
        },
      ],
    },
    tbody: [
      {
        tds: [
          {
            title: 'Asiento',
            isTh: true,
          },
          {
            title: 'Incluido',
          },
        ],
      },
      {
        tds: [
          {
            title: 'Equipaje de mano: 1 bulto',
            isTh: true,
          },
          {
            title: 'Incluido',
          },
        ],
      },
      {
        tds: [
          {
            title: 'Equipaje en bodega: 23Kg',
            isTh: true,
          },
          {
            title: 'Incluido',
          },
        ],
      },
    ],
  },
  bookingReference: {
    label: 'Código de reserva',
    code: 'jipoym',
  },
  segmentSelectorIsTabs: true,
  segmentSelector: {
    hasButton: true,
    alertText: '',
    flightLabel: '',
    flights: [
      {
        from: 'SRE',
        to: 'VVI',
        fromCity: 'Sucre',
        toCity: 'Sant Cruz',
        date: 'Miércoles 28 Agosto 2018',
        flightNumber: 'Z8 821',
        disabled: false,
        active: true,
      },
      {
        from: 'VVI',
        to: 'LPB',
        fromCity: 'Santa Cruz',
        toCity: 'El Alto',
        date: 'Sábado 31 Agosto 2018',
        flightNumber: 'PR0044',
        disabled: false,
        active: false,
      },
      {
        from: 'LPB',
        to: 'VVI',
        fromCity: 'El Alto',
        toCity: 'Santa Cruz',
        date: 'Sábado 31 Agosto 2018',
        flightNumber: 'PR0049',
        disabled: false,
        active: false,
      },
      {
        from: 'VVI',
        to: 'SRE',
        fromCity: 'Santa Cruz',
        toCity: 'Sucre',
        date: 'Sábado 31 Agosto 2018',
        flightNumber: 'PR0054',
        disabled: false,
        active: false,
      },
    ],
  },
  paymentData: {
    methods: {
      items: [
        {
          isCurrent: true,
          name: 'Credit Card',
          hasCurrencyOptions: false,
          title: '',
          titleTag: '',
          id: 'creditcardCollapse',
          image: 'creditcard.svg',
          imageSelected: 'creditcard-selected.svg',
          template: '_cc',
        },
      ],
    },
    paymentSummary: {
      title: 'Resumen',
      body: {
        show: false,
        items: [
          {
            title: 'Total precio',
            price: '1.000.000',
          },
          {
            title: 'Total Paid',
            price: '0',
          },
          {
            title: 'Reservación',
            price: '115.000',
          },
        ],
      },
      footer: {
        items: [
          {
            title: 'Total precio',
          },
          {
            price: '1.115.000',
          },
        ],
      },
    },
  },
  creditcard: {
    title: 'credit card options',
    options: [
      {
        label: 'Visa',
        id: 'ccTypeVisa',
        name: 'creditCardType',
        imageURL: 'payment-methods/credit-cards/visa.svg',
        imageAlt: 'Visa payment',
      },
      {
        label: 'Mastercard',
        id: 'ccTypeMasterCard',
        name: 'creditCardType',
        imageURL: 'payment-methods/credit-cards/mastercard.svg',
        imageAlt: '',
      },
      {
        label: 'American express',
        id: 'ccTypeAmerican',
        name: 'creditCardType',
        imageURL: 'payment-methods/credit-cards/american-express.svg',
        imageAlt: '',
      },
      {
        label: 'Diners Club',
        id: 'ccTypeDiners',
        name: 'creditCardType',
        imageURL: 'payment-methods/credit-cards/diners-club.svg',
        imageAlt: '',
      },
    ]
  },
  CreditCardFormData: {
    msgRequired: '* Campos obligatorios',
    formGroups: [
      {
        elements: [
          {
            elementsGroup: [
              {
                type: 'inputForm',
                element: {
                  label: 'Número de tarjeta*',
                },
              },
              {
                type: 'inputForm',
                element: {
                  label: 'Titular de la tarjeta*',
                },
              },
              {
                type: 'selectForm',
                element: {
                  label: 'Fecha de caducidad*',
                  id: 'ccExpiryDate',
                  options: [
                    {
                      name: 'MM',
                      id: 'ccExpiryDateMM',
                    },
                    {
                      name: 'AAAA',
                      id: 'ccExpiryDateYYYY',
                    },
                  ],
                },
              },
              {
                type: 'inputForm',
                element: {
                  label: 'CVV*',
                  tooltip: 'Tooltip text goes here',
                },
              },
            ],
          },
        ],
      },
    ],
  },
  sitemapIBE: [
    {
      section: 'Booking flow',
      available: true,
      pages: [
        {
          url: 'BF-select_flight.html',
          label: `Select flight`,
          notAvailable: false,
        },
        {
          label: 'Select flight (one way)',
          url: 'BF-select_flight_OW.html',
          notAvailable: false,
        },
        {
          label: 'Select flight (multicity)',
          url: 'BF-select_flight_multicity.html',
          notAvailable: true,
        },
        {
          label: 'Passenger data',
          url: 'BF-passenger_data.html',
        },
        {
          label: 'Add extras',
          url: 'BF-services_add.html',
          notAvailable: true,
        },
        {
          label: 'Add extras (one way)',
          url: 'BF-services_add_oneway.html',
          notAvailable: true,
        },
        {
          label: 'Seat selection',
          url: 'BF-seat_selection.html',
          notAvailable: true,
        },
        {
          label: 'Payment',
          url: 'BF-payment.html',
          notAvailable: false,
        },
        {
          label: 'Payment confirmation',
          url: 'BF-payment_confirmation.html',
          notAvailable: false,
        },
        {
          label: 'Payment confirmation (one way)',
          url: 'BF-payment_confirmation_OW.html',
          notAvailable: false,
        },
        {
          label: 'Payment confirmation (QR)',
          url: 'BF-payment_confirmation_QR.html',
          notAvailable: true,
        },
        {
          label: 'Invoice login',
          url: 'BF-invoice_login.html',
          notAvailable: false,
        },
      ],
    },
    {
      section: 'Manage my Booking',
      pages: [
        {
          label: 'Login',
          url: 'BF-MMB_login.html',
          notAvailable: true,
        },
        {
          label: 'Home',
          url: 'BF-MMB_home.html',
          notAvailable: true,
        },
        {
          label: 'Cancel Flight',
          url: 'BF-MMB_cancel_flight.html',
          notAvailable: true,
        },
        {
          label: 'Change Flight',
          url: 'BF-MMB_change_flight.html',
          notAvailable: true,
        },
        {
          label: 'Guest details',
          url: 'BF-MMB_passenger_data.html',
        },
      ],
    },
    {
      section: 'Check in',
      available: true,
      pages: [
        {
          label: 'Login',
          url: 'BF-WCI_login.html',
        },
        {
          label: 'Itinerary',
          url: 'BF-WCI_home.html',
        },
        {
          label: 'Itinerary (one way)',
          url: 'BF-WCI_home-OW.html',
        },
        {
          label: 'Passengers data',
          url: 'BF-WCI_passenger_data.html',
          notAvailable: false,
        },
        {
          label: 'Seats',
          url: 'BF-WCI_seat_selection.html',
          notAvailable: false,
        },
        {
          label: 'Pre-Confirmation',
          url: 'BF-WCI_pre-confirmation.html',
          notAvailable: true,
        },
        {
          label: 'Pre-Confirmation (one way)',
          url: 'BF-WCI_pre-confirmation-OW.html',
          notAvailable: true,
        },
        {
          label: 'Boarding pass',
          url: 'BF-WCI_home_checkedin.html',
          notAvailable: false,
        },
        {
          label: 'Boarding pass (disabled)',
          url: 'BF-WCI_home_checkedin-disabled.html',
          notAvailable: false,
        },
      ],
    },
    {
      section: 'Agent cabinet',
      pages: [
        {
          label: 'Login',
          url: 'AC_login.html',
          notAvailable: true,
        },
        {
          label: 'Home',
          url: 'AC_home.html',
          notAvailable: true,
        },
        {
          label: 'Account management',
          url: 'AC_account_management.html',
          notAvailable: true,
        },
        {
          label: 'MMB',
          url: 'AC_mmb.html',
          notAvailable: true,
        },
        {
          label: 'Refunds',
          url: 'AC_refunds.html',
          notAvailable: true,
        },
        {
          label: 'Payment',
          url: 'AC-payment.html',
          notAvailable: true,
        },
        {
          label: 'Payment hold proceed',
          url: 'AC_payment_hold_proceed.html',
          notAvailable: true,
        },
      ],
    },
    {
      section: 'Members',
      pages: [
        {
          label: 'Login',
          url: 'U-FLY_login.html',
          notAvailable: true,
        },
        {
          label: 'Forgot password',
          url: 'U-FLY_forgot_password.html',
          notAvailable: true,
        },
        {
          label: 'Sign up',
          url: 'U-FLY_sign_up.html',
          notAvailable: true,
        },
        {
          label: 'Confirmation',
          url: 'U-FLY_confirmation.html',
          notAvailable: true,
        },
        {
          label: 'Profile update',
          url: 'U-FLY_profile_update.html',
          notAvailable: true,
        },
      ],
    },
    {
      section: 'Error pages',
      available: true,
      pages: [
        {
          label: 'Error 404',
          url: '404.html',
          notAvailable: false,
        },
      ],
    },
  ],
  sitemapCorp: [
    {
      section: 'Pages',
      available: true,
      pages: [
        {
          label: 'Standby',
          url: 'CORP-standby.html',
          notAvailable: false,
        },
        {
          label: 'Standby 2',
          url: 'CORP-standby-2.html',
          notAvailable: false,
        },
        {
          label: 'Home',
          url: 'CORP-index.html',
          notAvailable: false,
        },
        {
          label: 'Sign up',
          url: 'CORP-sign_up.html',
          notAvailable: true,
        },
        {
          label: 'Sign in',
          url: 'CORP-sign_in.html',
          notAvailable: true,
        },
        {
          label: 'Article with image list',
          url: 'CORP-article_with_img_list.html',
          notAvailable: false,
        },
        {
          label: 'Newsletter subscription',
          url: 'CORP-newsletter_subscribe.html',
          notAvailable: false,
        },
        {
          label: 'Article with table and note',
          url: 'CORP-article_with_table_note.html',
          notAvailable: true,
        },
        {
          label: 'Article with complex table',
          url: 'CORP-article_with_complex_table.html',
          notAvailable: true,
        },
        {
          label: 'Article with anchors',
          url: 'CORP-fast_navigation_anchors.html',
          notAvailable: true,
        },
        {
          label: 'Destinations guide',
          url: 'CORP-destination-guides.html',
          notAvailable: true,
        },
        {
          label: 'Destination guide (city)',
          url: 'CORP-destination_guides_city.html',
          notAvailable: true,
        },
        {
          label: 'News list',
          url: 'CORP-news_list.html',
          notAvailable: false,
        },
        {
          label: 'News detail',
          url: 'CORP-news_detail.html',
          notAvailable: false,
        },
        {
          label: 'News detail table',
          url: 'CORP-news_detail-table.html',
          notAvailable: false,
        },
        {
          label: 'Sitemap',
          url: 'CORP-sitemap.html',
          notAvailable: false,
        },
      ],
    },
    {
      section: 'Landings',
      pages: [
        {
          label: 'Landing page 01',
          url: 'CORP-LANDING-01.html',
          notAvailable: true,
        },
      ],
    },
    {
      section: 'Components (HTML)',
      available: false,
      pages: [
        {
          label: 'Header (home)',
          url: 'COMP-CORP-header-home.html',
          notAvailable: true,
        },
        {
          label: 'Header (inner pages)',
          url: 'COMP-CORP-header-page.html',
          notAvailable: true,
        },
        {
          label: 'Main banner',
          url: 'COMP-CORP-main_banner.html',
          notAvailable: true,
        },
        {
          label: 'Multiple panel',
          url: 'COMP-CORP-multiple-panel.html',
          notAvailable: true,
        },
        {
          label: 'Destinations offers',
          url: 'COMP-CORP-destinations-offers.html',
          notAvailable: true,
        },
        {
          label: 'Main offers',
          url: 'COMP-CORP-main-offers.html',
          notAvailable: true,
        },
        {
          label: 'Footer',
          url: 'COMP-CORP-main_footer.html',
          notAvailable: true,
        },
        {
          label: 'Sign in',
          url: 'COMP-CORP-sign-in.html',
          notAvailable: true,
        },
        {
          label: 'Breadcrumbs',
          url: 'COMP-CORP-breadcrumbs.html',
          notAvailable: true,
        },
      ],
    },
    {
      section: 'UI-Elements (HTML)',
      available: true,
      pages: [
        {
          label: 'Accordion',
          url: 'COMP-CORP-accordion.html',
          notAvailable: true,
        },
        {
          label: 'Headings',
          url: 'COMP-CORP-headings.html',
        },
        {
          label: 'Paragraphs',
          url: 'COMP-CORP-paragraphs.html',
          notAvailable: true,
        },
        {
          label: 'Tables',
          url: 'COMP-CORP-tables.html',
          notAvailable: true,
        },
        {
          label: 'Lists',
          url: 'COMP-CORP-lists.html',
          notAvailable: false,
        },
        {
          label: 'Images',
          url: 'COMP-CORP-image.html',
          notAvailable: true,
        },
        {
          label: 'Pagination',
          url: 'COMP-CORP-pagination.html',
          notAvailable: true,
        },
      ],
    },
  ],
  sitemapPDF: [
    {
      section: 'PDF',
      available: true,
      pages: [
        {
          label: 'Boarding pass',
          url: 'PDF-boarding-pass-pdf.html',
          notAvailable: false,
        },
        {
          label: 'Itinerary',
          url: 'PDF-itinerary-pdf.html',
          notAvailable: false,
        },
      ],
    },
  ],
  sitemapMD: [
    {
      section: 'Components (HTML)',
      pages: [
        {
          label: 'Flight search',
          url: 'COMP-flight_search.html',
          notAvailable: true,
        },
        {
          label: 'Booking steps',
          url: 'COMP-booking_steps.html',
          notAvailable: true,
        },
        {
          label: 'Summary',
          url: 'COMP-summary.html',
          notAvailable: true,
        },
        {
          label: 'Footer (ibe)',
          url: 'COMP-footer.html',
          notAvailable: true,
        },
        {
          label: 'Payment details table',
          url: 'COMP-payment-detail-tables.html',
          notAvailable: true,
        },
      ],
    },
    {
      section: 'UI-Elements (HTML)',
      available: true,
      pages: [
        {
          label: 'Grid',
          url: 'COMP-grid.html',
        },
        {
          label: 'Colors',
          url: 'COMP-colors.html',
          notAvailable: false,
        },
        {
          label: 'Font icons',
          url: '../src/assets/fonts/icons/demo.html',
        },
        {
          label: 'Headings (h1,h2,h3...)',
          url: 'COMP-headings.html',
        },
        {
          label: 'Buttons',
          url: 'COMP-buttons.html',
        },
        {
          label: 'Form elements',
          url: 'COMP-form_elements.html',
        },
        {
          label: 'Modals',
          url: 'COMP-modals.html',
        },
      ],
    },
  ],
  sitemapEmails: [
    {
      section: 'Boarding Pass',
      available: true,
      pages: [
        {
          label: 'Boarding Pass (en)',
          url: 'emails/boardingpass-en.html',
          notAvailable: false,
        },
        {
          label: 'Itinerary (es)',
          url: 'emails/itinerary-es.html',
          notAvailable: false,
        },
      ],
    },
  ],
  searchFlight: {
    routes: {
      journeys: [
        {
          caption: 'Selecciona un origen',
          way: 'departure',
          inputForm: {
            groupId: 'searchOutbound',
            placeholder: 'Origen',
            label: 'Desde',
            id: 'searchOutbound_input',
            value: 'ASU',
            valueSmall: 'Asunción',
            idCollapse: 'searchOutbound_options',
          },
        },
        {
          caption: 'Selecciona un destino',
          way: 'arrival',
          inputForm: {
            groupId: 'searchReturn',
            placeholder: 'Destino',
            label: 'Hacia',
            id: 'searchReturn_input',
            value: 'AGT',
            valueSmall: 'Ciudad del Este',
            idCollapse: 'searchReturn_options',
          },
        },
      ],
      multicityJourneys: [
        {
          way: 'departure',
          inputForm: {
            groupId: 'searchOutbound-mc',
            placeholder: 'Origen',
            label: 'Desde',
            id: 'searchOutbound-mc_input',
            value: 'AGT',
            valueSmall: 'Ciudad del Este',
            idCollapse: 'searchOutbound-mc_options',
          },
        },
        {
          way: 'arrival',
          inputForm: {
            groupId: 'searchReturn-mc',
            placeholder: 'Destino',
            label: 'Hacia',
            id: 'searchReturn-mc_input',
            value: 'AIR',
            valueSmall: 'Buenos Aires',
            idCollapse: 'searchReturn-mc_options',
          },
        },
      ],
    },
    dates: {
      journeys: [
        {
          way: 'departure',
          inputForm: {
            groupId: 'searchDatesOutbound',
            placeholder: 'Fechas',
            label: 'Fecha de ida',
            id: 'searchDatesOutbound_input',
            valueDate: {
              day: '27',
              month: 'Sep',
              week: 'Domingo',
            },
            idCollapse: 'searchDatesOutbound_options',
          },
        },
        {
          way: 'arrival',
          inputForm: {
            groupId: 'searchDatesInbound',
            placeholder: '',
            label: 'Fecha de vuelta',
            id: 'searchDatesInbound_input',
            valueDate: {
              day: '7',
              month: 'Oct',
              week: 'Miércoles',
            },
            idCollapse: 'searchDatesInbound_options',
          },
        },
      ],
    },
    triptype: {
      position: 'within-calendar', // `default` or `within-calendar`
      style: 'switch',
      'switch': {
        options: [
          {
            checked: true,
            label: 'Ida y vuelta',
            id: 'triptype1',
            name: 'triptypeSelection',
          },
          {
            checked: false,
            label: 'Sólo ida',
            id: 'triptype2',
            name: 'triptypeSelection',
          },
        ],
      },
    },
    typology: {
      show: true,
      template: 'dropdown',  // styles: dropdown, search-collapsable
      caption: 'Selección tipo de pasajero',
      label: '',
      value: '2 adultos, 1 niño, 1 infante',
      buttonText: '',
      options: [
        {
          label: 'Adultos',
          labelInfo: '(> 12 años)',
          numericUpdown: {
            id: 'paxAdult',
            value: '2',
          },
        },
        {
          label: 'Niños',
          labelInfo: '(2-11 años)',
          numericUpdown: {
            id: 'paxChildren',
            value: '0',
          },
        },
        {
          label: 'Infantes',
          labelInfo: '(<2 años)',
          numericUpdown: {
            id: 'paxInfants',
            value: '0',
          },
          tooltip: '',
        },
      ],
    },
    currency: {
      show: false,
      formControl: {
        hasLabel: false,
        label: 'Currency',
        id: 'currencyId',
        options: [
          {
            name: 'PYG',
            id: '',
          },
        ],
      },
    },
    promocode: {
      show: false,
      formControl: {
        hasLabel: false,
        label: '+ Promocode',
        placeholder: 'Type your promocode',
        name: 'promocode',
        id: 'promocode',
      },
    },
    searchButton: {
      url: 'BF-select_flight.html',
      text: 'Buscar',
    },
  },
  multiplePanel: {
    responsive: {
      collapsable: false
    },
    navItems: [
      {
        ariaControl: 'multiplePanelTab1_tab',
        id: 'multiplePanelTab1',
        text: 'Vuelo',
        panel: {
          show: true,
          button: {
            icon: '',
            text: 'Book a trip',
          },
          contentInclude: 'partials/components/IBE/flight-search/_flight-search.njk',
        },
      },
      {
        ariaControl: 'multiplePanelTab2_tab',
        id: 'multiplePanelTab2',
        tabindex: -1,
        text: 'Check in',
        url: 'BF-WCI_login.html',
        panel: {
          show: false,
          button: {
            icon: '',
            text: 'Check in',
          },
          contentInclude: '',
        },
      },
    ],
  },
  mainHeader: {
    navPrimary: [
      {
        text: 'Reservar',
        url: '#',
      },
      {
        text: 'Gestionar',
        icon: 'manage',
        submenus: [
          {
            items: [
              {
                title: 'Check in Online',
                url: '#',
              },
              {
                title: 'Factura',
                url: '#',
              },
              {
                title: 'Pago de la reserva',
                url: '#',
              },
              {
                title: 'Cambios',
                url: '#',
              },
            ],
          },
        ],
      },
      {
        text: 'Información',
        icon: 'info-bubble',
        url: '#',
        submenus: [
          {
            items: [
              {
                title: 'Equipaje',
                url: '#',
                submenuList: [
                  {
                    text: 'Equipajes especiales',
                    url: '#',
                  },
                  {
                    text: 'Artículos no permitidos',
                    url: '#',
                  },
                  {
                    text: 'Tarifas Equipaje',
                    url: '#',
                  },
                ],
              },
              {
                title: 'Check in',
                url: '#',
              },
              {
                title: 'Tarifas',
                url: '#',
              },
              {
                title: 'Vuelos cháracter y grupos',
                url: '#',
              },
              {
                title: 'Métodos de pago',
                url: '#',
              },
              {
                title: 'Preguntas frequentes',
                url: '#',
              },
            ],
          },
        ],
      },
    ],
    navSecondary: {
      links: [
        {
          linkIcon: 'language-circle',
          url: '#',
          dataType: 'languages',
          dropdownList: [
            {
              title: 'Paraguay (PY)',
              url: '#',
            },
            {
              title: 'Argentina (AR)',
              url: '#',
            },
            {
              title: 'Uruguay (UY)',
              url: '#',
            },
            {
              title: 'Brasil (BR)',
              url: '#',
            },
            {
              title: 'Chile (CL)',
              url: '#',
            },
            {
              title: 'Bolivia (BO)',
              url: '#',
            },
            {
              title: 'Perú (PE)',
              url: '#',
            },
            {
              title: 'México (MX)',
              url: '#',
            },
            {
              title: 'España (ES)',
              url: '#',
            },
            {
              title: 'Francia (FR)',
              url: '#',
            },
            {
              title: 'Portugal (PT)',
              url: '#',
            },
            {
              title: 'Usa (US)',
              url: '#',
            },
          ],
        },
      ],
    },
  },
  newsList: {
    hasShowMoreLink: {
      caption: 'Seguir leyendo',
      pdfCaption: '[pdf]',
    },
  },
  mainFooter: {
    logo: {
      show: true,
    },
    flyPrograms: {
      show: false,
    },
    newsletter: {
      show: false,
    },
    bottom: {
      copyright: {
        show: true,
      },
    },
    socialmedia: {
      show: true,
    },
    nav: {
      show: true,
      isCollapsible: false,
      items: [
        {
          title: 'Acerca de nosotros',
          subItems: [
            {
              caption: 'Quiénes somos',
            },
            {
              caption: 'Prensa',
            },
            {
              caption: 'Flota',
            },
            {
              caption: 'Destinos',
            },
            {
              caption: 'Revista',
            },
            {
              caption: 'Contacto',
            },
            {
              caption: 'Empleo',
            },
            {
              caption: 'Mapa del sítio',
            },
          ],
        },
        {
          title: 'Información legal',
          subItems: [
            {
              caption: 'Contrato de transporte',
            },
            {
              caption: 'Términos y condiciones',
            },
            {
              caption: 'Política de privacidad',
            },
            {
              caption: 'Política de cookies',
            },
          ],
        },
      ],
    },
  },
  socialmedias: {
    title: '',
    hasFlatColors: true,
    items: [
      {
        name: 'facebook',
        url: 'https://www.facebook.com/Paranair-332679717288173',
      },
      {
        name: 'twitter',
        url: 'https://twitter.com/paranair',
      },
      {
        name: 'instagram',
        url: 'https://www.instagram.com/paranair_/',
      },
    ],
  },
  languageBar: {
    label: 'Language:',
    selectedText: 'Paraguay (PY)',
    items: [
      {
        title: 'Paraguay (PY)',
        url: '#',
      },
      {
        title: 'Argentina (AR)',
        url: '#',
      },
      {
        title: 'Uruguay (UY)',
        url: '#',
      },
      {
        title: 'Brasil (BR)',
        url: '#',
      },
      {
        title: 'Chile (CL)',
        url: '#',
      },
      {
        title: 'Bolivia (BO)',
        url: '#',
      },
      {
        title: 'Perú (PE)',
        url: '#',
      },
      {
        title: 'México (MX)',
        url: '#',
      },
      {
        title: 'España (ES)',
        url: '#',
      },
      {
        title: 'Francia (FR)',
        url: '#',
      },
      {
        title: 'Portugal (PT)',
        url: '#',
      },
      {
        title: 'Usa (US)',
        url: '#',
      },
    ],
  },
  copyright: {
    text: '&copy; 2018 Paranair Airlines',
  },
  poweredby: {
    label: 'Powered by',
    text: '',
    image: 'logo-newshore.svg',
    linkUrl: 'http://newshore.es',
  },
  mainOffers: {
    gridCols: 'grid-col col-12 col-xs-4',
    offers: [
      {
        isOfferDeal: true,
        icon: 'baggage-h',
        title: 'Equipaje',
        caption: 'Compruebe la información sobre el equipaje que puede llevar con usted',
        button: 'Ver más',
        buttonUrl: '',
      },
      {
        isOfferDeal: true,
        icon: 'press',
        title: 'Prensa',
        caption: 'Consulte las noticias más recientes de nuestra página',
        button: 'Ver más',
        buttonUrl: '',
      },
      {
        isOfferDeal: true,
        icon: 'airplane-up-fleet',
        title: 'Flota',
        caption: 'Conozca las características de nuestros aviones CRJ200',
        button: 'Reserva',
        buttonUrl: '',
      },
    ],
  },
  destinationOffersPrice: {
    template: 'table', // items or table
    title: 'Ofertas desde',
    selector: 'Asunción',
    selectorOptions: [
      {
        name: 'La Paz',
      },
      {
        name: 'Punta del Este',
      },
      {
        name: 'Mengomeyén',
      },
      {
        name: 'Sao Paulo',
      },
      {
        name: 'Santiago',
      },
    ],
    destinationOptionTitle: 'Destinations offers',
    destinationOptions: {
      items: [
        {
          labelFrom: 'Desde',
          text: '',
          title: 'La Paz',
          price: '328,50',
          link: {
            url: 'javascript:void(0)',
            title: '',
            label: '',
          },
        },
        {
          labelFrom: 'Desde',
          text: '',
          title: 'Punta del Este',
          price: '312,00',
          link: {
            url: 'javascript:void(0)',
            title: '',
            label: '',
          },
        },
        {
          labelFrom: 'Desde',
          text: '',
          title: 'Mengomeyén',
          price: '354,00',
          link: {
            url: 'javascript:void(0)',
            title: '',
            label: '',
          },
        },
        {
          labelFrom: 'Desde',
          text: '',
          title: 'Sao Paulo',
          price: '354,00',
          link: {
            url: 'javascript:void(0)',
            title: '',
            label: '',
          },
        },
        {
          labelFrom: 'Desde',
          text: '',
          title: 'Santiago',
          price: '354,00',
          link: {
            url: 'javascript:void(0)',
            title: '',
            label: '',
          },
        },
      ],
    },
  },
  destinationOffers: {
    main: false,
    aside: {
      items: [
        {
          isMain: false,
          city: 'Buenos Aires',
          from: 'Desde',
          price: 271,
          img: 'buenos-aires',
          url: '',
          offerLink: {
            caption: '',
            url: '',
          },
        },
      ],
    },
  },
  mapRoutesHome: {
    title: 'Mapa de Rutas',
    introdution: 'Paranair te ofrece una gran variedad de destinos y el mejor servicio en aeropuerto y a bordo. Conoce a dónde volamos.',
    img: 'maproutes.png',
    button: {
      text: 'Ver más',
      url: 'javascript:void(0)',
    },
  },
  destinations: {
    items: [
      {
        countryName: 'Argentina',
        cities: [
          {
            name: 'Buenos Aires',
            airportcode: '(AEP)',
          },
          {
            name: 'Buenos Aires',
            airportcode: '(EZE)',
          },
          {
            name: 'Salta',
            airportcode: '(SLA)',
          },
        ],
      },
      {
        countryName: 'Brasil',
        cities: [
          {
            name: 'Curitiba',
            airportcode: '(CWB)',
          },
          {
            name: 'Florianópolis',
            airportcode: '(FLN)',
          },
          {
            name: 'Rio de Janeiro - Galeao',
            airportcode: '(GIG)',
          },
          {
            name: 'Sao Paulo - Guarulhos',
            airportcode: '(GRU)',
          },
        ],
      },
      {
        countryName: 'Chile',
        cities: [
          {
            name: 'Iquique',
            airportcode: '(IQQ)',
          },
        ],
      },
      {
        countryName: 'Paraguay',
        cities: [
          {
            name: 'Asunción',
            airportcode: '(ASU)',
          },
          {
            name: 'Ciudad del Este',
            airportcode: '(AGT)',
          },
        ],
      },
      {
        countryName: 'Uruguay',
        cities: [
          {
            name: 'Montevideo',
            airportcode: '(MVD)',
          },
          {
            name: 'Punta del Este',
            airportcode: '(PDP)',
          },
        ],
      },
    ],
  },
  newsletter: {
    title: 'Suscríbete a nuestra lista de correo',
    intro: 'Recibe en tu correo electrónico las mejores ofertas, promociones y noticias más destacadas',
    buttonText: 'Suscríbase',
    canBeClosed: false,
    closeLabel: '',
    form: {
      hasLabel: false,
      label: 'E-mail',
      placeholder: 'E-mail',
      name: 'newsletterInput',
      id: 'newsletterInput',
    },
  },
  boardingPass: {
    hasDetailsSection: false,
    hasExtraInfoSection: false,
    hasFoldSection: false,
    title: {
      caption: 'Tarjeta de embarque',
      translation: 'Boarding Pass',
    },
    passenger: {
      label: {
        caption: 'Nombre del Pasajero',
        translation: 'Passenger Name',
      },
      value: 'Perez/Maria Cecilia',
      passport: 'PP 12345678',
    },
    flight: {
      label: {
        caption: 'Vuelo',
        translation: 'Flight',
      },
      value: 'Z8 800',
    },
    date: {
      label: {
        caption: 'Fecha',
        translation: 'Date',
      },
      value: 'Miércoles 24 de octubre de 2018',
    },
    from: {
      label: {
        caption: 'Desde',
        translation: 'From',
      },
      value: 'ASUNCION',
      iata: '(ASU)',
    },
    to: {
      label: {
        caption: 'Hacia',
        translation: 'To',
      },
      value: 'CIUDAD DEL ESTE',
      iata: '(AGT)',
    },
    gate: {
      label: {
        caption: 'Puerta',
        translation: 'Gate',
      },
      value: 'A confirmar',
    },
    boardingTime: {
      label: {
        caption: 'Hora Embarque',
        translation: 'Boarding Time',
      },
      value: '10:30',
    },
    departureTime: {
      label: {
        caption: 'Hora Salida',
        translation: 'Departure Time',
      },
      value: '10:53',
    },
    seat: {
      label: {
        caption: 'Asiento',
        translation: 'Seat',
      },
      value: '2D',
    },
    row: {
      label: {
        caption: 'Fila',
        translation: 'Row',
      },
      value: '2',
    },
    frequentFlyer: {
      show: false,
      label: {
        caption: 'frequent Flyer',
      },
      value: '4732934392037',
    },
    ticket: {
      label: {
        caption: 'N° Ticket',
        translation: 'Ticket number',
      },
      value: 'N7360199',
    },
    baggage: {
      title: {
        caption: 'EQUIPAJE PERMITIDO',
        translation: 'Baggage Allowance',
      },
      subtitle: {
        caption: 'Cada pasajero puede llevar',
        translation: 'Each passengar may take',
      },
      hold: {
        show: false,
        title: {
          caption: 'EQUIPAJE EN BODEGA',
        },
        subtitle: {
          caption: '1 Pieza de Equipaje',
          translation: '1 Piece of Baggage',
        },
        description: {
          caption: 'Franquicia de equipaje facturado en bodega',
          translation: 'Free baggage allowance',
        },
        weight: {
          adult: {
            caption: 'ADULTO',
            value: '20',
          },
          child: {
            caption: 'MENOR',
            value: '10',
          },
        },
      },
      carry: {
        title: {
          caption: 'EQUIPAJE DE MANO',
          translation: 'HAND BAGGAGE',
        },
        subtitle: {
          caption: '1 Artículo Personal',
          translation: '1 Personal Item',
        },
        weight: {
          caption: 'El equipaje de mano no debe superar las siguientes medidas',
          translation: 'Hand baggage must not exceed the following measurements',
          'default': {
            value: '5',
          },
        },
        sizes: {
          caption: 'El peso máximo es de',
          translation: 'Hand baggage must not weight more than',
          height: {
            value: '30',
            caption: 'alto',
            translation: 'height',
          },
          width: {
            value: '20',
            caption: 'ancho',
            translation: 'width',
          },
          length: {
            value: '27',
            caption: 'largo',
            translation: 'length',
          },
        },
      },
      remiders: {
        items: [
          {
            title: {
              caption: 'NOTA',
              translation: 'NOTE',
            },
            caption: 'En caso de que el equipaje de mano no cumpla con estos requisitos, el pasajero deberá despachar el equipaje.',
            translation: 'Should the hand baggage not comply with these requirements, the passenger will have to check it.',
          },
        ],
      },
    },
    importantReminders: {
      caption: 'Recordatorios Importantes.',
      translation: 'Important Reminders.',
      items: [
        {
          image: 'pdf/checked-baggage',
          title: {
            caption: 'EQUIPAJE DESPACHADO PERMITIDO',
            translation: 'CHECKED BAGGAGE ALLOWANCE',
          },
          caption: 'Las políticas del transportista referentes a la franquicia permitida de equipaje despachado, su peso y tamaño por pieza según destino, así como las políticas de equipaje para infantes (0-2 años) se encuentran disponibles en el sitio web www.paranair.com.',
          translation: 'The carrier’s policies for checked baggage, weight and size per piece in accordance to destination, as well as the policies for infant (ages 0-2) baggage may be found at www.paranair.com.',
        },
        {
          image: 'pdf/legal-obligations',
          title: {
            caption: 'OBLIGACIONES LEGALES – DOCUMENTOS REQUERIDOS',
            translation: 'LEGAL OBLIGATIONS – REQUIRED DOCUMENTS',
          },
          caption: 'Es responsabilidad del pasajero cumplir con todos los requisitos migratorios, impositivos y aduaneros, tanto en el punto de origen como en el destino final de su itinerario. El pasajero deberá contar además con todos los documentos legales exigidos para realizar el viaje, incluyendo, pero sin limitarse a pasaporte, visas, cedula de identidad, según fuera el caso. Si así no fuera, el transportista podrá denegar el embarque.',
          translation: 'The passenger must comply with all immigration, tax and customs requirements at the point of origin and final destination. The passenger must bear all valid travel documents, including but not limited to passport, visa, national identification card, as required. If this is not the case, the carrier may refuse carriage.',
        },
        {
          image: 'pdf/airport-arrival',
          title: {
            caption: 'PRESENTACIÓN EN EL AEROPUERTO',
            translation: 'AIRPORT ARRIVAL TIMES',
          },
          caption: 'El pasajero deberá presentarse en el aeropuerto al menos dos (2) horas antes del horario marcado para la partida del vuelo. El pasajero deberá además chequear los monitores disponibles en el terminal aeroportuario para conocer su puerta de embarque, debiendo presentarse en la misma al menos cuarenta y cinco (45) minutos antes de la salida del vuelo. En caso de que el pasajero no se presente a la puerta de embarque con dicha antelación, el transportista se reserva el derecho de reasignar el asiento del pasajero.',
          translation: 'The passenger must arrive at the airport at least two (2) hours prior to departure time. The passenger must also check the airport monitors available through the terminal in order to find out the boarding gate. The passenger must be at the gate at least forty-five (45) minutes prior to flight departure. Failure to do so may result in the carrier re-assigning the passenger’s seat.',
        },
        {
          image: 'pdf/dangerous-goods',
          title: {
            caption: 'MERCANCÍAS PELIGROSAS',
            translation: 'DANGEROUS GOODS',
          },
          caption: 'No se permite el transporte de mercancías peligrosas ya sea como equipaje despachado o como equipaje de mano. Más información sobre las políticas específicas del transportista sobre mercancías peligrosas puede ser obtenida en el sitio web www.paranair.com.',
          translation: 'No dangerous goods are allowed in either checked baggage or carry-on baggage. More information on the carrier’s policies on dangerous goods may be found at www.paranair.com.',
        },
      ],
    },
    lastMessage: {
      caption: 'POR FAVOR IMPRIMA 2 COPIAS DE ESTA TARJETA DE EMBARQUE',
      translation: 'PLEASE PRINT 2 COPIES OF THIS BOARDING PASS',
    },
    weightUnit: 'kg',
    sizeUnit: 'cm',
  },
  sitemap: {
    items: [
      {
        caption: 'Reservar',
        items: [
          {
            caption: 'Reservar vuelo',
          },
        ],
      },
      {
        caption: 'Gestionar',
        items: [
          {
            caption: 'Chec in Online',
          },
          {
            caption: 'Factura',
          },
          {
            caption: 'Cambios',
          },
        ],
      },
      {
        caption: 'Información',
        items: [
          {
            caption: 'Equipaje',
            items: [
              {
                caption: 'Equipajes especiales',
              },
              {
                caption: 'Artículos no permitidos',
                items: [
                  {
                    caption: 'Level 4 example 1',
                  },
                  {
                    caption: 'Level 4 example 2',
                  },
                ],
              },
              {
                caption: 'Tarifas de equipaje',
              },
            ],
          },
          {
            caption: 'Check in',
          },
          {
            caption: 'Tarifas',
          },
          {
            caption: 'Vuelos charter y grupos',
          },
          {
            caption: 'Métodos de pago',
          },
          {
            caption: 'Preguntas frecuentes',
          },
        ],
      },
      {
        caption: 'Acerca de nosotros',
        items: [
          {
            caption: 'Quienes somos',
          },
          {
            caption: 'Prensa',
          },
          {
            caption: 'Flota',
          },
          {
            caption: 'Destinos',
          },
          {
            caption: 'Contacto',
          },
          {
            caption: 'Empleo',
          },
          {
            caption: 'Mapa del sitio',
          },
        ],
      },
      {
        caption: 'Información legal',
        items: [
          {
            caption: 'Contrato de transporte',
          },
          {
            caption: 'Términos y condiciones',
          },
          {
            caption: 'Política de privacidad',
          },
          {
            caption: 'Política de cookies',
          },
        ],
      },
    ],
  },
  emailBox: {
    intro: 'Send boarding passes by e-mail',
    buttonText: 'Send',
    form: {
      label: 'E-mail',
      name: 'wci-client-email',
      id: 'wci-client-email',
    },
  }
}

module.exports = data;
module.exports = {
  projectName: 'Sky',
  site_title: 'Sky [HTML]',
  demo_imgs_path: '../src/assets/imgs/',
  core_assets_path: '../../../core/src/assets/',
  project_assets_path: '../src/assets/',
  logo_img: 'logo.svg',
  logo_negative_img: 'logo-neg.svg',
  pathFont: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700,800,900&amp;subset=latin-ext',
  day: 'día',
  SHARED: {
    currency: '$',
    weightmeasure: 'Kg',
    promoLabelDefault: 'Promo',
    formRequiredSymbol: '*',
    formRequiredText: '* Campos obligatorios',
    from: 'Desde',
    nextFlight: 'Siguiente vuelo',
    flightNotavailable: 'El vuelo no está disponible.<br/>El horario de salida y el de regreso no coinciden.',
    close: 'close',
    addedCaption: '(incluido)',
  },
  scripts: {
    header: false,
  },
  showUserAccount: false,
  header: {
    nav: {
      itemsAlign: 'left',
      items: [{
          caption: 'Reservar',
        },
        {
          caption: 'Gestionar',
        },
        {
          caption: 'Información',
        },
      ],
    },
    bookingSteps: {
      show: true,
    },
    headerRight: {
      show: false,
    },
    auth: {
      show: true,
      url: '',
      label: 'Ingreso / registro',
      text: 'Aprovecha los beneficios',
      modal: {
        id: 'login_modal',
      },
    },
    summary: {
      show: true,
      label: {
        show: false,
      },
      route: {
        airport: {
          city: {
            show: false,
          },
          code: {
            time: {
              show: false,
            },
          },
        },
      },
      flight: {
        show: false,
      },
      time: {
        show: true,
        departure: {
          caption: 'Salida',
          hour: '19:30',
        },
        arrival: {
          caption: 'Llegada',
          hour: '22:35',
        },
      },
    },
  },
  loginForm: {
    formHead: {
      title: '¡Únete a SKY y haz tu compra más simple!',
      subtitle: '',
      list: {
        items: [{
            title: 'Guarda tú última búsqueda y compra en solo unos pasos.',
            icon: '',
          },
          {
            title: '¿Viajes en grupo? registra tus acompañantes frecuentes.',
            icon: '',
          },
          {
            title: 'Tu historio de tus vuelos en solo un click.',
            icon: '',
          },
        ],
      },
    },
    check: {
      status: true,
      label: 'Acepto recibir promociones y descuentos.',
    },
    formTypeClass: 'login-form',
    isVertical: true,
    buttonText: 'Iniciar sesión',
    isSocial: true,
    forgotPassword: {
      title: '¿Has olvidado la contraseña?',
      url: '#',
      hasModal: {
        modalBefore: true,
        id: 'forgot-password-modal',
      },
    },
    text: '<p>Al suscribirte, acepta los <a href=\'#\'>Términos de uso</a> de SKY</p>',
    hasGetText: true,
    infoButton: {
      text: '¿Aun no tienes cuenta?. Entonces',
      footnote: '<p><a href=\'#\'>Continúa comprando sin descuentos ni ofertas</a></p>',
      link: {
        title: 'Regístrate',
        url: 'CORP-sign_up.html',
      },
    },
    data: {
      formGroups: [{
        elements: [{
          elementsGroup: [{
              type: 'inputForm',
              element: {
                label: 'Dirección de correo electrónico',
                type: 'email',
              },
            },
            {
              type: 'inputForm',
              element: {
                label: 'Contraseña',
                type: 'password',
                icon: 'seguro',
                iconButton: true,
                iconTitle: 'Ver contraseña',
                iconRight: true,
              },
            },
          ],
        }, ],
      }, ],
    },
  },
  registerForm: {
    formTypeClass: 'register-form',
    isVertical: true,
    buttonText: 'Regístrate',
    isSocial: true,
    forgotPassword: '',
    text: '<p>Al suscribirte, acepta los <a href=\'#\'>Términos de uso</a> de SKY</p>',
    hasGetText: true,
    infoButton: {
      text: '¿Ya tienes cuenta?. Entonces',
      footnote: '<p><a href=\'#\'>Continúa comprando sin descuentos ni ofertas</a></p>',
      link: {
        title: 'Inicia sesión',
        url: 'CORP-sign_in.html',
      },
    },
    formHead: {
      title: '¡Únete a SKY y haz tu compra más simple!',
      subtitle: '',
      list: {
        items: [{
            title: 'Guarda tú última búsqueda y compra en solo unos pasos.',
            icon: '',
          },
          {
            title: '¿Viajes en grupo? registra tus acompañantes frecuentes.',
            icon: '',
          },
          {
            title: 'Tu historio de tus vuelos en solo un click.',
            icon: '',
          },
        ],
      },
    },
    check: {
      status: true,
      label: 'Acepto recibir promociones y descuentos.',
    },
    data: {
      formGroups: [{
        elements: [{
            type: 'inputForm',
            element: {
              label: 'Dirección de correo electrónico',
              type: 'email',
            },
          },
          {
            type: 'inputForm',
            element: {
              label: 'Contraseña',
              type: 'password',
              icon: 'seguro',
              iconButton: true,
              iconTitle: 'Ver contraseña',
              iconRight: true,
            },
          },
        ],
      }, ],
    },
  },
  forgotPasswordForm: {
    formHead: {
      title: '¿Olvidaste tu contraseña',
    },
    check: {},
    formTypeClass: 'forgot-password_form',
    buttonText: 'Listo',
    isSocial: false,
    hasGetText: false,
    infoButton: {},
    data: {
      formGroups: [{
        elements: [{
          elementsGroup: [{
            type: 'inputForm',
            element: {
              label: 'Ingresa tu email',
              type: 'email',
              id: 'form-email',
            },
          }, ],
        }, ],
      }, ],
    },
  },
  bookingRetrieve: {
    formHead: {
      title: '¿No tienes cuenta? Busca aquí tu reserva',
    },
    formTypeClass: 'booking-retrieve',
    buttonText: 'Buscar',
    isSocial: false,
    forgotPassword: '',
    hasGetText: false,
    data: {
      formGroups: [{
        elements: [{
          elementsGroup: [{
              type: 'inputForm',
              element: {
                label: 'N° de reserva',
                type: 'number',
              },
            },
            {
              type: 'inputForm',
              element: {
                label: 'Apellido',
                type: 'text',
              },
            },
          ],
        }, ],
      }, ],
    },
  },
  multipleSearch: {
    show: true,
    isCollapsable: true,
    active: 1,
    items: [{
        link: '',
        label: 'Vuelos',
        icon: 'icon--flights',
        controls: 'multipleSearchFlights_tab',
        id: 'multipleSearchFlights',
        tabindex: -1,
        contentInclude: 'partials/components/IBE/flight-search/_flight-search.njk',
      },
      {
        link: {},
        label: 'Paquetes de viajes',
        icon: 'icon--flightshotels',
        controls: 'multipleSearchHotelsFlights_tab',
        id: 'multipleSearchHotelsFlights',
        tabindex: -1,
        contentInclude: 'partials/_replacement-partial.njk',
      },
      {
        link: {},
        label: 'Hoteles',
        icon: 'icon--hotels',
        controls: 'multipleSearchHotels_tab',
        id: 'multipleSearchHotels',
        tabindex: -1,
        contentInclude: 'partials/_replacement-partial.njk',
      },
      {
        link: {},
        label: 'Autos',
        icon: 'icon--cars',
        controls: 'multipleSearchCars_tab',
        id: 'multipleSearchCars',
        tabindex: -1,
        contentInclude: 'partials/_replacement-partial.njk',
      },
    ],
  },
  bookingSteps: [{
      stepNumber: '1',
      stepLabel: 'Vuelos',
      stepUrl: '',
    },
    {
      stepNumber: '2',
      stepLabel: 'Adicionales',
      stepUrl: '',
    },
    {
      stepNumber: '3',
      stepLabel: 'Pago',
      stepUrl: '',
    },
    {
      stepNumber: '4',
      stepLabel: 'Confirmación',
      stepUrl: '',
    },
  ],
  multipleCalendarOptions: false,
  months: [{
      month: 'Diciembre',
      year: '2017',
    },
    {
      month: 'Enero',
      year: '2018',
      selected: true,
    },
    {
      month: 'Febrero',
      year: '2018',
    },
    {
      month: 'Marzo',
      year: '2018',
    },
    {
      month: 'Abril',
      year: '2018',
    },
    {
      month: 'Mayo',
      year: '2018',
    },
    {
      month: 'Junio',
      year: '2018',
    },
    {
      month: 'Julio',
      year: '2018',
    },
    {
      month: 'Agosto',
      year: '2018',
    },
    {
      month: 'Septiembre',
      year: '2018',
    },
  ],
  days: [{
      day: '24 AGO',
      week: '',
      price: '144.837',
    },
    {
      day: '25 AGO',
      week: '',
      price: '162.000',
      selected: true,
    },
    {
      day: '26 AGO',
      week: '',
      price: '173.943',
    },
    {
      day: '27 AGO',
      week: '',
      price: '120.528',
    },
    {
      day: '28 AGO',
      week: '',
      price: '208.593',
      bestprice: true,
      exceed: true,
    },
    {
      day: '29 AGO',
      week: '',
      price: '208.593',
      exceed: true,
    },
    {
      day: '30 AGO',
      week: '',
      price: '162.000',
    },
    {
      day: '31 AGO',
      week: '',
      price: '144.837',
      noflights: true,
    },
    {
      day: '01 Jan',
      week: '',
      price: '144.837',
    },
    {
      day: '02 Jan',
      week: '',
      price: '144.837',
    },
    {
      day: '03 Jan',
      week: '',
      price: '144.837',
    },
    {
      day: '04 Jan',
      week: '',
      price: '144.837',
    },
    {
      day: '03 Jan',
      week: '',
      price: '144.837',
    },
  ],
  journeys: [{
      'class': 'outbound',
      way: 'Ida',
      headerConnector: '<span class=\'icon icon-arrow-right\'></span>',
      headerImgSrc: '',
      departureAirport: 'Santiago',
      departureAirportCode: 'SCL',
      departureAirportTerminal: 'Terminal 1',
      arrivalAirport: 'Rio de Janeiro',
      arrivalAirportCode: 'GIG',
      arrivalAirportTerminal: 'Terminal 4',
      hasExtraDayLabel: false,
      date: {
        week: 'Miércoles,',
        weekAbbr: 'Mie',
        day: '8',
        month: 'Agosto',
        monthAbbr: 'Ago',
        year: '',
      },
      stop: {},
      tecStop: {},
      flightNumbers: [{
        number: '809',
        operator: 'Z8',
      }, ],
      fare: 1,
      fareSelectedName: 'SuperParanair',
      items: [{
          quantity: '2',
          title: 'Adultos',
          fare: 'Plus',
          price: '378.980',
          items: [{
              caption: 'ticket',
            },
            {
              caption: '1ra. Maleta',
              addedCaption: '(incluida)',
            },
            {
              caption: 'Asiento estandar',
              addedCaption: '(incluido)',
            },
          ],
        },
        {
          quantity: '1',
          title: 'Niño',
          fare: 'Plus',
          price: '189.490',
          items: [{
              caption: 'ticket',
            },
            {
              caption: '1ra. Maleta',
              addedCaption: '(incluída)',
            },
            {
              caption: 'Asiento estandar',
              addedCaption: '(incluído)',
            },
          ],
        },
        {
          quantity: '',
          title: 'Asientos',
          fare: '',
          route: {
            departure: 'Santiago',
            arrival: 'Africa',
          },
          price: '$6.990',
          items: [{
              caption: 'ticket',
            },
            {
              caption: '1ra. Maleta',
              addedCaption: '(incluída)',
            },
            {
              caption: 'Asiento estandar',
              addedCaption: '(incluído)',
            },
          ],
        },
      ],
    },
    {
      'class': 'inbound',
      way: 'Vuelta',
      headerConnector: '<span class=\'icon icon-arrow-right\'></span>',
      headerImgSrc: '',
      departureAirport: 'Rio de Janeiro',
      departureAirportCode: 'GIG',
      departureAirportTerminal: 'Terminal 2',
      arrivalAirport: 'Santiago',
      arrivalAirportCode: 'SCL',
      arrivalAirportTerminal: 'Terminal 4',
      hasExtraDayLabel: false,
      date: {
        week: 'Miércoles',
        weekAbbr: 'Mie',
        day: '15',
        month: 'Ago',
        year: '',
      },
      stop: {},
      tecStop: {},
      flightNumbers: [{
        number: '811',
        operator: 'Z8',
      }, ],
      fare: 3,
      fareSelectedName: 'Paranair plus',
      items: [{
          quantity: '2',
          title: 'Adultos',
          fare: 'Plus',
          price: '378.980',
          items: [{
              caption: 'ticket',
            },
            {
              caption: '1ra. Maleta',
              add: true,
            },
            {
              caption: 'Asiento estandar',
              add: true,
            },
          ],
        },
        {
          quantity: '1',
          title: 'Niño',
          fare: 'Plus',
          price: '189.490',
          items: [{
              caption: 'ticket',
            },
            {
              caption: '1ra. Maleta',
              add: true,
            },
            {
              caption: 'Asiento estandar',
              add: true,
            },
          ],
        },
        {
          quantity: '',
          title: 'Asientos',
          fare: '',
          route: {
            departure: 'Santiago',
            arrival: 'Africa',
          },
          price: '$6.990',
          items: [{
              caption: 'ticket',
            },
            {
              caption: '1ra. Maleta',
              add: true,
            },
            {
              caption: 'Asiento estandar',
              add: true,
            },
          ],
        },
      ],
    },
  ],
  flightCodeTimeSeparator: '|',
  flightCaption: 'Vuelo',
  flightLabelOutbound: 'Salida',
  flightLabelInbound: 'Llegada',
  flightExtraDaysPosition: 'left',
  flightSelectedFareButtonSelectable: true,
  flightSelectFareLinkUrl: '',
  flightSelectShowFares: false,
  flightScale: {
    title: 'Detalle del vuelo',
    subtitle: 'Jueves 06 Septiembre 2018',
    timeTotalText: 'Tiempo total del viaje:',
    timeTotal: '14h 00m',
    departureTime: '10:30',
    departureAirport: 'Santiago',
    departureAirportCode: 'SCL',
    departureDuration: '2h 20m',
    flightNumberDeparture: 'Vuelo Z8 809',
    scales: [{
        title: 'Argentina',
        time: '12:00',
        departureAirportCode: 'AR',
        transit: {
          time: '10h 10m',
          title: 'Transito en Santiago',
        },
      },
      {
        title: 'Argentina',
        time: '22:20',
        departureAirportCode: 'AR',
        isFinalizeFlight: true,
      },
    ],
    arrivalTime: '14:00',
    arrivalAirport: 'Rio de Janeiro',
    arrivalAirportCode: 'GIG',
    arrivalDuration: '1h 30m',
    flightNumberArrival: 'Vuelo Z8 811',
  },
  flights: {
    showDateHeader: true,
    infoLegend: {
      items: [{
          icon: 'nomascota',
          label: 'No hay cupo para mascotas',
        },
        {
          icon: 'novuelo',
          label: 'No hay vuelos',
        },
      ],
    },
    items: [{
        departureTime: '10:30',
        departureAirport: '',
        departureAirportCode: 'GIG',
        departureAirportTerminal: 'Terminal 2',
        duration: '0:40',
        stop: {},
        tecStop: {},
        arrivalTime: '14:00',
        arrivalAirport: '',
        arrivalAirportCode: 'SCL',
        arrivalAirportTerminal: 'Terminal 2',
        priceFrom: '1.115.000',
        priceNote: '',
        connection: false,
        fareSelected: false,
        fareSelectedName: '',
        fareType: 'fare2',
        hasPromo: false,
        unavailable: true,
        flightnumbers: {
          label: 'Vuelo',
          items: [{
            operator: '',
            number: '546',
          }, ],
        },
      },
      {
        departureTime: '11:40',
        departureAirport: '',
        departureAirportCode: 'GIG',
        departureAirportTerminal: 'Terminal 2',
        duration: '',
        stop: {},
        tecStop: {},
        arrivalTime: '12:00',
        arrivalAirport: '',
        arrivalAirportCode: 'SCL',
        arrivalAirportTerminal: 'Terminal 2',
        priceFrom: '1.115.000',
        priceNote: '2 asientos disponibles en esta tarifa',
        connection: false,
        fareSelected: false,
        fareSelectedName: '',
        fareType: 'fare3',
        hasPromo: false,
        flightnumbers: {
          label: 'Vuelo',
          items: [{
            operator: '',
            number: '546',
          }, ],
        },
        info: {
          items: [{
            icon: 'nomascota',
            label: 'No hay cupo para mascotas',
          }, ],
        },
      },
      {
        departureTime: '14:30',
        departureAirport: '',
        departureAirportCode: 'GIG',
        duration: '',
        stop: {},
        tecStop: {},
        arrivalTime: '16:00',
        arrivalAirport: '',
        arrivalAirportCode: 'SCL',
        priceFrom: '1.115.000',
        priceNote: '',
        connection: false,
        fareSelected: true,
        fareSelectedName: '',
        fareType: 'fare2',
        hasPromo: false,
        unavailable: false,
        flightnumbers: {
          label: 'Vuelo',
          items: [{
              operator: '',
              number: '444',
            },
            {
              operator: '',
              number: '666',
            },
          ],
        },
      },
      {
        departureTime: '11:40',
        departureAirport: '',
        departureAirportCode: 'GIG',
        departureAirportTerminal: 'Terminal 2',
        duration: '',
        stop: {},
        tecStop: {},
        arrivalTime: '12:00',
        arrivalAirport: '',
        arrivalAirportCode: 'SCL',
        arrivalAirportTerminal: 'Terminal 2',
        priceFrom: '1.115.000',
        priceNote: '',
        connection: false,
        fareSelected: false,
        fareSelectedName: '',
        fareType: 'fare3',
        hasPromo: false,
        flightnumbers: {
          label: 'Vuelo',
          items: [{
            operator: '',
            number: '343',
          }, ],
        },
      },
    ],
  },
  paxSelector: {
    title: 'Pasajeros',
    label: 'Asiento',
    nextButton: {},
  },
  passengersData: {
    passengers: [{
        label: 'Adulto 1',
        gender: '',
        firstName: 'Jorge',
        lastName: 'García',
        type: 'Adulto',
        seat: '--',
        seatStatus: 'current',
        checkedIn: true,
        hasInfoButton: false,
      },
      {
        label: 'Adulto 2',
        gender: '',
        firstName: 'Oscar',
        lastName: 'Arias',
        type: 'Adulto',
        seat: '15A',
        seatStatus: 'selected',
        checkedIn: false,
        hasInfoButton: false,
      },
      {
        label: 'Niño 1',
        gender: '',
        firstName: 'Carmen',
        lastName: 'María Galeano',
        type: 'Niño',
        seat: '--',
        seatStatus: '',
        statusInput: 'has-error',
        messageInput: 'Error message',
        checkedIn: false,
        hasInfoButton: false,
      },
      {
        label: 'Niño 2',
        gender: '',
        firstName: 'John',
        lastName: 'Jacinto',
        type: 'Adulto',
        seat: '4F',
        seatStatus: 'selected',
        checkedIn: false,
        hasInfoButton: false,
      },
    ],
  },
  seatmap: {
    seatsHasPopover: false,
    letters: [{
        letter: 'A',
      },
      {
        letter: 'B',
      },
      {
        letter: 'C',
      },
      {
        letter: '',
      },
      {
        letter: 'D',
      },
      {
        letter: 'E',
      },
      {
        letter: 'F',
      },
    ],
    seats: [{
        hasExitLeft: true,
        seats_group: [{
            status: 'upfront',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'upfront',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'upfront',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '1',
          },
          {
            status: 'upfront',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'upfront',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'upfront',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: 'exit',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '2',
          },
          {
            status: 'exit',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'exit',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'exit',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '3',
          },
          {
            status: 'exit',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'exit',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'exit',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: 'exit',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'exit',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'exit',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '4',
          },
          {
            status: 'exit',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'exit',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'selected',
            passanger: 'P3',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: 'exit',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'exit',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'exit',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '5',
          },
          {
            status: 'exit',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'exit',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'exit',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: 'unavailable',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '6',
          },
          {
            status: 'unavailable',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: 'unavailable',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '7',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'front',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'front',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: 'front',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'front',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '8',
          },
          {
            status: 'unavailable',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'front',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'front',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '9',
          },
          {
            status: 'unavailable',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'front',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'front',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        hideRow: true,
        seats_group: [{
            status: 'empty',
            text: '',
          },
          {
            status: 'empty',
            text: '',
          },
          {
            status: 'empty',
            text: '',
          },
          {
            status: '',
            row_number: true,
            text: '',
          },
          {
            status: 'empty',
            text: '',
          },
          {
            status: 'empty',
            text: '',
          },
          {
            status: 'empty',
            text: '',
          },
        ],
      },
      {
        hasExitLeft: true,
        hasExitRight: true,
        seats_group: [{
            status: 'xlarge',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'xlarge',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'xlarge',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '11',
          },
          {
            status: 'xlarge',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'xlarge',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'xlarge',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '12',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '13',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '14',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: 'selected',
            passanger: 'P4',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '15',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '16',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '17',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '18',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '19',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '20',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '21',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '22',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '23',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '24',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '25',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '26',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
    ],
  },
  seatsLegend: {
    hasResponsiveModal: false,
    desc: {
      title: 'Detalle',
    },
    footnote: '',
    items: [{
        type: 'upfront',
        text: 'Primera fila',
        price: '',
        description: '',
      },
      {
        type: 'exit',
        text: 'Salida rápida',
        price: '',
        description: '',
      },
      {
        type: 'front',
        text: 'Más adelante',
        price: '',
        description: '',
      },
      {
        type: 'xlarge',
        text: 'Extra espacio',
        price: '',
        description: '',
      },
      {
        type: '',
        text: 'Disponible',
        price: '',
        description: '',
      },
      {
        type: 'unavailable',
        text: 'No Disponible',
        price: '',
        description: '',
      },
      {
        type: 'selected',
        text: 'Seleccionado',
        price: '',
        description: '',
      },
    ],
  },
  fares: {
    faresLayoutTabs: false,
    localText: 'Doméstico',
    internationalText: 'Internacional',
    notApplyFareText: 'Sin cargo',
    fareText: 'Cargo',
    currency: 'USD',
    compareFares: false,
    label: 'Tarifa',
    selectLabel: {
      caption: 'Selecciona tu opción:',
    },
    removeRedirect: true,
    items: [{
        pos: '1',
        name: 'Zero',
        isPromo: false,
        price: '0',
        unavailable: false,
        fareButtonText: '+ 0',
        farePrice: '',
        fareDescription: [{
            description: 'Pasaje aéreo',
            notIncluded: false,
            icon: '',
            localPrice: '',
            internationalPrice: '',
            fareLabel: '',
          },
          {
            description: 'Equipaje de mano',
            notIncluded: false,
            icon: '',
            localPrice: '',
            internationalPrice: '',
            fareLabel: '',
          },
          {
            description: 'Asiento preferente',
            notIncluded: true,
            icon: '',
            localPrice: '',
            internationalPrice: '',
            fareLabel: '',
          },
          {
            description: 'Equipaje Bodega 32 kg',
            notIncluded: true,
            icon: '',
            localPrice: '',
            internationalPrice: '',
            fareLabel: '',
          },
          {
            description: 'Cambio de vuelo',
            notIncluded: true,
            icon: '',
            localPrice: '',
            internationalPrice: '',
            fareLabel: '',
          },
          {
            description: 'Star Pass',
            notIncluded: true,
            icon: '',
            localPrice: '',
            internationalPrice: '',
            fareLabel: '',
          },
          {
            description: 'Cambio de nombre',
            notIncluded: true,
            icon: '',
            localPrice: '',
            internationalPrice: '',
            fareLabel: '',
          },
        ],
      },
      {
        pos: '2',
        name: 'Plus',
        isPromo: false,
        price: '4.000',
        unavailable: false,
        fareButtonText: '+ 4.000',
        farePrice: '',
        fareDescription: [{
            description: 'Pasaje aéreo',
            notIncluded: false,
            icon: '',
            localPrice: '',
            internationalPrice: '',
            fareLabel: '',
          },
          {
            description: 'Equipaje de mano',
            notIncluded: false,
            icon: '',
            localPrice: '',
            internationalPrice: '',
            fareLabel: '',
          },
          {
            description: 'Asiento preferente',
            notIncluded: false,
            icon: '',
            localPrice: '',
            internationalPrice: '',
            fareLabel: '',
          },
          {
            description: 'Equipaje Bodega 32 kg',
            notIncluded: false,
            icon: '',
            localPrice: '',
            internationalPrice: '',
            fareLabel: '',
          },
          {
            description: 'Cambio de vuelo',
            notIncluded: false,
            icon: '',
            localPrice: '',
            internationalPrice: '',
            fareLabel: '',
            extraDescription: 'Wow!',
          },
          {
            description: 'Star Pass',
            notIncluded: true,
            icon: '',
            localPrice: '',
            internationalPrice: '',
            fareLabel: '',
          },
          {
            description: 'Cambio de nombre',
            notIncluded: true,
            icon: '',
            localPrice: '',
            internationalPrice: '',
            fareLabel: '',
          },
        ],
      },
      {
        pos: '3',
        name: 'Full',
        isPromo: false,
        price: '7.000',
        unavailable: false,
        selected: false,
        fareButtonText: '+ 7.000',
        farePrice: '',
        fareDescription: [{
            description: 'Pasaje aéreo',
            notIncluded: false,
            icon: '',
            localPrice: '',
            internationalPrice: '',
            fareLabel: '',
          },
          {
            description: 'Equipaje de mano',
            notIncluded: false,
            icon: '',
            localPrice: '',
            internationalPrice: '',
            fareLabel: '',
          },
          {
            description: 'Asiento preferente',
            notIncluded: false,
            icon: '',
            localPrice: '',
            internationalPrice: '',
            fareLabel: '',
          },
          {
            description: 'Equipaje Bodega 32 kg',
            notIncluded: false,
            icon: '',
            localPrice: '',
            internationalPrice: '',
            fareLabel: '',
          },
          {
            description: 'Cambio de vuelo',
            notIncluded: true,
            icon: '',
            localPrice: '',
            internationalPrice: '',
            fareLabel: '',
          },
          {
            description: 'Star Pass',
            notIncluded: false,
            icon: '',
            localPrice: '',
            internationalPrice: '',
            fareLabel: '',
          },
          {
            description: 'Cambio de nombre',
            notIncluded: false,
            icon: '',
            localPrice: '',
            internationalPrice: '',
            fareLabel: '',
          },
        ],
      },
    ],
  },
  baggages: [{
      title: 'En cabina',
      icon: 'carry_on2',
      type: [{
          price: '3.990',
          promo: true,
        },
        {
          price: '3.990',
          promo: true,
        },
      ],
    },
    {
      title: 'En bodega',
      icon: 'equipaje_en_bodega',
      type: [{
          size: 'Selecciona $0',
          label: 'Select price',
          promo: true,
        },
        {
          size: 'Selecciona $0',
          label: 'Select price',
          promo: true,
        },
      ],
    },
  ],
  baggagesOW: [{
    title: '',
    type: [{
        price: '229',
        size: '20',
        promo: true,
      },
      {
        price: '360',
        size: '25',
        promo: true,
      },
      {
        price: '365',
        size: '30',
        promo: true,
      },
    ],
  }, ],
  specialBaggages: [{
      title: 'Departing Oversized Baggage',
      type: [{
          price: '229',
          size: '20',
          promo: true,
        },
        {
          price: '360',
          size: '25',
          promo: true,
        },
        {
          price: '365',
          size: '30',
          promo: true,
        },
      ],
    },
    {
      title: 'Returning Oversized Baggage',
      type: [{
        price: '365',
        size: '30',
        promo: false,
      }, ],
    },
  ],
  specialBaggagesOW: [{
    type: [{
        price: '229',
        size: '20',
        promo: true,
      },
      {
        price: '360',
        size: '25',
        promo: true,
      },
      {
        price: '365',
        size: '30',
        promo: true,
      },
    ],
  }, ],
  meals: [{
      name: 'Thai Red Curry Chicken with Rice Thai Red Curry Chicken with Rice',
      image: 'meal_01.png',
      price: '70',
    },
    {
      name: 'Japanese Beef Bento',
      image: 'meal_02.png',
      price: '105',
    },
    {
      name: 'Dim Sum Set',
      image: 'meal_03.png',
      price: '70',
      active: true,
    },
    {
      name: 'Evian Natural Mineral Water 330ml',
      image: '',
      price: '15',
    },
  ],
  searchResultConnector: '-',
  searchResultHasTypology: false,
  editSearchButtonText: 'Editar',
  summaryData: [{
    isCollapse: true,
    hasPriceResume: false,
    fareByFlight: false,
    showAirportName: false,
    showBottomTotal: true,
    showBottomTotalLabel: 'Total',
    buttonClose: '',
    moreDetailsCollapsible: false,
    moreDetailsStyle: 'simple',
    summaryCollapseTitle: 'Detalle de compra',
    totalCostValue: '234.567',
    totalCostLabel: 'Valor actual:',
    legalTerms: '',
    totalFinal: {
      label: 'total',
      value: '1.154.114',
    },
    typology: {
      show: false,
    },
    details: {
      show: false,
    },
    closeBtn: {
      show: false,
    },
  }, ],
  summaryTypology: {
    showTitle: false,
    isCollapsible: true,
    totalPrice: '1.115.000,00',
    resumePrices: [
      [{
          label: '1 x Tarifa adulto',
          price: 'PYG 1.000.000',
        },
        {
          label: '1 x Tarifa niño',
          price: 'PYG 1.000.000',
        },
        {
          label: '1 x Tarifa bebé',
          price: 'PYG 1.000.000',
        },
      ],
      [{
          label: '1 x Tasas y/o impuestos adulto',
          price: 'PYG 1.000.000',
        },
        {
          label: '1 x Tasas y/o impuestos niño',
          price: 'PYG 115.000',
        },
        {
          label: '1 x Tasas y/o impuestos bebé',
          price: 'PYG 115.000',
        },
      ],
    ],
  },
  bookingReference: {
    label: 'Código de reserva',
    code: 'jipoym',
  },
  segmentSelectorIsTabs: false,
  segmentSelector: {
    hasButton: true,
    alertText: '* Debes seleccionar los asientos para cada pasajero *',
    flightLabel: '',
    flights: [{
        from: 'SAN',
        to: 'GIG',
        fromCity: 'Santiago',
        toCity: 'Rio de Janeiro',
        date: 'Miércoles 28 Agosto 2018',
        flightNumber: 'Z8 821',
        disabled: false,
        active: true,
      },
      {
        from: 'GIG',
        to: 'SAN',
        fromCity: 'Rio de Janeiro',
        toCity: 'Santiago',
        date: 'Sábado 31 Agosto 2018',
        flightNumber: 'PR0044',
        disabled: false,
        active: false,
      },
    ],
  },
  paymentData: {
    methods: {
      items: [{
        isCurrent: true,
        name: 'Credit Card',
        hasCurrencyOptions: false,
        title: '',
        titleTag: '',
        id: 'creditcardCollapse',
        image: 'creditcard.svg',
        imageSelected: 'creditcard-selected.svg',
        template: '_cc',
      }, ],
    },
  },
  CreditCardOptions: [{
      label: 'Visa',
      id: 'ccTypeVisa',
      name: 'creditCardType',
      imageURL: 'payment-methods/credit-cards/visa.svg',
      imageAlt: 'Visa payment',
    },
    {
      label: 'Mastercard',
      id: 'ccTypeMasterCard',
      name: 'creditCardType',
      imageURL: 'payment-methods/credit-cards/mastercard.svg',
      imageAlt: '',
    },
    {
      label: 'American express',
      id: 'ccTypeAmerican',
      name: 'creditCardType',
      imageURL: 'payment-methods/credit-cards/american-express.svg',
      imageAlt: '',
    },
  ],
  CreditCardFormData: {
    formGroups: [{
      elements: [{
        elementsGroup: [{
            type: 'inputForm',
            element: {
              label: 'Número de tarjeta',
            },
          },
          {
            type: 'inputForm',
            element: {
              label: 'Titular de la tarjeta',
            },
          },
          {
            type: 'selectForm',
            element: {
              label: 'Fecha de caducidad',
              id: 'ccExpiryDate',
              options: [{
                  name: 'MM',
                  id: 'ccExpiryDateMM',
                },
                {
                  name: 'YYYY',
                  id: 'ccExpiryDateYYYY',
                },
              ],
            },
          },
          {
            type: 'inputForm',
            element: {
              label: 'CW2 / CV2',
              hasTooltip: true,
              tooltipText: 'Tooltip text goes here',
            },
          },
        ],
      }, ],
    }, ],
  },
  sitemapIBE: [{
      section: 'Booking flow',
      available: true,
      pages: [{
          label: 'Select flight',
          url: 'BF-select_flight.html',
          notAvailable: false,
        },
        {
          label: 'Select flight (one way)',
          url: 'BF-select_flight_OW.html',
          notAvailable: true,
        },
        {
          label: 'Select flight (multicity)',
          url: 'BF-select_flight_multicity.html',
          notAvailable: true,
        },
        {
          label: 'Fare selection',
          url: 'BF-fare_selection.html',
          notAvailable: false,
        },
        {
          label: 'Passenger data',
          url: 'BF-passenger_data.html',
          notAvailable: true,
        },
        {
          label: 'Seat selection',
          url: 'BF-seat_selection.html',
          notAvailable: false,
        },
        {
          label: 'Baggage selection',
          url: 'BF-baggage_selection.html',
          notAvailable: false,
        },
        {
          label: 'Baggage selection (one way)',
          url: 'BF-baggage_selection-OW.html',
          notAvailable: false,
        },
        {
          label: 'Add extras',
          url: 'BF-services_add.html',
          notAvailable: false,
        },
        {
          label: 'Add extras (one way)',
          url: 'BF-services_add_oneway.html',
          notAvailable: true,
        },
        {
          label: 'Payment',
          url: 'BF-payment.html',
          notAvailable: true,
        },
        {
          label: 'Payment confirmation',
          url: 'BF-payment_confirmation.html',
          notAvailable: true,
        },
        {
          label: 'Payment confirmation (one way)',
          url: 'BF-payment_confirmation_OW.html',
          notAvailable: true,
        },
        {
          label: 'Payment confirmation (QR)',
          url: 'BF-payment_confirmation_QR.html',
          notAvailable: true,
        },
      ],
    },
    {
      section: 'Manage my Booking',
      available: false,
      pages: [{
          label: 'Login',
          url: 'BF-MMB_login.html',
          notAvailable: true,
        },
        {
          label: 'Home',
          url: 'BF-MMB_home.html',
          notAvailable: true,
        },
        {
          label: 'Cancel Flight',
          url: 'BF-MMB_cancel_flight.html',
          notAvailable: true,
        },
        {
          label: 'Change Flight',
          url: 'BF-MMB_change_flight.html',
          notAvailable: true,
        },
        {
          label: 'Guest details',
          url: 'BF-MMB_passenger_data.html',
        },
      ],
    },
    {
      section: 'Check in',
      available: false,
      pages: [{
          label: 'Login',
          url: 'BF-WCI_login.html',
          notAvailable: true,
        },
        {
          label: 'Itinerary',
          url: 'BF-WCI_home.html',
          notAvailable: true,
        },
        {
          label: 'Itinerary (one way)',
          url: 'BF-WCI_home-OW.html',
          notAvailable: true,
        },
        {
          label: 'Passengers',
          url: 'BF-WCI_passenger_data.html',
          notAvailable: true,
        },
        {
          label: 'Seats',
          url: 'BF-WCI_seat_selection.html',
          notAvailable: false,
        },
        {
          label: 'Pre-Confirmation',
          url: 'BF-WCI_pre-confirmation.html',
          notAvailable: true,
        },
        {
          label: 'Pre-Confirmation (one way)',
          url: 'BF-WCI_pre-confirmation-OW.html',
          notAvailable: true,
        },
        {
          label: 'Boarding pass',
          url: 'BF-WCI_home_checkedin.html',
          notAvailable: true,
        },
      ],
    },
    {
      section: 'Agent cabinet',
      pages: [{
          label: 'Login',
          url: 'AC_login.html',
          notAvailable: true,
        },
        {
          label: 'Home',
          url: 'AC_home.html',
          notAvailable: true,
        },
        {
          label: 'Account management',
          url: 'AC_account_management.html',
          notAvailable: true,
        },
        {
          label: 'MMB',
          url: 'AC_mmb.html',
          notAvailable: true,
        },
        {
          label: 'Refunds',
          url: 'AC_refunds.html',
          notAvailable: true,
        },
        {
          label: 'Payment',
          url: 'AC-payment.html',
          notAvailable: true,
        },
        {
          label: 'Payment hold proceed',
          url: 'AC_payment_hold_proceed.html',
          notAvailable: true,
        },
      ],
    },
    {
      section: 'Members',
      pages: [{
          label: 'Login',
          url: 'U-FLY_login.html',
          notAvailable: true,
        },
        {
          label: 'Forgot password',
          url: 'U-FLY_forgot_password.html',
          notAvailable: true,
        },
        {
          label: 'Sign up',
          url: 'U-FLY_sign_up.html',
          notAvailable: true,
        },
        {
          label: 'Confirmation',
          url: 'U-FLY_confirmation.html',
          notAvailable: true,
        },
        {
          label: 'Profile update',
          url: 'U-FLY_profile_update.html',
          notAvailable: true,
        },
      ],
    },
    {
      section: 'Error pages',
      available: false,
      pages: [{
        label: 'Error 404',
        url: '404.html',
        notAvailable: true,
      }, ],
    },
  ],
  sitemapCorp: [{
      section: 'Pages',
      available: true,
      pages: [{
          label: 'Home',
          url: 'CORP-index.html',
          notAvailable: false,
        },
        {
          label: 'Sign up',
          url: 'CORP-sign_up.html',
          notAvailable: true,
        },
        {
          label: 'Sign in',
          url: 'CORP-sign_in.html',
          notAvailable: true,
        },
        {
          label: 'Servicios de carga',
          url: 'CORP-carga.html',
          notAvailable: false,
        },
        {
          label: 'Login empresas',
          url: 'CORP-login-empresas.html',
          notAvailable: false,
        },
        {
          label: 'Article with image list',
          url: 'CORP-article_with_img_list.html',
          notAvailable: true,
        },
        {
          label: 'Newsletter subscription',
          url: 'CORP-newsletter_subscribe.html',
          notAvailable: true,
        },
        {
          label: 'Article with table and note',
          url: 'CORP-article_with_table_note.html',
          notAvailable: true,
        },
        {
          label: 'Article with complex table',
          url: 'CORP-article_with_complex_table.html',
          notAvailable: true,
        },
        {
          label: 'Article with anchors',
          url: 'CORP-fast_navigation_anchors.html',
          notAvailable: true,
        },
        {
          label: 'Destinations guide',
          url: 'CORP-destination-guides.html',
          notAvailable: false,
        },
        {
          label: 'Destination guide (city)',
          url: 'CORP-destination_guides_city.html',
          notAvailable: true,
        },
        {
          label: 'Servicios adicionales',
          url: 'CORP-servicios-adicionales.html',
          notAvailable: false,
        },
        {
          label: 'Servicios tarifario',
          url: 'CORP-servicios-tarifario.html',
          notAvailable: false,
        },
        {
          label: 'Travel Sky',
          url: 'CORP-travel-sky.html',
          notAvailable: false,
        },
        {
          label: 'Vuela siempre low cost',
          url: 'CORP-siempre-low-cost.html',
          notAvailable: false,
        },
        {
          label: 'Cyber Sky',
          url: 'CORP-cyber-sky.html',
          notAvailable: false,
        },
        {
          label: 'Contact Us',
          url: 'CORP-contact-us.html',
          notAvailable: false,
        },
        {
          label: 'News list',
          url: 'CORP-news_list.html',
          notAvailable: true,
        },
        {
          label: 'News detail',
          url: 'CORP-news_detail.html',
          notAvailable: true,
        },
        {
          label: 'News detail table',
          url: 'CORP-news_detail-table.html',
          notAvailable: true,
        },
      ],
    },
    {
      section: 'Landings',
      pages: [{
        label: 'Landing page 01',
        url: 'CORP-LANDING-01.html',
        notAvailable: true,
      }, ],
    },
    {
      section: 'Components (HTML)',
      available: true,
      pages: [{
          label: 'Header (home)',
          url: 'COMP-CORP-header-home.html',
          notAvailable: true,
        },
        {
          label: 'Header (inner pages)',
          url: 'COMP-CORP-header-page.html',
          notAvailable: true,
        },
        {
          label: 'Main banner',
          url: 'COMP-CORP-main_banner.html',
          notAvailable: true,
        },
        {
          label: 'Multiple panel',
          url: 'COMP-CORP-multiple-panel.html',
          notAvailable: true,
        },
        {
          label: 'Destinations offers',
          url: 'COMP-CORP-destinations-offers.html',
          notAvailable: true,
        },
        {
          label: 'Main offers',
          url: 'COMP-CORP-main-offers.html',
          notAvailable: true,
        },
        {
          label: 'Footer',
          url: 'COMP-CORP-main_footer.html',
          notAvailable: true,
        },
        {
          label: 'Sign in',
          url: 'COMP-CORP-sign-in.html',
          notAvailable: true,
        },
        {
          label: 'Breadcrumbs',
          url: 'COMP-CORP-breadcrumbs.html',
          notAvailable: true,
        },
      ],
    },
    {
      section: 'UI-Elements (HTML)',
      available: true,
      pages: [{
          label: 'Accordion',
          url: 'COMP-CORP-accordion.html',
          notAvailable: true,
        },
        {
          label: 'Headings',
          url: 'COMP-CORP-headings.html',
        },
        {
          label: 'Paragraphs',
          url: 'COMP-CORP-paragraphs.html',
          notAvailable: true,
        },
        {
          label: 'Tables',
          url: 'COMP-CORP-tables.html',
          notAvailable: false,
        },
        {
          label: 'Lists',
          url: 'COMP-CORP-lists.html',
          notAvailable: false,
        },
        {
          label: 'Images',
          url: 'COMP-CORP-image.html',
          notAvailable: true,
        },
        {
          label: 'Pagination',
          url: 'COMP-CORP-pagination.html',
          notAvailable: true,
        },
      ],
    },
  ],
  sitemapMD: [{
      section: 'Components (HTML)',
      pages: [{
          label: 'Flight search',
          url: 'COMP-flight_search.html',
          notAvailable: true,
        },
        {
          label: 'Booking steps',
          url: 'COMP-booking_steps.html',
          notAvailable: true,
        },
        {
          label: 'Summary',
          url: 'COMP-summary.html',
          notAvailable: true,
        },
        {
          label: 'Footer (ibe)',
          url: 'COMP-footer.html',
          notAvailable: true,
        },
        {
          label: 'Payment details table',
          url: 'COMP-payment-detail-tables.html',
          notAvailable: true,
        },
      ],
    },
    {
      section: 'UI-Elements (HTML)',
      available: true,
      pages: [{
          label: 'Grid',
          url: 'COMP-grid.html',
        },
        {
          label: 'Colors',
          url: 'COMP-colors.html',
          notAvailable: false,
        },
        {
          label: 'Font icons',
          url: '../src/assets/fonts/icons/demo.html',
        },
        {
          label: 'Headings (h1,h2,h3...)',
          url: 'COMP-headings.html',
        },
        {
          label: 'Buttons',
          url: 'COMP-buttons.html',
        },
        {
          label: 'Form elements',
          url: 'COMP-form_elements.html',
        },
        {
          label: 'Modals',
          url: 'COMP-modals.html',
        },
      ],
    },
  ],
  searchFlight: {
    routes: {
      journeys: [
        {
          caption: 'Viajaré desde',
          way: 'departure',
          inputForm: {
            groupId: 'searchOutbound',
            placeholder: 'Selecciona una origen',
            label: 'Desde',
            id: 'searchOutbound_input',
            value: 'SCL',
            valueSmall: 'Santiago',
            idCollapse: 'searchOutbound_options',
          },
        },
        {
          caption: 'Viajaré hacia',
          way: 'arrival',
          inputForm: {
            groupId: 'searchReturn',
            placeholder: 'Selecciona un destino',
            label: 'Hacia',
            id: 'searchReturn_input',
            value: 'GIG',
            valueSmall: 'Rio de Janeiro',
            idCollapse: 'searchReturn_options',
          },
        },
      ],
      multicityJourneys: [{
          way: 'departure',
          inputForm: {
            groupId: 'searchOutbound-mc',
            placeholder: 'Selecciona una origen',
            label: 'Desde',
            id: 'searchOutbound-mc_input',
            value: 'SCL',
            valueSmall: 'Santiago',
            idCollapse: 'searchOutbound-mc_options',
          },
        },
        {
          way: 'arrival',
          inputForm: {
            groupId: 'searchReturn-mc',
            placeholder: 'Selecciona un destino',
            label: 'Hacia',
            id: 'searchReturn-mc_input',
            value: 'AIR',
            valueSmall: 'Buenos Aires',
            idCollapse: 'searchReturn-mc_options',
          },
        },
      ],
    },
    dates: {
      calendarSwitch: {
        label: 'Mostrar tarifas más baratas en el mes',
        url: '',
      },
      journeys: [{
          way: 'departure',
          title: 'Elije una fecha de ida',
          inputForm: {
            groupId: 'searchDatesOutbound',
            placeholder: '05/12/2018',
            label: 'Fecha de ida',
            id: 'searchDatesOutbound_input',
            value: '16/10/2018',
            valueSmall: '',
            idCollapse: 'searchDatesOutbound_options',
          },
        },
        {
          way: 'arrival',
          title: 'Elije una fecha de vuelta',
          inputForm: {
            groupId: 'searchDatesInbound',
            placeholder: '10/01/2019',
            label: 'Fecha de vuelta',
            id: 'searchDatesInbound_input',
            value: '24/10/2018',
            valueSmall: '',
            idCollapse: 'searchDatesInbound_options',
          },
        },
      ],
    },
    triptype: {
      position: 'default', // `default` or `within-calendar`
      style: 'radios',
      radios: {
        options: [{
            radio: true,
            checked: true,
            label: 'Ida y vuelta',
            id: 'triptype_roundtrip',
            name: 'triptypeSelection',
          },
          {
            radio: true,
            checked: false,
            label: 'Sólo ida',
            id: 'triptype_ow',
            name: 'triptypeSelection',
          },
          {
            radio: true,
            checked: false,
            label: 'Multitramo',
            id: 'triptype_multicity',
            name: 'triptypeSelection',
          },
        ],
      },
    },
    typology: {
      show: true,
      template: 'search-collapsable',  // styles: dropdown, search-collapsable
      label: 'Pasajeros',
      caption: 'Selección tipo de pasajero',
      title: 'Agrega acompañantes',
      value: '2 adultos, 6 mascotas',
      buttonText: 'Hecho',
      options: [{
          label: 'Adultos',
          labelInfo: 'Sobre 12 años',
          numericUpdown: {
            id: 'paxAdult',
            value: '2',
          },
        },
        {
          label: 'Niños',
          labelInfo: '2 a 11 años',
          numericUpdown: {
            id: 'paxChildren',
            value: '0',
            minus: 'disabled',
          },
        },
        {
          label: 'Infantes',
          labelInfo: 'Menor a 2 años',
          numericUpdown: {
            id: 'paxInfants',
            value: '0',
            minus: 'disabled',
          },
          tooltip: '',
        },
        {
          label: 'Mascotas',
          labelInfo: '',
          numericUpdown: {
            id: 'paxPets',
            value: '6',
            minus: '',
          },
          tooltip: '',
        },
      ],
    },
    currency: {
      show: false,
      formControl: {
        hasLabel: false,
        label: 'Currency',
        id: 'currencyId',
        options: [{
          name: 'PYG',
          id: '',
        }, ],
      },
    },
    promocode: {
      show: true,
      formControl: {
        label: 'Promocode',
        placeholder: '',
        name: 'promocode',
        id: 'promocode',
      },
    },
    searchButton: {
      url: 'BF-select_flight.html',
      text: 'Buscar',
      disabled: false,
    },
    searchTT: {
      topbar: true,
      closeBtn: 'close',
    },
  },
  multiplePanel: {
    responsive: {
      collapsable: true
    },
    navItems: [{
      ariaControl: 'multiplePanelTab1_tab',
      id: 'multiplePanelTab1',
      text: '',
      panel: {
        show: true,
        button: {
          icon: '',
          text: '',
        },
        contentInclude: 'partials/corporative/_multiple-search.njk',
      },
    }, ],
  },
  mainHeader: {
    navToggle: {
      icon: '',
      text: 'Toggle navigation',
      id: 'main-header',
    },
    navToggleUser: {
      icon: 'usuario',
      id: '',
      hasModal: {
        id: 'login_modal',
      },
    },
    navPrimary: [{
        text: 'Planifica tu viaje',
        icon: 'servicios',
        url: '#',
        subnavDesc: {
          descTitle: {
            title: '!Haz de tu viaje una experiencia en la que tú decides como viajar!',
          },
          descText: {
            lists: {
              items: [{
                  title: 'Busca las tarifas mas economicas del mes',
                  icon: '',
                },
                {
                  title: 'Agrega hotel a tu viaje',
                  icon: '',
                },
                {
                  title: 'Arrienda un auto',
                  icon: '',
                },
                {
                  title: 'Elige un paquete de viaje',
                  icon: '',
                },
                {
                  title: 'Reserva un transfer',
                  icon: '',
                },
              ],
            },
          },
        },
        submenus: [{
          items: [{
              title: 'Planea tu viaje',
              url: '',
              submenuList: [{
                  text: 'Tarifas más bajas',
                  url: '#',
                },
                {
                  text: 'Destinos y vuelos',
                  url: '#',
                },
                {
                  text: 'Tarifa light y plus',
                  url: '#',
                },
                {
                  text: 'Servicios adicionales',
                  url: '#',
                },
                {
                  text: 'Menu a bordo',
                  url: '#',
                },
                {
                  text: 'Estado de vuelos',
                  url: '#',
                },
              ],
            },
            {
              title: 'Servicios',
              url: '',
              submenuList: [{
                  text: 'Hoteles',
                  url: '#',
                },
                {
                  text: 'Autos',
                  url: '#',
                },
                {
                  text: 'Paquetes de viaje',
                  url: '#',
                },
                {
                  text: 'Transfer al aeropuerto',
                  url: '#',
                },
                {
                  text: 'Reserva de grupos',
                  url: '#',
                },
                {
                  text: 'Travel Sky',
                  url: '#',
                },
                {
                  text: 'Panoramas',
                  url: '#',
                },
              ],
            },
          ],
        }, ],
      },
      {
        text: 'Administra tu vuelo',
        url: '#',
        icon: 'servicios_adicionales',
        subnavDesc: {
          descTitle: {
            title: '¡Elige cómo quieres volar, agregando tus servicios adicionales ahora!',
          },
          descText: {
            lists: {
              items: [{
                  title: 'Agrega equipaje',
                  icon: '',
                },
                {
                  title: 'Modifica la ubicación de tu asiento',
                  icon: '',
                },
                {
                  title: 'Ahorra tiempo con StarPass',
                  icon: '',
                },
                {
                  title: 'Cambia la fecha o destino de tu vuelo',
                  icon: '',
                },
                {
                  title: 'Anula tu vuelo',
                  icon: '',
                },
              ],
            },
          },
        },
        components: {
          items: [{
            path: 'partials/components/IBE/_booking-retrieve.njk',
          }, ],
        },
      },
      {
        text: 'Check in',
        url: '#',
        icon: 'checkin',
        subnavDesc: {
          descTitle: {
            title: '¿Todo listo para tu viaje? ¡Hazlo fácil! Chequeate por la web.',
          },
          descText: {
            text: 'Check in disponible desde 48 horas hasta 1 hora antes de tu vuelo ¡Y si te falto algo, agrégalo ahora y estarás ahorrando!',
          },
        },
        components: {
          items: [{
            path: 'partials/components/IBE/_booking-retrieve.njk',
          }, ],
        },
      },
    ],
    navtertiary: {
      links: [{
          text: 'Personas',
          url: '#',
          active: true,
        },
        {
          text: 'Empresas',
          url: '#',
        },
      ],
    },
    navSecondary: {
      links: [{
          linkIcon: 'usuario',
          url: '',
          label: 'Ingreso / registro',
          text: 'Aprovecha los beneficios',
          dataType: 'user',
          hasModal: {
            id: 'login_modal',
          },
        },
        {
          linkIcon: '',
          url: '#',
          label: 'Chile',
          dataType: 'languages',
          dropdownList: [{
              title: 'Chile',
              url: '#',
              image: 'chile.jpg',
              status: true,
            },
            {
              title: 'English',
              url: '#',
              image: 'us.jpg',
            },
            {
              title: 'Otros (Español)',
              url: '#',
              image: 'españa.jpg',
            },
            {
              title: 'Argentina',
              url: '#',
              image: 'argentina.jpg',
            },
            {
              title: 'Perú',
              url: '#',
              image: 'peru.jpg',
            },
            {
              title: 'Uruguay',
              url: '#',
              image: 'uruguay.jpg',
            },
            {
              title: 'Brasil',
              url: '#',
              image: 'brasil.jpg',
            },
          ],
        },
        {
          linkIcon: 'currency',
          url: '#',
          label: 'CLP',
          dataType: 'currency',
          dropdownList: [{
              title: 'CLP',
              url: '#',
            },
            {
              title: 'USD',
              url: '#',
            },
            {
              title: 'ARS',
              url: '#',
            },
            {
              title: 'BRL',
              url: '#',
            },
            {
              title: 'PEN',
              url: '#',
            },
          ],
        },
      ],
    },
    navShortcut: {
      links: [{
          icon: 'desde',
          url: '',
          text: 'Tus vuelos',
          hasModal: {},
          components: {
            items: [{
              path: 'partials/components/IBE/_booking-retrieve.njk',
            }, ],
          },
        },
        {
          icon: 'checkin',
          url: '',
          text: 'Check in',
          hasModal: {},
          components: {
            items: [{
              path: 'partials/components/IBE/_booking-retrieve.njk',
            }, ],
          },
        },
      ],
    },
  },
  newsList: {
    hasShowMoreLink: {
      caption: 'Seguir leyendo',
      pdfCaption: '[pdf]',
    },
  },
  mainFooter: {
    title: 'Bienvenido a SkyAirline.com',
    logo: {
      show: false,
    },
    flyPrograms: {
      show: false,
    },
    newsletter: {
      show: false,
    },
    bottom: {
      copyright: {
        show: true,
      },
      languageBar: {
        show: false,
      },
    },
    socialmedia: {
      show: true,
    },
    nav: {
      isCollapsible: false,
      items: [{
          title: 'Información útil',
          subItems: [{
              caption: 'Centro de ayuda',
            },
            {
              caption: 'Tarifario de Productos',
            },
            {
              caption: 'Destinos',
            },
          ],
        },
        {
          title: 'Portales',
          subItems: [{
              caption: 'Portal Empresas',
            },
            {
              caption: 'SKY Carga',
            },
            {
              caption: 'Mercado Público',
            },
          ],
        },
        {
          title: 'Nosotros',
          subItems: [{
              caption: 'Trabaja con nosotros',
            },
            {
              caption: 'Reclutamiento Pilotos',
            },
            {
              caption: 'Nuestra historia',
            },
            {
              caption: 'Condiciones Legales',
            },
            {
              caption: 'Tarifario de Productos',
            },
            {
              caption: 'Productos',
            },
            {
              caption: 'Contáctanos',
            },
          ],
        },
      ],
    },
  },
  socialmedias: {
    title: 'Síguenos',
    hasFlatColors: true,
    items: [{
        name: 'twitter',
        url: 'https://twitter.com/',
      },
      {
        name: 'facebook',
        url: 'https://www.facebook.com/',
      },
      {
        name: 'youtube',
        url: 'https://www.youtube.com/',
      },
      {
        name: 'instagram',
        url: 'https://www.instagram.com/',
      },
    ],
  },
  socialmediasLogin: {
    title: 'Inicia Sesión',
    textSeparator: 'o si prefieres inicia sesión con:',
    items: [{
        name: 'facebook',
        url: 'https://www.facebook.com/',
        title: 'Iniciar sesión con Facebook',
        hasImg: 'https://www.skyairline.com/Content/img/external/facebook.png',
      },
      {
        name: 'google',
        url: 'https://www.google.com/',
        title: 'Iniciar sesión con Google',
        hasImg: 'https://www.skyairline.com/Content/img/external/gmail.png',
      },
    ],
  },
  socialmediasRegister: {
    title: 'Regístrate',
    textSeparator: 'o si prefieres registrarte con:',
    notifications: {
      status: true,
      text: 'Acepto recibir promociones y descuentos',
    },
    items: [{
        name: 'facebook',
        url: 'https://www.facebook.com/',
        title: 'Regístrate con Facebook',
        hasImg: 'https://www.skyairline.com/Content/img/external/facebook.png',
      },
      {
        name: 'google',
        url: 'https://www.google.com/',
        title: 'Regístrate con Google',
        hasImg: 'https://www.skyairline.com/Content/img/external/gmail.png',
      },
    ],
  },
  languageBar: {
    label: 'Chile',
    items: [{
        title: 'Chile',
        url: '#',
        image: 'chile.jpg',
        status: true,
      },
      {
        title: 'English',
        url: '#',
        image: 'us.jpg',
      },
      {
        title: 'Otros (Español)',
        url: '#',
        image: 'españa.jpg',
      },
      {
        title: 'Argentina',
        url: '#',
        image: 'argentina.jpg',
      },
      {
        title: 'Perú',
        url: '#',
        image: 'peru.jpg',
      },
      {
        title: 'Uruguay',
        url: '#',
        image: 'uruguay.jpg',
      },
      {
        title: 'Brasil',
        url: '#',
        image: 'brasil.jpg',
      },
    ],
  },
  copyright: {
    text: '&copy; 1999-2018 SKY Airline. Todos los derechos reservados.- SKY Airline - Av Del Valle Sur 537 - Santiago / Chile',
  },
  poweredby: {
    label: 'Powered by',
    linkUrl: 'http://newshore.es',
  },
  mainOffers: {
    gridCols: 'grid-col col-12 col-lg-3 col-xs-6 col-md-3',
    title: '<span class=\'text-color1\'>Vuela</span> low cost con SKY',
    offers: [{
        isOfferDeal: true,
        icon: 'ticket',
        title: 'Tarifa Light y plus',
        caption: '',
        button: 'Ver más',
        buttonUrl: '',
      },
      {
        isOfferDeal: true,
        icon: 'servicios_adicionales',
        title: 'Servicios adicionales',
        caption: '',
        button: 'Ver más',
        buttonUrl: '',
      },
      {
        isOfferDeal: true,
        icon: 'nuestros_destinos',
        title: 'Nuestros destinos',
        caption: '',
        button: 'Ver más',
        buttonUrl: '',
      },
      {
        isOfferDeal: true,
        icon: 'tarifario_servicios',
        title: 'Tarifario de servicios',
        caption: '',
        button: 'Ver más',
        buttonUrl: '',
      },
    ],
  },
  destinationOffersPrice: {
    title: 'Destinos destacados desde',
    selector: 'Santiago de Chile',
    selectorOptions: [{
        name: 'La Paz',
      },
      {
        name: 'Punta del Este',
      },
      {
        name: 'Mengomeyén',
      },
      {
        name: 'Sao Paulo',
      },
      {
        name: 'Santiago',
      },
    ],
    destinationOptionTitle: 'Destinations offers',
    destinationOptions: {
      items: [{
          labelFrom: '',
          text: '+ Tasas fiscales $7.428 Precio final por tramo desde',
          country: 'Antofagasta',
          price: '25.418',
          promoPrice: '17.990',
          link: {
            url: '#',
            title: 'Obtenga los mejores vuelos',
            label: 'Ver vuelos',
          },
        },
        {
          labelFrom: '',
          text: '+ Tasas fiscales $7.428 Precio final por tramo desde',
          country: 'Concepción',
          price: '19.418',
          promoPrice: '17.990',
          link: {
            url: '#',
            title: 'Obtenga los mejores vuelos',
            label: 'Ver vuelos',
          },
        },
        {
          labelFrom: '',
          text: '+ Tasas fiscales $7.428 Precio final por tramo desde',
          country: 'Calama',
          price: '27.918',
          promoPrice: '17.990',
          link: {
            url: '#',
            title: 'Obtenga los mejores vuelos',
            label: 'Ver vuelos',
          },
        },
        {
          labelFrom: '',
          text: '+ Tasas fiscales $7.428 Precio final por tramo desde',
          country: 'La Serena',
          price: '16.918',
          promoPrice: '17.990',
          link: {
            url: '#',
            title: 'Obtenga los mejores vuelos',
            label: 'Ver vuelos',
          },
        },
        {
          labelFrom: '',
          text: '+ Tasas fiscales $7.428 Precio final por tramo desde',
          country: 'Buenos Aires',
          price: '66.378',
          promoPrice: '17.990',
          link: {
            url: '#',
            title: 'Obtenga los mejores vuelos',
            label: 'Ver vuelos',
          },
        },
        {
          labelFrom: '',
          text: '+ Tasas fiscales $7.428 Precio final por tramo desde',
          country: 'Antofagasta',
          price: '25.418',
          promoPrice: '17.990',
          link: {
            url: '#',
            title: 'Obtenga los mejores vuelos',
            label: 'Ver vuelos',
          },
        },
        {
          labelFrom: '',
          text: '+ Tasas fiscales $7.428 Precio final por tramo desde',
          country: 'La Serena',
          price: '16.918',
          promoPrice: '17.990',
          link: {
            url: '#',
            title: 'Obtenga los mejores vuelos',
            label: 'Ver vuelos',
          },
        },
        {
          labelFrom: '',
          text: '+ Tasas fiscales $7.428 Precio final por tramo desde',
          country: 'Concepción',
          price: '19.418',
          promoPrice: '17.990',
          link: {
            url: '#',
            title: 'Obtenga los mejores vuelos',
            label: 'Ver vuelos',
          },
        },
      ],
    },
  },
  destinationOffers: {
    links: {
      items: [{
          icon: 'desde',
          caption: {
            title: 'Buscador de tarifas',
            text: 'Encuentra la tarifa <span class=\'color\'>mas economica del mes</span>',
          },
        },
        {
          icon: 'desde',
          caption: {
            title: '¿Tarifa <span class=\'color\'><strong>Light</strong></span> o <span class=\'color\'><strong>Plus</strong></span>?',
            text: 'Ver los videos y conocelas',
          },
        },
        {
          icon: 'desde',
          caption: {
            title: 'Centro de ayuda',
            text: 'Resuelve tu dudas o consultas',
          },
        },
      ],
    },
    main: {
      items: [{
        isMain: true,
        emptyOffer: true,
        city: '',
        from: 'Vuela desde',
        price: '',
        img: 'offer-argentina',
        url: '#',
        offerLink: {
          caption: 'Vuela ahora',
          url: '#',
        },
      }, ],
    },
    aside: {
      items: [{
        isMain: false,
        city: 'Santiago de Chile',
        text: 'Destino destacado',
        img: 'santiago-chile',
        url: '#',
        offerLink: {
          caption: 'Ver todos los destinos',
          url: '#',
        },
      }, ],
    },
  },
  mapRoutesHome: {
    title: 'Mapa de Rutas',
    introdution: 'Paranair te ofrecen una gran variedad de destinos y el mejor servicio en aeropuerto y a bordo. Conoce a dónde volamos',
    img: 'maproutes.png',
    button: {
      text: 'info',
      url: 'javascript:void(0)',
    },
  },
  destinations: {
    items: [{
        countryName: 'Centro',
        cities: [{
          name: 'Santiago',
          airportcode: 'IQQ',
        }, ],
      },
      {
        countryName: 'Norte',
        cities: [{
            name: 'Arica',
            airportcode: 'AEP',
          },
          {
            name: 'Iquique',
            airportcode: 'EZE',
          },
          {
            name: 'Calama',
            airportcode: 'SLA',
          },
          {
            name: 'Antofagasta',
            airportcode: 'SLA',
          },
          {
            name: 'Capiapó',
            airportcode: 'SLA',
          },
          {
            name: 'La Serena',
            airportcode: 'SLA',
          },
        ],
      },
      {
        countryName: 'Sur',
        cities: [{
            name: 'Concepción',
            airportcode: 'CWB',
          },
          {
            name: 'Temuco',
            airportcode: 'FLN',
          },
          {
            name: 'Valdivia',
            airportcode: 'GIG',
          },
          {
            name: 'Osorno',
            airportcode: 'GRU',
          },
          {
            name: 'Puerto Mott',
            airportcode: 'GRU',
          },
          {
            name: 'Balmaceda',
            airportcode: 'GRU',
          },
          {
            name: 'Punta Arenas',
            airportcode: 'GRU',
          },
        ],
      },
      {
        countryName: 'Internacional',
        cities: [{
            name: 'Florianópolis',
            airportcode: 'GIG',
          },
          {
            name: 'Rio de Janeiro',
            airportcode: 'SCL',
          },
          {
            name: 'Punta del Este',
            airportcode: 'SCL',
          },
          {
            name: 'Mopntevideo',
            airportcode: 'SCL',
          },
          {
            name: 'Buenos Aires',
            airportcode: 'SCL',
          },
          {
            name: 'Rosaria',
            airportcode: 'SCL',
          },
          {
            name: 'Córdoba',
            airportcode: 'SCL',
          },
          {
            name: 'Mendonza',
            airportcode: 'SCL',
          },
          {
            name: 'Lima',
            airportcode: 'SCL',
          },
        ],
      },
    ],
  },
  newsletter: {
    title: 'Suscribiéndote a <strong>skynews</strong> podrás',
    intro: '',
    canBeClosed: true,
    closeLabel: 'Close',
    list: {
      items: [{
          text: 'Ser el primero en conocer nuestras ofertas.',
        },
        {
          text: 'Obtener un 20% de descuento en tu maleta comprando por la web',
        },
      ],
    },
    buttonText: '',
    form: {
      hasLabel: false,
      label: 'Subscribe to our newsletter',
      placeholder: 'Quiero registrarme en skynews',
      name: 'newsletterInput',
      id: 'newsletterInput',
    },
  },
}
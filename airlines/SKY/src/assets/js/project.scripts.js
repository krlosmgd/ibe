// local project script to simulate Angular functions
var statusToggle = false;
var indexCurrent = 0;
var indexOld = 0;
var mainHeaderStatus = false;
$(document).ready(function () {
    var W = $(window).outerWidth();

    if ($('.week').length > 0) {
        $('.week').each(function(){
            var weekText = $(this).text();
            if (W < 640) {
                len=$(this).text().replace(/ /g,'').length;
                $(this).text($(this).text().substr(0,3));
            } else {
                $(this).text(weekText);
            }
        });
    }


    var body = $('body');

    /*** SUMMARY ***/
    // ------------------------------------

    // ----------------------------------------------------------------

    function fixedSummaryScrolling() {
        if ($(window).scrollTop() > stickyCombinedBar) {
            body.addClass('search-fixed');
        } else {
            body.removeClass('search-fixed');
        }  
    }
   
    // Main header collapse navs
    $('.main-header_nav-primary .nav_item button.nav_item_link').on('click', function() {
        mainHeaderCollapse($(this), $('.main-header_nav-primary .nav_item button.nav_item_link'));
    });

    $('.main-header_nav-shortcut .nav_item button.nav_item_link').on('click', function() {
        mainHeaderCollapse($(this), $('.main-header_nav-shortcut .nav_item button.nav_item_link'));
    });

    // Main header collapse navs primary
    $('.main-header_navbar-toggle.toggle1').on('click', function() {
        if($('.main-header_nav').hasClass("open-login") && statusToggle) {
            $('.main-header_nav').removeClass("open-login");
        } else {
            mainHeaderNavbarToggle(statusToggle);
        }
    });

    // function modal login and register
    $('.modal-login .register-form').hide();
    $('.modal-login .login-form .auth-form_footer a:eq(0)').on('click', function(event) {
        event.preventDefault();
        $('.modal-login .login-form').hide();
        $('.modal-login .register-form').show();
    });
    $('.modal-login .register-form .auth-form_footer a:eq(0)').on('click', function(event) {
        event.preventDefault();
        $('.modal-login .register-form').hide();
        $('.modal-login .login-form').show();
    });
    $(window).scroll(function() {
        //fixedSummaryScrolling();
    });

    // Close newsletter alert when click close button
    var newsletterForm = $('.newsletter');
    var newsletterFormCloseBtb = $('.newsletter_close-btn')
    newsletterFormCloseBtb.on('click', function() {
        newsletterForm.css('display', 'none');
    })

    // Select fare, add fare to button and hide collapse
    var flights = $('.flight');
    var flightButtons = $('.flight .price-rate');
    flightButtons.each(function() {
        $(this).click(function() {
            var flight = $(this).parents(':eq(2)');
            flights.removeClass('is-selected');
            flight.addClass('is-selected');
        });
    });

    // Fares simulation // Select fare, add fare to button and hide collaps
    var fares = $('.flights_fares_item');
    fares.each(function() {
        $(this).click(function(e) {
            var priceRateButton = $(this).parents(':eq(3)').find('.price-rate');
            var flight = $(this).parents(':eq(3)');
            var flightTable = $(this).parents(':eq(4)');
            var fareCollapse = $(this).parents(':eq(1)');
            priceRateButton.find('.fare-label').remove();

            // // add span with farename inside button
            // if ($(this).hasClass('fare1')) {
            //     priceRateButton.removeClass('fare2', 'fare3');
            //     priceRateButton.addClass('fare1');
            //     priceRateButton.prepend('<span class="fare-label"><span class="hide-visually">Selected fare</span>ZERO</span>');
            // } else if ($(this).hasClass('fare2')) {
            //     priceRateButton.removeClass('fare1', 'fare3');
            //     priceRateButton.addClass('fare2');
            //     priceRateButton.prepend('<span class="fare-label"><span class="hide-visually">Selected fare</span>PLUS</span>');
            // } else {
            //     priceRateButton.removeClass('fare1', 'fare2');
            //     priceRateButton.addClass('fare3');
            //     priceRateButton.prepend('<span class="fare-label"><span class="hide-visually">Selected fare</span>FULL</span>');
            // }

    //         fares.each(function() {
    //             $(this).removeClass('selected');
    //         })
    //         flightTable.find('.flight').each(function() {
    //             $(this).removeClass('is-selected');
    //         });
    //         flight.addClass('is-selected');
    //         $(this).addClass('selected');
    //         fareCollapse.collapse('hide');
    //     });
    // });


    // On Resize
    // $(window).on('resize',function(){
    //     resetSummary();
    // });
});

function mainHeaderCollapse(element, trigger) {
    var status = Boolean(mainHeaderStatus);
    var navItemTrigger = trigger;
    var bodyStatus = $("body");
    indexCurrent = element.parent().index();
    var shortcut = navItemTrigger.parent().eq(indexCurrent).parents().find('.main-header_nav-shortcut').attr('class');
    if (indexCurrent != indexOld) {
        navItemTrigger.parent().eq(indexOld).children("button.nav_item_link").attr("aria-expanded", "false");
        if (shortcut == "main-header_nav-shortcut") {
            navItemTrigger.parent().eq(indexOld).removeClass('active');
            navItemTrigger.parent().eq(indexCurrent).addClass('active');
        }
        status = true;
        !bodyStatus.hasClass("overlay-in-md") ? bodyStatus.addClass("overlay-in") : ''
        navItemTrigger.parent().eq(indexCurrent).children("button.nav_item_link").attr("aria-expanded", "true");
    } else {
        if(!status) {
            status = true;
            element.attr("aria-expanded", "true");
            if (shortcut == "main-header_nav-shortcut") {
                navItemTrigger.parent().eq(indexCurrent).addClass('active');
            }
            !bodyStatus.hasClass("overlay-in-md") ? bodyStatus.addClass("overlay-in") : '';
        } else {
            status = false;
            element.attr("aria-expanded", "false");
            if (shortcut == "main-header_nav-shortcut") {
                navItemTrigger.parent().eq(indexCurrent).removeClass('active');
            }
            !bodyStatus.hasClass("overlay-in-md") ? bodyStatus.removeClass("overlay-in") : '';
        }
    }
    mainHeaderStatus = status;
    indexOld = indexCurrent;
}

function mainHeaderNavbarToggle() {
    var bodyStatus = $("body");
    if(!statusToggle) {
        statusToggle = !statusToggle;
        $('.main-header_nav').addClass("collapse");
        $('.main-header_nav').show();
        $('.main-header_nav-shortcut').hide();
        bodyStatus.addClass("overlay-in-md");
    } else {
        statusToggle = !statusToggle;
        $('.main-header_nav').removeAttr("style", "");
        $('.main-header_nav').removeClass("collapse");
        $('.main-header_nav-shortcut').show();
        bodyStatus.removeClass("overlay-in-md");
    }
}
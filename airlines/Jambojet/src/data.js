const fares = {
  fare1: {
    name: 'Economy',
  },
  fare2: {
    name: 'Economy+',
  },
  fare3: {
    name: 'Business',
  }
}

//## search destinations options
const destinations = {
  sortBy: 'city', // 'country' | 'city' // order instruction
  items: [
    {
      countryName: '',
      cities: [
        {
          name: 'Nairobi (JKIA)',
          airportcode: 'NBO',
        }, 
        {
          name: 'Eldoret',
          airportcode: 'EDL',
        },
        {
          name: 'Kisumu',
          airportcode: 'KIS',
        },
        {
          name: 'Mombasa',
          airportcode: 'MBA',
        },
        {
          name: 'Malindi',
          airportcode: 'MYD',
        },
        {
          name: 'Ukunda (Diani)',
          airportcode: 'UKA',
        },
        {
          name: 'Entebbe',
          airportcode: 'EBB',
        }
    ],
    }
  ]
};

function getDestinations() {
  switch (destinations.sortBy) {
    case 'country':
      break;
    case 'city':
      // format items
      const formatedDestinations = [];
      destinations.items.forEach((destination) => {
        destination.cities.forEach((city) => {
          formatedDestinations.push({
            cities: [{
              name: city.name,
              airportcode: city.airportcode,
              countryName: destination.countryName,
            }]
          });
        });
      });
      // sort items
      formatedDestinations.sort((a, b) => {
        if (a.cities[0].name < b.cities[0].name)
          return -1;
        if (a.cities[0].name > b.cities[0].name)
          return 1;
        return 0;
      });
      return formatedDestinations;
  }
  return destinations.items;
}

module.exports = {
  projectName: 'Jambojet',
  site_title: 'Jambojet [HTML]',
  demo_imgs_path: '../src/assets/imgs/',
  core_assets_path: '../../../core/src/assets/',
  project_assets_path: '../src/assets/',
  logo_img: 'logo.svg',
  logo_negative_img: 'logo-neg.svg',
  pathFont: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700&amp;subset=latin-ext',
  currencyPosition: 'left',
  decimal: ',00',
  SHARED: {
    currency: 'KES',
    weightmeasure: 'Kg',
    promoLabelDefault: 'Promo',
    formRequiredSymbol: '*',
    formRequiredText: '* Campos obligatorios',
    from: 'From',
    day: 'día',
    nextFlight: 'Siguiente vuelo',
    flightNotavailable: 'El vuelo no está disponible.<br/>El horario de salida y el de regreso no coinciden.',
    close: 'Close',
    by: 'by',
    return: 'back',
  },
  showUserAccount: false,
  header: {
    headerRight: {
      show: true,
      items: [{
        dataType: 'languages',
      }, ],
    },
    nav: {
      itemsAlign: 'left',
      items: [{
          caption: 'Reservar',
        },
        {
          caption: 'Gestionar',
        },
        {
          caption: 'Información',
        },
      ],
    },
  },
  footer: {
    logo: {
      show: false
    },
  },
  navFixed: {
    ibe: false,
    corporate: true
  },
  bookingRetrieve: {
    formHead: {
      title: '',
    },
    formTypeClass: 'booking-retrieve',
    buttonText: 'Iniciar Check-in',
    isSocial: false,
    forgotPassword: '',
    hasGetText: false,
    data: {
      formGroups: [{
        elements: [{
          elementsGroup: [{
              type: 'inputForm',
              element: {
                label: 'Referencia de reserva/PNR',
                type: 'text',
                value: 'AMRHGL',
                id: 'pnr'
              },
            },
            {
              type: 'inputForm',
              element: {
                label: 'Apellido',
                type: 'text',
                id: 'lastName'
              },
            },
          ],
        }, ],
      }, ],
    },
  },
  multipleSearch: {
    show: false,
    active: 1,
    items: [{
        link: '',
        label: 'Search for flights',
        icon: 'icon--flights',
        controls: 'multipleSearchFlights_tab',
        id: 'multipleSearchFlights',
        tabindex: -1,
        contentInclude: 'partials/components/IBE/flight-search/_flight-search.njk',
      },
      {
        link: {
          url: '',
          title: 'Open in new window',
          target: '_blank',
        },
        label: 'Search for Hotels',
        icon: 'icon--hotels',
        controls: 'multipleSearchHotels_tab',
        id: 'multipleSearchHotels',
        tabindex: -1,
      },
      {
        link: {
          url: '',
          title: '',
        },
        label: 'Search for Flights and Hotels',
        icon: 'icon--flightshotels',
        controls: 'multipleSearchHotelsFlights_tab',
        id: 'multipleSearchHotelsFlights',
        tabindex: -1,
      },
    ],
  },
  bookingSteps: [{
      stepNumber: '1',
      stepLabel: 'Vuelo',
      stepUrl: '',
    },
    {
      stepNumber: '2',
      stepLabel: 'Pasajeros',
      stepUrl: '',
    },
    {
      stepNumber: '3',
      stepLabel: 'Pago',
      stepUrl: '',
    },
  ],
  multipleCalendarOptions: false,
  months: [{
      month: 'Diciembre',
      year: '2017',
    },
    {
      month: 'Enero',
      year: '2018',
      selected: true,
    },
    {
      month: 'Febrero',
      year: '2018',
    },
    {
      month: 'Marzo',
      year: '2018',
    },
    {
      month: 'Abril',
      year: '2018',
    },
    {
      month: 'Mayo',
      year: '2018',
    },
    {
      month: 'Junio',
      year: '2018',
    },
    {
      month: 'Julio',
      year: '2018',
    },
    {
      month: 'Agosto',
      year: '2018',
    },
    {
      month: 'Septiembre',
      year: '2018',
    },
  ],
  days: [{
      day: '3 JUN',
      week: 'Domingo',
      price: '299',
    },
    {
      day: '4 JUN',
      week: 'Lunes',
      price: '477',
      selected: true,
    },
    {
      day: '5 JUN',
      week: 'Martes',
      price: '477',
    },
    {
      day: '6 JUN',
      week: 'Miércoles',
      price: '325',
    },
    {
      day: '7 JUN',
      week: 'Jueves',
      price: '477',
      bestprice: true,
    },
    {
      day: '8 JUN',
      week: 'Viernes',
      price: '566',
    },
    {
      day: '9 JUN',
      week: 'Sábado',
      price: '644',
    },
    {
      day: '31 AGO',
      week: 'Domingo',
      price: '1.115.000',
    },
    {
      day: '01 Jan',
      week: 'Lunes',
      price: '1.115.000',
    },
    {
      day: '02 Jan',
      week: 'Martes',
      price: '1.115.000',
    },
    {
      day: '03 Jan',
      week: 'Miércoles',
      price: '1.115.000',
    },
  ],
  journeys: [{
      'class': 'outbound',
      way: 'Ida',
      headerConnector: '-',
      headerImgSrc: '',
      departureAirport: 'Madrid',
      departureAirportCode: 'Mad',
      departureAirportTerminal: 'Terminal 1',
      arrivalAirport: 'San Antonio de Palé',
      arrivalAirportCode: 'NBN',
      arrivalAirportTerminal: 'Terminal 4',
      hasExtraDayLabel: false,
      extraDay: '+1',
      date: {
        week: 'Miércoles',
        weekAbbr: 'Mie',
        day: '8',
        month: 'Agosto',
        monthAbbr: 'Ago',
        year: '2018',
      },
      connections: {
        nonstop: {
          style: '',
          text: '',
        },
        direct: {
          style: '',
          text: '',
        },
        connecting: {
          style: 'tooltip',
          text: '1 stop',
        },
        stopover: {
          style: '',
          text: '',
        },
      },
      flightNumbers: [{
        number: '1234',
        operator: 'XX',
      }, ],
      fare: 1,
      fareSelectedName: 'Economy+',
    },
    {
      'class': 'inbound',
      way: 'Vuelta',
      headerConnector: '-',
      headerImgSrc: '',
      departureAirport: 'San Antonio de Palé',
      departureAirportCode: 'NBN',
      departureAirportTerminal: 'Terminal 2',
      arrivalAirport: 'Madrid',
      arrivalAirportCode: 'Mad',
      arrivalAirportTerminal: 'Terminal 4',
      hasExtraDayLabel: false,
      date: {
        week: 'Miércoles',
        weekAbbr: 'Mie',
        day: '15',
        month: 'Ago',
        year: '2018',
      },
      // connections: {
      //   extraDay: '',
      //   nonstop: {
      //     style: '',
      //     text: '',
      //   },
      //   direct: {
      //     style: '',
      //     text: '',
      //   },
      //   connecting: {
      //     style: '',
      //     text: '',
      //   },
      //   stopover: {
      //     style: '',
      //     text: '',
      //   },
      // },
      flightNumbers: [{
        number: '1234',
        operator: 'xx',
      }, ],
      fare: 3,
      fareSelectedName: 'Economy',
    },
  ],
  flightCodeTimeSeparator: '',
  flightCaption: 'Vuelo:',
  flightLabelOutbound: '',
  flightLabelInbound: '',
  flightExtraDaysPosition: 'right',
  flightSelectedFareButtonSelectable: false,
  flightScale: {
    title: 'Detalle del vuelo',
    subtitle: 'Miércoles 6 Junio 2018',
    timeTotalText: 'Tiempo total del viaje:',
    timeTotal: '14h 00m',
    departureTime: '7:40',
    departureAirport: 'Madrid',
    departureAirportCode: 'MAD',
    departureDuration: '2h 20m',
    flightNumberDeparture: 'Vuelo XX-1234',
    scales: [{
        title: 'Dakar',
        time: '10:00',
        departureAirportCode: 'DKR',
        transit: {
          time: '10h 10m',
          title: 'Tiempo de transito',
        },
      },
      {
        title: 'Dakar',
        time: '22:00',
        departureAirportCode: 'DKR',
        isFinalizeFlight: true,
      },
    ],
    arrivalTime: '23:30',
    arrivalAirport: 'Abidjan',
    arrivalAirportCode: 'ABJ',
    arrivalDuration: '1h 30m',
    flightNumberArrival: 'Vuelo XX-1234',
  },
  flights: {
    items: [{
        departureTime: '09:30',
        departureAirport: 'Madrid',
        departureAirportCode: 'MAD',
        departureAirportTerminal: 'Terminal 2',
        duration: '6:00h',
        extraDay: '',
        connections: {
          nonstop: {
            style: '',
            text: '',
          },
          direct: {
            style: '',
            text: 'Direct',
          },
          connecting: {
            style: '',
            text: '',
          },
          stopover: {
            style: '',
            text: '',
          },
        },
        arrivalTime: '15:30',
        arrivalAirport: 'Madrid',
        arrivalAirportCode: 'MAD',
        arrivalAirportTerminal: 'Terminal 2',
        priceFrom: '447',
        fareSelected: false,
        fareSelectedName: fares.fare2.name,
        fareType: 'fare2',
        hasPromo: false,
        unavailable: true,
      },
      {
        departureTime: '16:20',
        departureAirport: 'Madrid',
        departureAirportCode: 'MAD',
        departureAirportTerminal: 'Terminal 2',
        duration: '6:00h',
        extraDay: '+1',
        connections: {
          nonstop: {
            style: '',
            text: '',
          },
          direct: {
            style: 'modal',
            text: 'Direct',
          },
          connecting: {
            style: '',
            text: '',
          },
          stopover: {
            style: '',
            text: '',
          },
        },
        arrivalTime: '22:20',
        arrivalAirport: 'San Antonio de Palé',
        arrivalAirportCode: 'NBN',
        arrivalAirportTerminal: 'Terminal 2',
        priceFrom: '447.000',
        fareSelected: false,
        fareSelectedName: fares.fare3.name,
        fareType: 'fare3',
        hasPromo: false,
        isFaresOpen: {
          active: false,
          label: 'Cerrar',
        },
      },
      {
        departureTime: '23:30',
        departureAirport: 'Madrid',
        departureAirportCode: 'MAD',
        departureAirportTerminal: 'Terminal 2',
        duration: '6:00h',
        extraDay: '+2',
        connections: {
          nonstop: {
            style: '',
            text: '',
          },
          direct: {
            style: '',
            text: '',
          },
          connecting: {
            style: 'popover',
            text: '1 Stop',
          },
          stopover: {
            style: '',
            text: '',
          },
        },
        arrivalTime: '6:00',
        arrivalAirport: 'San Antonio de Palé',
        arrivalAirportCode: 'NBN',
        arrivalAirportTerminal: 'Terminal 2',
        priceFrom: '447',
        fareSelected: false,
        fareSelectedName: fares.fare3.name,
        fareType: 'fare3',
        hasPromo: false,
        isFaresOpen: {
          active: false,
          label: 'Cerrar',
        },
      },
      {
        departureTime: '09:30',
        departureAirport: 'Madrid',
        departureAirportCode: 'MAD',
        duration: '6:00h',
        extraDay: '+1',
        connections: {
          nonstop: {
            style: '',
            text: '',
          },
          direct: {
            style: '',
            text: '',
          },
          connecting: {
            style: 'popover',
            text: '1 Stop',
          },
          stopover: {
            style: '',
            text: '',
          },
        },
        arrivalTime: '16:30',
        arrivalAirport: 'San Antonio de Palé',
        arrivalAirportCode: 'NBN',
        priceFrom: '1.108',
        fareSelected: false,
        fareSelectedName: fares.fare2.name,
        fareType: 'fare2',
        hasPromo: false,
        unavailable: false,
        isFaresOpen: {
          active: false,
          label: "Cerrar"
        }
      },
    ],
  },
  paxSelector: {
    title: 'Pasajeros',
    label: 'Asiento',
    nextButton: '',
  },
  passengersData: {
    passengers: [{
        label: 'Adulto 1:',
        gender: '',
        firstName: 'Jorge',
        lastName: 'García',
        type: 'Adulto',
        seat: '4B',
        seatStatus: '',
        checkedIn: true,
        hasInfoButton: false,
      },
      {
        label: 'Niño 1:',
        gender: '',
        firstName: 'Carmen',
        lastName: 'María Galeano',
        type: 'Niño',
        seat: '4A',
        seatStatus: 'selected',
        statusInput: 'has-error',
        messageInput: 'Error message',
        checkedIn: false,
        hasInfoButton: false,
      },
    ],
  },
  seatmap: {
    seatsHasPopover: true,
    letters: [{
        letter: 'A',
      },
      {
        letter: 'C',
      },
      {
        letter: '',
      },
      {
        letter: 'D',
      },
      {
        letter: 'F',
      },
    ],
    seats: [{
        seats_group: [{
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '1',
          },
          {
            status: 'empty',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'empty',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '2',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '3',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '4',
          },
          {
            status: 'current',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'selected',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '5',
          },
          {
            status: 'selected',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'selected',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '6',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '7',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: '',
            seat_exit_first: true,
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '8',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            seat_exit_last: true,
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '9',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '10',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '11',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '12',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '13',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '14',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
    ],
  },
  seatsLegend: {
    hasResponsiveModal: false,
    desc: {
      title: '',
    },
    footnote: '',
    items: [{
        type: '',
        text: 'Disponible',
        price: '',
        description: '',
      },
      {
        type: 'exit',
        text: 'Salida de emergencia',
        price: '',
        description: '',
      },
      {
        type: 'unavailable',
        text: 'No Disponible',
        price: '',
        description: '',
      },
      {
        type: 'selected',
        text: 'Seleccionado',
        price: '',
        description: '',
      },
    ],
  },
  faresCompare: {
    trigger: {
      text: 'Compare Fares',
    },
    head: {
      text: 'Compare the fares',
    },
  },
  fares: {
    faresLayoutTabs: false,
    faresClickUrl: '',
    localText: 'Doméstico',
    internationalText: 'Internacional',
    notApplyFareText: 'Sin cargo',
    fareText: 'Cargo',
    currency: 'USD',
    hasFaresCompare: true,
    items: [{
        pos: '1',
        name: fares.fare1.name,
        isPromo: false,
        price: '477',
        unavailable: false,
        selected: false,
        fareButtonText: 'Agotada',
        unavailable: true,
        farePrice: '',
        fareDescription: [{
            icon: '',
            description: 'Fare difference and change fee applies',
            fareLabel: false,
            localPrice: false,
            internationalPrice: false,
          },
          {
            icon: '',
            description: 'Carry-on baggage (0kg)',
            fareLabel: false,
            localPrice: false,
            internationalPrice: false,
          },
          {
            icon: '',
            description: 'Check-in baggage (25kg)',
            fareLabel: false,
            localPrice: false,
            internationalPrice: false,
          },
        ],
      },
      {
        pos: '2',
        name: fares.fare2.name,
        isPromo: false,
        price: '758',
        unavailable: false,
        selected: false,
        fareButtonText: '',
        farePrice: '758',
        fareDescription: [{
            icon: '',
            description: 'Fare difference and change fee applies',
            fareLabel: false,
            localPrice: false,
            internationalPrice: false,
          },
          {
            icon: '',
            description: 'Sweet Seat',
            fareLabel: false,
            localPrice: false,
            internationalPrice: false,
          },
          {
            icon: '',
            description: 'Carry-on baggage (7kg)',
            fareLabel: false,
            localPrice: false,
            internationalPrice: false,
          },
          {
            icon: '',
            description: 'Check-in baggage (30kg)',
            fareLabel: false,
            localPrice: false,
            internationalPrice: false,
          },
          {
            icon: '',
            description: 'Refunds',
            fareLabel: false,
            localPrice: false,
            internationalPrice: false,
          },
        ],
      },
      {
        pos: '3',
        name: fares.fare3.name,
        isPromo: false,
        price: '1.108',
        unavailable: false,
        selected: false,
        fareButtonText: '',
        farePrice: '447',
        fareDescription: [{
            icon: '',
            description: 'Fare difference and change fee applies',
            fareLabel: false,
            localPrice: false,
            internationalPrice: false,
          },
          {
            icon: '',
            description: 'Sweet Seat',
            fareLabel: false,
            localPrice: false,
            internationalPrice: false,
          },
          {
            icon: '',
            description: 'Carry-on baggage (7kg)',
            fareLabel: false,
            localPrice: false,
            internationalPrice: false,
          },
          {
            icon: '',
            description: 'Check-in baggage (30kg)',
            fareLabel: false,
            localPrice: false,
            internationalPrice: false,
          },
          {
            icon: '',
            description: 'Refunds',
            fareLabel: false,
            localPrice: false,
            internationalPrice: false,
          },
          {
            icon: '',
            description: 'Free Plaza Premium Lounge',
            fareLabel: false,
            localPrice: false,
            internationalPrice: false,
          },
          {
            icon: '',
            description: 'Priority Baggage Retrieval',
            fareLabel: false,
            localPrice: false,
            internationalPrice: false,
          },
        ],
      },
    ],
  },
  baggages: [{
      title: 'Departing Baggage',
      type: [{
          price: '229',
          size: '20',
          promo: true,
        },
        {
          price: '360',
          size: '25',
          promo: true,
        },
        {
          price: '365',
          size: '30',
          promo: true,
        },
      ],
    },
    {
      title: 'Returning Baggagee',
      type: [{
        price: '365',
        size: '30',
        promo: false,
      }, ],
    },
  ],
  baggagesOW: [{
    title: '',
    type: [{
        price: '229',
        size: '20',
        promo: true,
      },
      {
        price: '360',
        size: '25',
        promo: true,
      },
      {
        price: '365',
        size: '30',
        promo: true,
      },
    ],
  }, ],
  specialBaggages: [{
      title: 'Departing Oversized Baggage',
      type: [{
          price: '229',
          size: '20',
          promo: true,
        },
        {
          price: '360',
          size: '25',
          promo: true,
        },
        {
          price: '365',
          size: '30',
          promo: true,
        },
      ],
    },
    {
      title: 'Returning Oversized Baggage',
      type: [{
        price: '365',
        size: '30',
        promo: false,
      }, ],
    },
  ],
  specialBaggagesOW: [{
    type: [{
        price: '229',
        size: '20',
        promo: true,
      },
      {
        price: '360',
        size: '25',
        promo: true,
      },
      {
        price: '365',
        size: '30',
        promo: true,
      },
    ],
  }, ],
  meals: [{
      name: 'Thai Red Curry Chicken with Rice Thai Red Curry Chicken with Rice',
      image: 'meal_01.png',
      price: '70',
    },
    {
      name: 'Japanese Beef Bento',
      image: 'meal_02.png',
      price: '105',
    },
    {
      name: 'Dim Sum Set',
      image: 'meal_03.png',
      price: '70',
      active: true,
    },
    {
      name: 'Evian Natural Mineral Water 330ml',
      image: '',
      price: '15',
    },
  ],
  searchResultConnector: '-',
  searchResultHasTypology: false,
  editSearchButtonText: 'Editar',
  summaryData: [{
    //## connection position
    /*
      1 - right side of flight number
      2 - center below flight connector icon
    */
    connectionsPosition: 2,
    isCollapse: false,
    hasPriceResume: false,
    hasButtonContinue: true, // button continue next step
    notification: { // if have notification
      title: 'Añadido al carro'
    },
    fareByFlight: true,
    showBottomTotal: true,
    showBottomTotalLabel: 'Total',
    buttonClose: 'Cerrar',
    moreDetailsCollapsible: false,
    moreDetailsStyle: 'simple',
    summaryCollapseTitle: 'Resumen',
    totalCostValue: '1.433',
    totalCostLabel: '',
    legalTerms: '',
    totalFinal: {
      label: 'Total: ',
    },
  }, ],
  summaryTypology: {
    showTitle: false,
    isCollapsible: true,
    totalPrice: 'AUX 1.333,00',
    resumePrices: [
      [{
          label: '1 x Adulto',
          price: '300€',
        },
        {
          label: '1 x Niño',
          price: '300€',
        },
        {
          label: '1 x bebe',
          price: '300€',
        },
      ],
      [{
          label: '2 x Tasas',
          price: '300€',
        },
        {
          label: '1 x Tasas bebe',
          price: '133€',
        },
      ],
    ],
  },
  summaryDetailServices: {
    caption: 'Summary details - services',
    thead: {
      ths: [{
        title: 'Servicios',
      }, ],
    },
    tbody: [{
        tds: [{
            title: 'Asientos',
            isTh: true,
          },
          {
            title: 'Incluido',
          },
        ],
      },
      {
        tds: [{
            title: 'Equipaje extra',
            isTh: true,
          },
          {
            title: 'Incluido',
          },
        ],
      },
      {
        tds: [{
            title: '2 x seguro de viaje',
            isTh: true,
          },
          {
            price: {
              value: 100,
            }
          },
        ],
      },
      {
        tds: [{
            title: '<strong>TOTAL SERVICIOS</strong>',
            isTh: true,
          },
          {
            price: {
              value: 100,
            }
          },
        ],
      },
    ],
  },
  bookingReference: {
    label: 'Código de reserva / PNR',
    code: 'ASDLKJ',
  },
  segmentSelectorIsTabs: true,
  segmentSelector: {
    hasButton: true,
    alertText: '',
    flightLabel: '',
    flights: [{
        from: 'SRE',
        to: 'VVI',
        fromCity: 'Sucre',
        toCity: 'Sant Cruz',
        date: 'Miércoles 28 Agosto 2018',
        flightNumber: 'Z8 821',
        disabled: false,
        active: true,
      },
      {
        from: 'VVI',
        to: 'LPB',
        fromCity: 'Santa Cruz',
        toCity: 'El Alto',
        date: 'Sábado 31 Agosto 2018',
        flightNumber: 'PR0044',
        disabled: false,
        active: false,
      },
      {
        from: 'LPB',
        to: 'VVI',
        fromCity: 'El Alto',
        toCity: 'Santa Cruz',
        date: 'Sábado 31 Agosto 2018',
        flightNumber: 'PR0049',
        disabled: false,
        active: false,
      },
      {
        from: 'VVI',
        to: 'SRE',
        fromCity: 'Santa Cruz',
        toCity: 'Sucre',
        date: 'Sábado 31 Agosto 2018',
        flightNumber: 'PR0054',
        disabled: false,
        active: false,
      },
    ],
  },
  paymentData: {
    methods: {
      items: [{
        isCurrent: true,
        name: 'Credit Card',
        hasCurrencyOptions: false,
        title: '',
        titleTag: '',
        id: 'creditcardCollapse',
        image: 'creditcard.svg',
        imageSelected: 'creditcard-selected.svg',
        template: '_cc',
      }, ],
    },
  },
  creditcard: {
    title: 'Tarjeta de crédito',
    options: [{
        label: 'Visa',
        id: 'credit_card_type_VI',
        name: 'creditCardType',
        // imageURL: 'payment-methods/credit-cards/visa.svg',
        imageAlt: 'Visa payment',
        checked: true
      },
      {
        label: 'Mastercard',
        id: 'credit_card_type_MC',
        name: 'creditCardType',
        // imageURL: 'payment-methods/credit-cards/mastercard.svg',
        imageAlt: '',
      },
      {
        label: 'American express',
        id: 'credit_card_type_AE',
        name: 'creditCardType',
        // imageURL: 'payment-methods/credit-cards/american-express.svg',
        imageAlt: '',
      },
    ]
  },
  CreditCardFormData: {
    msgRequired: '* Campos obligatorios',
    formGroups: [{
      elements: [{
        elementsGroup: [{
            type: 'inputForm',
            element: {
              label: 'Tarjeta de crédito*',
            },
          },
          {
            type: 'inputForm',
            element: {
              label: 'Titular de la tarjeta*',
            },
          },
          {
            type: 'selectForm',
            element: {
              label: 'Fecha de caducidad*',
              id: 'ccExpiryDate',
              options: [{
                  name: 'DD',
                  id: 'ccExpiryDateDD',
                },
                {
                  name: 'MM',
                  id: 'ccExpiryDateMM',
                },
                {
                  name: 'YYYY',
                  id: 'ccExpiryDateYYYY',
                },
              ],
            },
          },
          {
            type: 'inputForm',
            element: {
              label: 'Número CW2 / CV2*',
              id: "cw2Cv2"
            },
          },
        ],
      }, ],
    }, ],
  },
  sitemapIBE: [{
      section: 'Booking flow',
      available: true,
      pages: [{
          label: 'Select flight',
          url: 'BF-select_flight.html',
          notAvailable: false,
        },
        {
          label: 'Select flight (one way)',
          url: 'BF-select_flight_OW.html',
          notAvailable: true,
        },
        {
          label: 'Select flight (multicity)',
          url: 'BF-select_flight_multicity.html',
          notAvailable: true,
        },
        {
          label: 'Passenger data',
          url: 'BF-passenger_data.html',
          notAvailable: false,
        },
        {
          label: 'Add extras',
          url: 'BF-services_add.html',
          notAvailable: true,
        },
        {
          label: 'Add extras (one way)',
          url: 'BF-services_add_oneway.html',
          notAvailable: true,
        },
        {
          label: 'Seat selection',
          url: 'BF-seat_selection.html',
          notAvailable: true,
        },
        {
          label: 'Payment',
          url: 'BF-payment.html',
          notAvailable: false,
        },
        {
          label: 'Payment confirmation',
          url: 'BF-payment_confirmation.html',
          notAvailable: false,
        },
        {
          label: 'Payment confirmation (one way)',
          url: 'BF-payment_confirmation_OW.html',
          notAvailable: false,
        },
        {
          label: 'Payment confirmation (QR)',
          url: 'BF-payment_confirmation_QR.html',
          notAvailable: true,
        },
        {
          label: 'Services Add',
          url: 'BF-services_add.html',
          notAvailable: false,
        },
      ],
    },
    {
      section: 'Manage my Booking',
      available: false,
      pages: [{
          label: 'Login',
          url: 'BF-MMB_login.html',
          notAvailable: true,
        },
        {
          label: 'Home',
          url: 'BF-MMB_home.html',
          notAvailable: true,
        },
        {
          label: 'Cancel Flight',
          url: 'BF-MMB_cancel_flight.html',
          notAvailable: true,
        },
        {
          label: 'Change Flight',
          url: 'BF-MMB_change_flight.html',
          notAvailable: true,
        },
        {
          label: 'Guest details',
          url: 'BF-MMB_passenger_data.html',
        },
      ],
    },
    {
      section: 'Check in',
      available: false,
      pages: [{
          label: 'Login',
          url: 'BF-WCI_login.html',
        },
        {
          label: 'Itinerary',
          url: 'BF-WCI_home.html',
        },
        {
          label: 'Itinerary (one way)',
          url: 'BF-WCI_home-OW.html',
        },
        {
          label: 'Passengers',
          url: 'BF-WCI_passenger_data.html',
          notAvailable: true,
        },
        {
          label: 'Seats',
          url: 'BF-WCI_seat_selection.html',
          notAvailable: false,
        },
        {
          label: 'Pre-Confirmation',
          url: 'BF-WCI_pre-confirmation.html',
          notAvailable: true,
        },
        {
          label: 'Pre-Confirmation (one way)',
          url: 'BF-WCI_pre-confirmation-OW.html',
          notAvailable: true,
        },
        {
          label: 'Boarding pass',
          url: 'BF-WCI_home_checkedin.html',
          notAvailable: false,
        },
      ],
    },
    {
      section: 'Agent cabinet',
      available: false,
      pages: [{
          label: 'Login',
          url: 'AC_login.html',
          notAvailable: true,
        },
        {
          label: 'Home',
          url: 'AC_home.html',
          notAvailable: true,
        },
        {
          label: 'Account management',
          url: 'AC_account_management.html',
          notAvailable: true,
        },
        {
          label: 'MMB',
          url: 'AC_mmb.html',
          notAvailable: true,
        },
        {
          label: 'Refunds',
          url: 'AC_refunds.html',
          notAvailable: true,
        },
        {
          label: 'Payment',
          url: 'AC-payment.html',
          notAvailable: true,
        },
        {
          label: 'Payment hold proceed',
          url: 'AC_payment_hold_proceed.html',
          notAvailable: true,
        },
      ],
    },
    {
      section: 'Members',
      available: false,
      pages: [{
          label: 'Login',
          url: 'U-FLY_login.html',
          notAvailable: true,
        },
        {
          label: 'Forgot password',
          url: 'U-FLY_forgot_password.html',
          notAvailable: true,
        },
        {
          label: 'Sign up',
          url: 'U-FLY_sign_up.html',
          notAvailable: true,
        },
        {
          label: 'Confirmation',
          url: 'U-FLY_confirmation.html',
          notAvailable: true,
        },
        {
          label: 'Profile update',
          url: 'U-FLY_profile_update.html',
          notAvailable: true,
        },
      ],
    },
    {
      section: 'Error pages',
      available: false,
      pages: [{
        label: 'Error 404',
        url: '404.html',
        notAvailable: false,
      }, ],
    },
  ],
  sitemapCorp: [{
      section: 'Pages',
      available: true,
      pages: [{
          label: 'Home',
          url: 'CORP-index.html',
          notAvailable: false,
        },
        {
          label: 'Standby',
          url: 'CORP-standby.html',
          notAvailable: false,
        },
        {
          label: 'Corporative Demo',
          url: 'CORP-demo.html',
          notAvailable: true,
        },
        {
          label: 'Sign up',
          url: 'CORP-sign_up.html',
          notAvailable: true,
        },
        {
          label: 'Sign in',
          url: 'CORP-sign_in.html',
          notAvailable: true,
        },
        {
          label: 'Inner page',
          url: 'CORP-sample-not-banner.html',
          notAvailable: false,
        },
        {
          label: 'Inner page (with banner)',
          url: 'CORP-sample-with-banner.html',
          notAvailable: false,
        },
        {
          label: 'Newsletter subscription',
          url: 'CORP-newsletter_subscribe.html',
          notAvailable: false,
        },
        {
          label: 'Article with table and note',
          url: 'CORP-article_with_table_note.html',
          notAvailable: true,
        },
        {
          label: 'Article with complex table',
          url: 'CORP-article_with_complex_table.html',
          notAvailable: true,
        },
        {
          label: 'Article with anchors',
          url: 'CORP-fast_navigation_anchors.html',
          notAvailable: true,
        },
        {
          label: 'Destinations guide',
          url: 'CORP-destination-guides.html',
          notAvailable: false,
        },
        {
          label: 'Destination guide (city)',
          url: 'CORP-destination_guides_city.html',
          notAvailable: true,
        },
        {
          label: 'News list',
          url: 'CORP-news_list.html',
          notAvailable: false,
        },
        {
          label: 'News detail',
          url: 'CORP-news_detail.html',
          notAvailable: false,
        },
        {
          label: 'News detail table',
          url: 'CORP-news_detail-table.html',
          notAvailable: true,
        },
      ],
    },
    {
      section: 'Landings',
      pages: [{
        label: 'Landing page 01',
        url: 'CORP-LANDING-01.html',
        notAvailable: true,
      }, ],
    },
    {
      section: 'Components (HTML)',
      available: true,
      pages: [{
          label: 'Header (home)',
          url: 'COMP-CORP-header-home.html',
          notAvailable: true,
        },
        {
          label: 'Header (inner pages)',
          url: 'COMP-CORP-header-page.html',
          notAvailable: true,
        },
        {
          label: 'Main banner',
          url: 'COMP-CORP-main_banner.html',
          notAvailable: true,
        },
        {
          label: 'Multiple panel',
          url: 'COMP-CORP-multiple-panel.html',
          notAvailable: false,
        },
        {
          label: 'Destinations offers',
          url: 'COMP-CORP-destinations-offers.html',
          notAvailable: false,
        },
        {
          label: 'Main offers',
          url: 'COMP-CORP-main-offers.html',
          notAvailable: false,
        },
        {
          label: 'Footer',
          url: 'COMP-CORP-main_footer.html',
          notAvailable: true,
        },
        {
          label: 'Sign in',
          url: 'COMP-CORP-sign-in.html',
          notAvailable: true,
        },
        {
          label: 'Breadcrumbs',
          url: 'COMP-CORP-breadcrumbs.html',
          notAvailable: false,
        },
        {
          label: 'Interactive Map',
          url: 'COMP-CORP-interactive-map.html',
          notAvailable: false,
        },
        {
          label: 'Feedback',
          url: 'COMP-CORP-feedback.html',
          notAvailable: false,
        },
      ],
    },
    {
      section: 'UI-Elements (HTML)',
      available: true,
      pages: [{
          label: 'Accordion',
          url: 'COMP-CORP-accordion.html',
          notAvailable: true,
        },
        {
          label: 'Headings',
          url: 'COMP-CORP-headings.html',
        },
        {
          label: 'Paragraphs',
          url: 'COMP-CORP-paragraphs.html',
          notAvailable: true,
        },
        {
          label: 'Tables',
          url: 'COMP-CORP-tables.html',
          notAvailable: true,
        },
        {
          label: 'Lists',
          url: 'COMP-CORP-lists.html',
          notAvailable: false,
        },
        {
          label: 'Images',
          url: 'COMP-CORP-image.html',
          notAvailable: true,
        },
        {
          label: 'Pagination',
          url: 'COMP-CORP-pagination.html',
          notAvailable: true,
        },
      ],
    },
  ],
  sitemapMD: [{
      section: 'Components (HTML)',
      pages: [{
          label: 'Flight search',
          url: 'COMP-flight_search.html',
          notAvailable: true,
        },
        {
          label: 'Booking steps',
          url: 'COMP-booking_steps.html',
          notAvailable: true,
        },
        {
          label: 'Summary',
          url: 'COMP-summary.html',
          notAvailable: true,
        },
        {
          label: 'Footer (ibe)',
          url: 'COMP-footer.html',
          notAvailable: true,
        },
        {
          label: 'Payment details table',
          url: 'COMP-payment-detail-tables.html',
          notAvailable: true,
        },
      ],
    },
    {
      section: 'UI-Elements (HTML)',
      available: true,
      pages: [{
          label: 'Grid',
          url: 'COMP-grid.html',
        },
        {
          label: 'Colors',
          url: 'COMP-colors.html',
          notAvailable: false,
        },
        {
          label: 'Font icons',
          url: '../src/assets/fonts/icons/demo.html',
        },
        {
          label: 'Headings (h1,h2,h3...)',
          url: 'COMP-headings.html',
        },
        {
          label: 'Buttons',
          url: 'COMP-buttons.html',
        },
        {
          label: 'Form elements',
          url: 'COMP-form_elements.html',
        },
        {
          label: 'Modals',
          url: 'COMP-modals.html',
        },
      ],
    },
  ],
  searchFlight: {
    optionsCloseBtn: {
      label: 'Close'
    },
    routes: {
      journeys: [{
          caption: 'Choose your <strong>departure</strong>',
          way: 'departure',
          inputForm: {
            groupId: 'searchOutbound',
            placeholder: 'From',
            label: 'Origin',
            id: 'searchOutbound_input',
            value: 'MAD',
            valueSmall: 'Madrid',
            idCollapse: 'searchOutbound_options',
          },
        },
        {
          caption: 'Choose your <strong>arrival</strong>',
          way: 'arrival',
          inputForm: {
            groupId: 'searchReturn',
            placeholder: 'To',
            label: 'Return',
            id: 'searchReturn_input',
            value: 'NBN',
            valueSmall: 'San Antonio de Palé',
            idCollapse: 'searchReturn_options',
          },
        },
      ],
      multicityJourneys: [{
          way: 'departure',
          inputForm: {
            groupId: 'searchOutbound-mc',
            placeholder: 'Origen',
            label: 'Desde',
            id: 'searchOutbound-mc_input',
            value: 'AGT',
            valueSmall: 'Ciudad del Este',
            idCollapse: 'searchOutbound-mc_options',
          },
        },
        {
          way: 'arrival',
          inputForm: {
            groupId: 'searchReturn-mc',
            placeholder: 'Destino',
            label: 'Hacia',
            id: 'searchReturn-mc_input',
            value: 'AIR',
            valueSmall: 'Buenos Aires',
            idCollapse: 'searchReturn-mc_options',
          },
        },
      ],
    },
    dates: {
      journeys: [{
          way: 'departure',
          caption: 'Choose your <strong>departure</strong> date',
          inputForm: {
            groupId: 'searchDatesOutbound',
            placeholder: 'Outbound',
            label: 'Outbound date',
            id: 'searchDatesOutbound_input',
            valueDate: {
              day: '6',
              month: 'Jul',
              week: 'Wednesday',
            },
            idCollapse: 'searchDatesOutbound_options',
          },
        },
        {
          caption: 'Choose your <strong>arrival</strong> date',
          way: 'arrival',
          inputForm: {
            groupId: 'searchDatesInbound',
            placeholder: 'Return',
            label: 'Return date',
            id: 'searchDatesInbound_input',
            valueDate: {
              day: '16',
              month: 'Jul',
              week: 'Monday',
            },
            idCollapse: 'searchDatesInbound_options',
          },
        },
      ],
    },
    triptype: {
      position: 'default', // `default` or `within-calendar`
      style: 'switch',
      switch: {
        options: [{
            checked: false,
            label: 'One Way',
            id: 'triptype1',
            name: 'triptypeSelection'
          },
          {
            checked: true,
            label: 'Round Trip',
            id: 'triptype2',
            name: 'triptypeSelection'
          },
          {
            checked: false,
            label: 'Multi-city',
            id: 'triptype3',
            name: 'triptypeSelection'
          }
        ]
      }
      // style: 'dropdown',
      // formControl: {
      //   label: '',
      //   id: 'triptypeSelection',
      //   options: [{
      //     name: 'Round trip',
      //     id: '',
      //   }, ]
      // }
    },
    typology: {
      show: true,
      template: 'search-collapsable', // styles: dropdown, search-collapsable
      caption: '<strong>Passenger(s)</strong>',
      label: '',
      value: '1',
      buttonText: '',
      options: [{
          label: 'Adultos',
          labelInfo: '(> 12 años)',
          numericUpdown: {
            id: 'paxAdult',
            value: '2',
          },
        },
        {
          label: 'Niños',
          labelInfo: '(2-11 años)',
          numericUpdown: {
            id: 'paxChildren',
            value: '0',
            minus: 'disabled',
          },
        },
        {
          label: 'Infantes',
          labelInfo: '(<2 años)',
          numericUpdown: {
            id: 'paxInfants',
            value: '0',
            minus: 'disabled',
          },
          tooltip: '',
        },
      ],
    },
    currency: {
      show: true,
      formControl: {
        hasLabel: true,
        label: 'Currency:',
        id: 'currencyId',
        options: [{
          name: 'PYG',
          id: ''
        }, ]
      }
    },
    promocode: {
      show: true,
      linkText: 'I have a <span>promocode</span>',
      formControl: {
        hasLabel: true,
        label: 'Promocode',
        placeholder: '',
        name: 'promocode',
        id: 'promocode',
        button: {
          text: 'Apply'
        }
      }
    },
    searchButton: {
      url: 'BF-select_flight.html',
      text: 'Buscar'
    }
  },
  multiplePanel: {
    responsive: {
      collapsable: false
    },
    navItems: [{
        ariaControl: 'multiplePanelTab1_tab',
        id: 'multiplePanelTab1',
        text: 'Book flight',
        panel: {
          show: true,
          button: {
            icon: '',
            text: 'Book flight',
          },
          contentInclude: 'partials/components/IBE/flight-search/_flight-search.njk',
        },
      },
      {
        ariaControl: 'multiplePanelTab2_tab',
        id: 'multiplePanelTab2',
        tabindex: -1,
        text: 'Check in',
        url: '',
        panel: {
          show: false,
          button: {
            icon: '',
            text: 'Check in',
          },
          contentInclude: 'partials/components/IBE/_booking-retrieve.njk',
        },
      },
      {
        ariaControl: 'multiplePanelTab3_tab',
        id: 'multiplePanelTab3',
        tabindex: -1,
        text: 'Manage booking',
        url: '',
        panel: {
          show: false,
          button: {
            icon: '',
            text: 'Manage booking',
          },
          contentInclude: 'partials/components/IBE/_booking-retrieve.njk',
        },
      },
    ],
  },
  mainHeader: {
    hasSubnavCloseButton: true, // add close button to primary sub elements
    // callCenter: {
    //   label: 'Call Center',
    //   content: {
    //     header: {
    //       title: 'Call Center',
    //     },
    //     body: {
    //       html: 'If you need assistance, call the Jambojet Call Centre on <span class="call-center_content_body_phone"><span class="call-center_content_body_phone_number">+254 711 024 545</span> or <span class="call-center_content_body_phone_number">+254 734 104 545.</span></span>',
    //     }
    //   }
    // },
    navPrimary: [{
        text: 'PLAN YOUR TRIP',
        icon: 'airplane_up',
        hasSubnavTitle: true, //## submenu title for responsive
        submenus: [
          {
            items: [
            {
              title: 'Destinations',
              url: '#',
              submenuList: [
                {
                  text: 'Diani (Ukunda)',
                  url: '#',
                },
                {
                  text: 'Eldoret ',
                  url: '#',
                },
                {
                  text: 'Entebbe ',
                  url: '#',
                },
                {
                  text: 'Kisumu',
                  url: '#',
                },
                {
                  text: 'Malindi',
                  url: '#',
                },
                {
                  text: 'Mombasa ',
                  url: '#',
                },
                {
                  text: 'Nairobi',
                  url: '#',
                },
              ]
            },
          ],
        },
        {
          items: [
            {
              title: 'Group Bookings',
              url: '#',
              submenuList: [
                {
                  text: 'How to Book',
                  url: '#',
                },
                {
                  text: 'Booking Policy ',
                  url: '#',
                },
              ]
            },
          ],
        },
        {
          items: [
            {
              title: 'Travel Requirements',
              url: '#',
              submenuList: [
                {
                  text: 'Local Travel',
                  url: '#',
                },
                {
                  text: 'Internation Travel',
                  url: '#',
                },
              ]
            },
          ]
        },
        {
          items: [
            {
              title: 'Travel Deals',
              url: '#',
            },
            {
              title: 'Flight Schedules',
              url: '#',
            },
            {
              title: 'How to Book',
              url: '#',
            },
            {
              title: 'Fare Categories',
              url: '#',
            },
            {
              title: 'Payment Methods',
              url: '#',
            },
          ],
        },
      ],
      },
      {
        text: 'TRAVEL INFORMATION',
        icon: 'airplane-right',
        hasSubnavTitle: true,
        submenus: [
          {
          items: [
            {
              title: 'Baggage',
              url: '#',
              submenuList: [
                {
                  text: 'Baggage Policy',
                  url: '#',
                },
                {
                  text: 'Baggage Allowance',
                  url: '#',
                },
                {
                  text: 'Checked Baggage',
                  url: '#',
                },
                {
                  text: 'Sporting Equipment',
                  url: '#',
                },
                {
                  text: 'Pet/Animal',
                  url: '#',
                },
                {
                  text: 'Carriage',
                  url: '#',
                },
              ]
            },
          ]
        },
        {
          items: [
            {
              title: 'Airport Information',
              url: '#',
              submenuList: [
                {
                  text: 'Diani (Ukunda)',
                  url: '#',
                },
                {
                  text: 'Eldoret',
                  url: '#',
                },
                {
                  text: 'Entebbe',
                  url: '#',
                },
                {
                  text: 'Kisumu',
                  url: '#',
                },
                {
                  text: 'Malindi',
                  url: '#',
                },
                {
                  text: 'Mombasa',
                  url: '#',
                },
                {
                  text: 'Nairobi',
                  url: '#',
                },
              ]
            },
          ]
        },
        {
          items: [
            {
              title: 'Special Assistance',
              url: '#',
              submenuList: [
                {
                  text: 'Expectanct Mothers',
                  url: '#',
                },
                {
                  text: 'Physically Challenged',
                  url: '#',
                },
                {
                  text: 'Blind, Deaf or Mentally Challenged',
                  url: '#',
                },
                {
                  text: 'Unaccompanied Minor',
                  url: '#',
                },
                {
                  text: 'Medical Form',
                  url: '#',
                },
              ]
            },
          ]
        },
        {
          items: [
            {
              title: 'Checking in',
              url: '#',
            },
            {
              title: 'Fees',
              url: '#',
            },
            {
              title: 'Seating',
              url: '#',
            },
          ],
        }, ],
      },
      {
        text: 'EXPERIENCE',
        icon: 'airplane-left',
        hasSubnavTitle: true,
        submenus: [{
          items: [{
              title: 'Offices',
              url: '#',
              submenuList: [
                {
                  text: 'Diani (Ukunda)',
                  url: '#',
                },
                {
                  text: 'Eldoret',
                  url: '#',
                },
                {
                  text: 'Entebbe',
                  url: '#',
                },
                {
                  text: 'Kisumu',
                  url: '#',
                },
                {
                  text: 'Malindi',
                  url: '#',
                },
                {
                  text: 'Mombasa ',
                  url: '#',
                },
                {
                  text: 'Nairobi',
                  url: '#',
                },
              ]
            },
          ]
        },
        {
          items: [
            {
              title: 'Our Fleet',
              url: '#',
            },
            {
              title: 'On time Performance',
              url: '#',
            },
          ]
        },
        {
          items:
          [
            {
              title: 'Duty Free Magazine',
              url: '#',
            },
          ]
        },
        {
          items: [
            {
              title: 'Gift Vouchers ',
              url: '#',
            },
          ],
        }, ],
      },
    ],
    navSecondary: {
      links: [
        {
          dataType: 'call-center',
          component: {
            label: 'Call Center',
            content: {
              header: {
                title: 'Call Center'
              },
              body: {
                html: 'If you need assistance, call the Jambojet Call Centre on <span class="call-center_content_body_phone"><span class="call-center_content_body_phone_number">+254 711 024 545</span> or <span class="call-center_content_body_phone_number">+254 734 104 545.</span></span>',
              }
            }
          }
        },
        {
          dataType: 'websearch',
        },
        {
          dataType: 'languages',
          label: '',
          linkIcon: 'language',
          url: '#',
          dropdownList: [{
              title: 'English (EN)',
              url: '#',
            },
            {
              title: 'Español (ES)',
              url: '#',
            },
            {
              title: 'Francais (FR)',
              url: '#',
            },
          ],
        },
        {
          dataType: 'authentication',
          style: 'link', // 'link' | 'popover'
          label: '',
          url: '#',
          dropdownList: [{
              title: 'Logout',
              url: '#',
            },
          ],
        },
      ],
    },
  },
  newsList: {
    hasShowMoreLink: {
      caption: 'Seguir leyendo',
      pdfCaption: '[pdf]',
    },
  },
  mainFooter: {
    logo: {
      show: true,
      isNegative: true,
    },
    flyPrograms: {
      show: false,
    },
    newsletter: {
      show: false,
    },
    bottom: {
      show: true,
      copyright: {
        show: true,
      },
      languageBar: {
        show: false,
      },
    },
    socialmedia: {
      show: true,
    },
    nav: {
      show: true,
      isCollapsible: true,
      items: [{
          title: 'ABOUT JAMBOJET',
          subItems: [
            {
              caption: 'Our Story',
            },
            {
              caption: 'Press Room ',
            },
            {
              caption: 'Advertise with Jambojet',
            },
            {
              caption: 'Business Partner ',
            },
            {
              caption: 'Careers',
            },
            {
              caption: 'Off Chocks ',
            },
            {
              caption: 'Terms & Conditions ',
            },
            {
              caption: 'Privacy Policy ',
            },
          ],
        },
        {
          title: 'SelfCare',
          subItems: [
            {
              caption: 'Manage Booking',
            },
            {
              caption: 'Online check-in',
            },
            {
              caption: 'Travel deals',
            },
            {
              caption: 'Agent Log in',
            },
          ],
        },
        {
          title: 'Support',
          subItems: [
            {
              caption: 'FAQ',
            },
            {
              caption: 'Offices',
            },
            {
              caption: 'Contact Us',
            },
          ],
        },
      ],
    },
    paymentMethodList: {
      show: true,
      title: {
        text: 'Payment methods',
      },
      paymentMethods: {
        items: [
          {
            name: 'master card',
            img: 'mastercard.svg',
            url: '#',
          },
          {
            name: 'visa',
            img: 'visa.svg',
            url: '#',
          },
          {
            name: 'JCB',
            img: 'JCB.svg',
          },
          {
            name: 'american express',
            img: 'american-express.svg',
            url: '#',
          },
          {
            name: 'master card',
            img: 'mastercard.svg',
            url: '#',
          },
        ]
      }
    }
  },
  socialmedias: {
    title: 'Connect with us',
    hasFlatColors: true,
    items: [{
        name: 'facebook',
        url: 'https://www.facebook.com/',
      },
      {
        name: 'twitter',
        url: 'https://twitter.com/',
      },
      {
        name: 'youtube',
        url: 'https://youtube.com/',
      },
      {
        name: 'instagram',
        url: 'https://instagram.com/',
      },
    ],
  },
  languageBar: {
    label: 'Language:',
    selectedText: 'Español (ES)',
    items: [{
        title: 'Español (ES)',
        url: '#',
      },
      {
        title: 'English (En)',
        url: '#',
      },
      {
        title: 'Francais (FR)',
        url: '#',
      },
    ],
  },
  copyright: {
    text: '&copy 2018 Jambojet Limited. All rights reserved. Usage of this website states your compliance of our <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a>.',
  },
  poweredby: {
    label: 'Powered by',
    text: '',
    image: 'logo-newshore.svg',
    linkUrl: 'http://newshore.es',
  },
  mainOffers: {
    gridCols: 'grid-col col-12 col-xs-4',
    offers: [{
        isOfferDeal: true,
        icon: 'cart',
        title: 'Equipaje',
        caption: 'Compruebe la información sobre el equipaje que puede llevar con usted',
        button: 'Ver más',
        buttonUrl: '',
      },
      {
        isOfferDeal: true,
        icon: 'calendar',
        title: 'Prensa',
        caption: 'Consulte las noticias más recientes de nuestra página',
        button: 'Ver más',
        buttonUrl: '',
      },
      {
        isOfferDeal: true,
        icon: 'airplane-right',
        title: 'Flota',
        caption: 'Conozca las características de nuestros aviones CRJ200',
        button: 'Reserva',
        buttonUrl: '',
      },
    ],
  },
  destinationOffersPrice: {
    template: 'items', // items or table
    title: 'Offers from',
    selector: 'Malabo',
    selectorOptions: [{
        name: 'Dakar',
      },
      {
        name: 'Accra',
      },
      {
        name: 'Mengomeyén',
      },
      {
        name: 'Mengomeyén',
      },
      {
        name: 'Pointe-Noir',
      },
    ],
    destinationOptionTitle: 'Destinations offers',
    destinationOptions: {
      items: [
        {
          labelFrom: 'Desde',
          title: 'Eldoret Two lines',
          text: '',
          price: '4,500',
          link: {
            fullarea: true,
            url: '#',
            title: '',
            label: '',
          }
        },
        {
          labelFrom: 'Desde',
          title: 'Ukunda',
          text: '',
          price: '3,800',
          link: {
            fullarea: true,
            url: '#',
            title: '',
            label: '',
          }
        },
        {
          labelFrom: 'Desde',
          title: 'Kisumu',
          text: '',
          price: '4,200',
          link: {
            fullarea: true,
            url: '#',
            title: '',
            label: ''
          }
        },
        {
          labelFrom: 'Desde',
          title: 'Entebbe',
          text: '',
          price: '4,800',
          link: {
            fullarea: true,
            url: '#',
            title: '',
            label: ''
          }
        }
      ],
    },
  },
  destinationOffers: {
    hasSelectOutPriceList: true,
    more: {
      text: 'More destinations',
      url: '#'
    },
    main: false,
    aside: {
      items: [{
        isMain: true,
        city: 'Malindi',
        from: true, // true to add default SHARED.from
        price: '3,600',
        img: 'destination-img2',
        url: '#',
        // offerLink: {
        //   caption: 'Explore',
        //   url: '#',
        // },
      }, ],
    },
  },
  mapRoutesHome: {
    title: 'Mapa de Rutas',
    introdution: 'En Newshore te ofrecen una gran variedad de destinos y el mejor servicio en aeropuerto y a bordo. Conoce a dónde volamos',
    img: 'maproutes.png',
    button: {
      text: 'info',
      url: 'javascript:void(0)',
    },
  },
  destinations: {
    itemsSortBy: destinations.sortBy,
    items: getDestinations(),
  },
  newsletter: {
    title: 'Get the latest sales straight to your email',
    intro: 'Do you want to be up to date? Subscribe here to receive updates on our airline, promos and customized marketing communication.',
    buttonText: 'Subscribe',
    canBeClosed: false,
    closeLabel: '',
    form: {
      hasLabel: true,
      label: 'e-mail',
      placeholder: false,
      name: 'newsletterInput',
      id: 'newsletterInput',
    },
    note: {
      text: "By signing up, you agree to Jambojet's <a href='#'>Privacy Policy</a> and <a href='#'>Terms of Use </a>"
    }
  },
}
module.exports = {
  projectName: 'HKE',
  site_title: 'HK IBE [HTML]',
  demo_imgs_path: '../src/assets/imgs/',
  core_assets_path: '../../../core/src/assets/',
  project_assets_path: '../src/assets/',
  logo_img: 'logo.svg',
  logo_negative_img: 'logo-neg.svg',
  pathFont: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700&amp;subset=latin-ext',
  currencyPosition: 'left',
  SHARED: {
    currency: 'HKD',
    weightmeasure: 'Kg',
    promoLabelDefault: 'Promo',
    formRequiredSymbol: '*',
    formRequiredText: '* Indicates a mandatory field',
    from: 'From',
    nextFlight: 'Next flight',
    flightNotavailable: 'Flight not unavailable<br />Departure and return time do not match',
  },
  showUserAccount: true,
  header: {
    headerRight: {
      show: true,
      layout: {
        show: true,
        agency: {
          title: 'agency',
          items: [{
            modifier: 'link-agent',
            title: 'AgentName',
            dropdownPopover: {
              ddPopover: {
                id: 'collapseUserAgentAccount',
                isAgentLogged: true,
                isLogged: true,
              },
              ddPopoverList: [{
                  text: 'Ufly portal',
                  url: 'AC_home.html',
                  icon: 'home',
                },
                {
                  text: 'Profile Update',
                  url: 'AC_account_management.html',
                  icon: 'edit',
                },
              ],
            },
          }, ],
        },
        uFly: {
          title: 'u-fly',
          items: [{
            modifier: 'link-agent',
            title: 'Richard Roe',
            dropdownPopover: {
              ddPopover: {
                id: 'collapseUserAgentAccount',
                isAgentLogged: false,
                isLogged: false,
                hasHeader: true,
                hasLogout: true,
              },
              ddPopoverList: [{
                  text: 'UFly Portal',
                  url: 'AC_home.html',
                  icon: 'home',
                },
                {
                  text: 'Profile Update',
                  url: 'AC_account_management.html',
                  icon: 'edit',
                },
              ],
            },
          }, ],
        },
      },
      items: [{
          dataType: 'user',
          dropdownList: {
            label: 'John Smith',
            items: [{
                title: 'Item 1',
                url: '',
              },
              {
                title: 'Item 2',
                url: '',
              },
              {
                title: 'Item 3',
                url: '',
              },
            ],
          },
        },
        {
          dataType: 'languages',
        },
        {
          dataType: 'currency',
        },
      ],
    },
    nav: {
      itemsAlign: 'left',
      items: [],
    },
  },
  headerHasNavRight: false,
  bookingSteps: [{
      stepNumber: '1',
      stepLabel: 'Select flight',
      stepUrl: '',
    },
    {
      stepNumber: '2',
      stepLabel: 'Guest details',
      stepUrl: '',
    },
    {
      stepNumber: '3',
      stepLabel: 'Add extras',
      stepUrl: '',
    },
    {
      stepNumber: '4',
      stepLabel: 'Seat selection',
      stepUrl: '',
    },
    {
      stepNumber: '5',
      stepLabel: 'Payment confirmation',
      stepUrl: '',
    },
  ],
  multipleCalendarOptions: true,
  months: [{
      month: 'December',
      year: '2017',
    },
    {
      month: 'January',
      year: '2018',
      selected: true,
    },
    {
      month: 'February',
      year: '2018',
    },
    {
      month: 'March',
      year: '2018',
    },
    {
      month: 'April',
      year: '2018',
    },
    {
      month: 'May',
      year: '2018',
    },
    {
      month: 'June',
      year: '2018',
    },
    {
      month: 'July',
      year: '2018',
    },
    {
      month: 'August',
      year: '2018',
    },
    {
      month: 'September',
      year: '2018',
    },
  ],
  days: [{
      day: '24 DEC',
      week: 'Sunday',
      price: '477',
    },
    {
      day: '25 DEC',
      week: 'Monday',
      price: '11,490',
      selected: true,
    },
    {
      day: '26 DEC',
      week: 'Tuesday',
      price: '326',
    },
    {
      day: '27 DEC',
      week: 'Wednesday',
      price: '11,490',
    },
    {
      day: '28 DEC',
      week: 'Thursday',
      price: '14,490',
      bestprice: true,
    },
    {
      day: '29 DEC',
      week: 'Friday',
      price: '477',
    },
    {
      day: '30 DEC',
      week: 'Saturday',
      price: '477',
    },
    {
      day: '31 DEC',
      week: 'Sunday',
      price: '477',
    },
    {
      day: '01 Jan',
      week: 'Monday',
      price: '477',
    },
    {
      day: '02 Jan',
      week: 'Tuesday',
      price: '477',
    },
    {
      day: '03 Jan',
      week: 'Wednesday',
      price: '477',
    },
  ],
  journeys: [{
      'class': 'outbound',
      way: 'Departure',
      headerConnector: '-',
      headerImgSrc: '../../App_Plugins/IBE/assets/img/selectflight/banner-departure.jpg',
      departureAirport: 'Hong Kong',
      arrivalAirport: 'Kunming',
      departureAirportCode: 'HKG',
      arrivalAirportCode: 'KMD',
      date: 'Sat, 16 Dec 2017',
      stop: {},
      tecStop: {},
      flightNumber: [
        'UO 1252',
        'UO 1278',
      ],
      fare: 1,
      fareSelectedName: 'Fun',
    },
    {
      'class': 'inbound',
      way: 'Return',
      headerConnector: '-',
      headerImgSrc: '../../App_Plugins/IBE/assets/img/selectflight/banner-return.jpg',
      departureAirport: 'Kunming',
      arrivalAirport: 'Hong Kong',
      departureAirportCode: 'KMD',
      arrivalAirportCode: 'HKG',
      date: 'Sat, 16 Dec 2017',
      stop: {},
      tecStop: {},
      flightNumber: [
        'UO 1252',
        'UO 1278',
      ],
      fare: 2,
      fareSelectedName: 'Flexible',
    },
  ],
  flightCodeTimeSeparator: '|',
  flightCaption: 'Flight',
  flightLabelOutbound: '',
  flightLabelInbound: '',
  flightExtraDaysPosition: 'right',
  flightSelectedFareButtonSelectable: true,
  flightScale: {
    title: 'Flight details',
    subtitle: 'Saturday 16 December 2017',
    timeTotalText: 'Total travel time:',
    timeTotal: '14h 00m',
    departureTime: '7:40',
    departureAirport: 'kunming',
    departureAirportCode: 'KMG',
    departureDuration: '2h 20m',
    flightNumberDeparture: 'Flight EB-5002',
    scales: [{
        title: 'Hong Kong',
        time: '12:00',
        departureAirportCode: 'HKG',
        transit: {
          time: '10h 10m',
          title: 'Transit in Hong Kong',
        },
      },
      {
        title: 'Hong Kong',
        time: '22:20',
        departureAirportCode: 'HKG',
        isFinalizeFlight: true,
      },
    ],
    arrivalTime: '12:00',
    arrivalAirport: 'Kagoshima',
    arrivalAirportCode: 'KOJ',
    arrivalDuration: '1h 30m',
    flightNumberArrival: 'Flight UO-1850',
  },
  flights: {
    items: [{
        departureTime: '10:30',
        departureAirport: 'Hong Kong',
        departureAirportCode: 'HKG',
        duration: '2:10h',
        stop: {
          style: 'modal',
          text: '1 stop',
          id: 'modal-flightConnections',
        },
        tecStop: {},
        arrivalTime: '14:00',
        arrivalAirport: 'Kunming',
        arrivalAirportCode: 'KMD',
        priceFrom: '325',
        connection: false,
        fareSelected: false,
        fareSelectedName: 'Fun',
        fareType: 'fare1',
        hasPromo: false,
        unavailable: true,
      },
      {
        departureTime: '07:40',
        departureAirport: 'Hong Kong',
        departureAirportCode: 'HKG',
        duration: '2:30h',
        stop: {
          style: 'modal',
          text: '2 stop',
          id: 'modal-flightConnections',
        },
        tecStop: {},
        arrivalTime: '10:10',
        arrivalAirport: 'Kunming',
        arrivalAirportCode: 'KMD',
        priceFrom: '1245',
        connection: true,
        fareSelected: true,
        fareSelectedName: 'Fun+',
        fareType: 'fare2',
        hasPromo: false,
      },
      {
        departureTime: '14:30',
        departureAirport: 'Hong Kong',
        departureAirportCode: 'HKG',
        duration: '2:00h',
        stop: {},
        tecStop: {},
        arrivalTime: '16:00',
        arrivalAirport: 'Kunming',
        arrivalAirportCode: 'KMD',
        priceFrom: '325',
        connection: false,
        fareSelected: false,
        fareSelectedName: 'U-biz',
        fareType: 'fare3',
        hasPromo: true,
      },
    ],
  },
  paxSelector: {
    title: '',
    label: 'Seat',
    nextButton: 'Next flight',
  },
  passengers: [{
      label: 'Guest 1:',
      gender: 'Mr.',
      firstName: 'John',
      lastName: 'Smith Danes Wilson',
      type: 'Adult',
      seat: '8F',
      seatStatus: '',
      checkedIn: true,
    },
    {
      label: 'Guest 2:',
      gender: 'Mrs.',
      firstName: 'Jane',
      lastName: 'Doe',
      type: 'Adult',
      seat: '',
      seatStatus: 'selected',
      checkedIn: false,
      hasInfoButton: true,
    },
    {
      label: 'Passenger 3:',
      gender: 'Mr.',
      firstName: 'Richard',
      lastName: 'Doe',
      type: 'Infant',
      seat: '3G',
      seatStatus: '',
      checkedIn: false,
      email: 'richard.roe@hkexpress.com',
      signUpReference: 'W4PZ7C',
      departure: '4540002144',
    },
  ],
  seatmap: {
    seatsHasPopover: true,
    letters: [{
        letter: 'A',
      },
      {
        letter: 'B',
      },
      {
        letter: 'C',
      },
      {
        letter: '',
      },
      {
        letter: 'D',
      },
      {
        letter: 'E',
      },
      {
        letter: 'F',
      },
    ],
    seats: [{
        seats_group: [{
            status: 'upfront',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
            dataTrigger: 'click',
          },
          {
            status: 'upfront',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'upfront',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '1',
          },
          {
            status: 'upfront',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'unavailable',
            text: 'Not available seat',
          },
          {
            status: 'unavailable',
            text: 'Not available seat',
          },
        ],
      },
      {
        seats_group: [{
            status: 'upfront',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'upfront',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'upfront',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '2',
          },
          {
            status: 'upfront',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'upfront',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'upfront',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: 'upfront',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'upfront',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'upfront',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '3',
          },
          {
            status: 'upfront',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'upfront',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'upfront',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '4',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '5',
          },
          {
            status: 'unavailable',
            text: 'Not available seat',
          },
          {
            status: 'unavailable',
            text: 'Not available seat',
          },
          {
            status: 'unavailable',
            text: 'Not available seat',
          },
        ],
      },
      {
        seats_group: [{
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '6',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '7',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: 'selected',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'current',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '8',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        hasExitLeft: true,
        hasExitRight: true,
        seats_group: [{
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '9',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: 'xlarge',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'xlarge selected',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'xlarge',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '10',
          },
          {
            status: 'xlarge',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'xlarge',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'xlarge',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: 'xlarge',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'xlarge',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'xlarge unavailable',
            text: 'Not available seat',
          },
          {
            status: '',
            row_number: true,
            text: '11',
          },
          {
            status: 'xlarge',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'xlarge',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: 'xlarge',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '12',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '13',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '14',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '15',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '16',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '17',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '18',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
      {
        seats_group: [{
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            row_number: true,
            text: '19',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: '2A Press enter to select this seat Standard Seat HKD 35',
          },
          {
            status: '',
            text: ' 2A Press enter to select this seat Standard Seat HKD 35',
          },
        ],
      },
    ],
  },
  seatsLegend: {
    hasResponsiveModal: true,
    desc: {
      title: '',
    },
    footnote: '*If you do not select a seat option above, one will be assigned for you at no cost.<br />*Some aircraft may have a different seating configuration. If your selected seat is not available on your flight, we will offer you an alternative seat in the same category selected, or refund you the amount paid.',
    items: [{
        type: '',
        text: 'Standard Seat',
        price: 'HKD 35',
        description: 'For safety reasons, the seatbacks in row 11 do not recline.',
      },
      {
        type: 'upfront',
        text: 'Upfront Seat',
        price: 'HKD 75',
        description: '',
      },
      {
        type: 'xlarge',
        text: 'Sweet Seat',
        price: 'HKD 160 <span class=\'rp\'>(25% off airport rate)</span>',
        description: 'Extra legroom. For safety reasons, the seatbacks in row 12 do not recline.',
      },
      {
        type: 'unavailable',
        text: 'Unavailable Seat',
        price: '',
        description: '',
      },
      {
        type: 'selected',
        text: 'Selected Seat',
        price: '',
        description: '',
      },
    ],
  },
  fares: {
    faresLayoutTabs: true,
    items: [{
        pos: '1',
        name: 'Fun',
        isPromo: true,
        price: '325',
        unavailable: false,
        fareButtonText: 'Select',
        farePrice: '',
        fareDescription: [{
            icon: 'boarding-pass',
            description: 'Fare difference and change fee applies',
          },
          {
            icon: 'baggage-v',
            description: 'Carry-on (0kg) / Check-in (25kg)',
          },
          {
            icon: 'reward-u',
            description: '10 reward-U points for HKD 1 spent',
          },
        ],
      },
      {
        pos: '2',
        name: 'Fun +',
        isPromo: false,
        price: '758',
        unavailable: false,
        fareButtonText: 'Select',
        farePrice: '',
        fareDescription: [{
            icon: 'boarding-pass',
            description: 'Fare difference and change fee applies',
          },
          {
            icon: 'baggage-v',
            description: 'Carry-on (7kg) / Check-in (25kg)',
          },
          {
            icon: 'reward-u',
            description: '10 reward-U points for HKD 1 spent',
          },
          {
            icon: 'seat-airplane',
            description: 'Up-front Seat',
          },
        ],
      },
      {
        pos: '3',
        name: 'U-biz',
        isPromo: false,
        price: '999,437',
        unavailable: false,
        fareButtonText: 'Select',
        farePrice: '',
        fareDescription: [{
            icon: 'boarding-pass',
            description: 'Changes up to 3 hours before departure',
          },
          {
            icon: 'baggage-v',
            description: 'Carry-on (7kg) / Check-in (30kg)',
            note: '* Please note size and weight limitations: <a href=\'#\' target=\'_blank\'>Checked baggage</a> | <a href=\'#\' target=\'_blank\'>Carry-on baggage</a>',
          },
          {
            icon: 'reward-u',
            description: '20 reward-U points for HKD 1 spent',
          },
          {
            icon: 'seat-airplane',
            description: 'Sweet Seat',
          },
          {
            icon: 'passenger-first',
            description: 'U-First Priority Check in and Pre-boarding',
          },
          {
            icon: 'refunds',
            description: 'Refunds*',
            note: '* <a href=\'#\' target=\'_blank\'>Terms and conditions</a> apply',
          },
          {
            icon: 'couch',
            description: 'Premium Lounge at Hong Kong Airport',
          },
          {
            icon: 'baggage-conveyor',
            description: 'Priority Baggage Retrieval',
          },
        ],
      },
    ],
  },
  baggages: [{
      title: 'Departing Baggage',
      type: [{
          price: '229',
          size: '20',
          promo: true,
        },
        {
          price: '360',
          size: '25',
          promo: true,
        },
        {
          price: '365',
          size: '30',
          promo: true,
        },
      ],
    },
    {
      title: 'Returning Baggagee',
      type: [{
        price: '365',
        size: '30',
        promo: false,
      }, ],
    },
  ],
  baggagesOW: [{
    title: '',
    type: [{
        price: '229',
        size: '20',
        promo: true,
      },
      {
        price: '360',
        size: '25',
        promo: true,
      },
      {
        price: '365',
        size: '30',
        promo: true,
      },
    ],
  }, ],
  specialBaggages: [{
      title: 'Departing Oversized Baggage',
      type: [{
          price: '229',
          size: '20',
          promo: true,
        },
        {
          price: '360',
          size: '25',
          promo: true,
        },
        {
          price: '365',
          size: '30',
          promo: true,
        },
      ],
    },
    {
      title: 'Returning Oversized Baggage',
      type: [{
        price: '365',
        size: '30',
        promo: false,
      }, ],
    },
  ],
  specialBaggagesOW: [{
    type: [{
        price: '229',
        size: '20',
        promo: true,
      },
      {
        price: '360',
        size: '25',
        promo: true,
      },
      {
        price: '365',
        size: '30',
        promo: true,
      },
    ],
  }, ],
  meals: [{
      name: 'Thai Red Curry Chicken with Rice Thai Red Curry Chicken with Rice',
      image: 'meal_01.png',
      price: '70',
    },
    {
      name: 'Japanese Beef Bento',
      image: 'meal_02.png',
      price: '105',
    },
    {
      name: 'Dim Sum Set',
      image: 'meal_03.png',
      price: '70',
      active: true,
    },
    {
      name: 'Evian Natural Mineral Water 330ml',
      image: '',
      price: '15',
    },
  ],
  searchResultConnector: 'to',
  searchResultHasTypology: true,
  editSearchButtonText: 'Change search',
  summaryData: [{
    isCollapse: true,
    hasPriceResume: true,
    fareByFlight: false,
    showBottomTotal: false,
    showBottomTotalLabel: '',
    moreDetailsCollapsible: true,
    moreDetailsStyle: 'byjourney',
    summaryCollapseTitle: '',
    totalCostValue: '2,544,00',
    totalCostLabel: 'Total cost',
    legalTerms: '* 유류할증료와 세금을 포함한 총 운임으로 구매 시점과 환율에 따라 변동될 수 있습니다. * *위탁 수하물, 기내식, 유아 동반, 좌석 업그레이드 등 항공권 구매 이외 모든 보조 상품은 구매 후 취소 및 환불이 불가하오니 이점 유의해주시기 바랍니다.',
  }, ],
  summaryOutboundDetails: [{
      blockHeader: 'Adult',
      blockRows: [{
          label: 'Fare',
          price: 'HKD 328',
        },
        {
          label: 'CN Security charge',
          price: 'HKD 255',
        },
        {
          label: 'CN terminal facilities charge',
          price: 'HKD 255',
        },
      ],
    },
    {
      blockHeader: 'Child',
      blockRows: [{
          label: 'Fare',
          price: 'HKD 328',
        },
        {
          label: 'CN Security charge',
          price: 'HKD 255',
        },
        {
          label: 'CN terminal facilities charge',
          price: 'HKD 255',
        },
      ],
    },
  ],
  summaryInboundDetails: [{
      blockHeader: 'Adult',
      blockRows: [{
          label: 'Fare',
          price: 'HKD 328',
        },
        {
          label: 'CN Security charge',
          price: 'HKD 255',
        },
        {
          label: 'CN terminal facilities charge',
          price: 'HKD 255',
        },
      ],
    },
    {
      blockHeader: 'Child',
      blockRows: [{
          label: 'Fare',
          price: 'HKD 328',
        },
        {
          label: 'CN Security charge',
          price: 'HKD 255',
        },
        {
          label: 'CN terminal facilities charge',
          price: 'HKD 255',
        },
      ],
    },
  ],
  segmentSelectorIsTabs: true,
  segmentSelector: {
    hasButton: false,
    alertText: '',
    flightLabel: 'Flight',
    flights: [{
        from: 'KMG',
        to: 'HKG',
        fromCity: 'Kumming',
        toCity: 'Hong Kong',
        date: '',
        flightNumber: 'UO 1252',
        disabled: true,
        active: false,
      },
      {
        from: 'HKG',
        to: 'KMG',
        fromCity: 'Hong Kong',
        toCity: 'Kumming',
        date: '',
        flightNumber: 'UO 1254',
        disabled: false,
        active: true,
      },
      {
        from: 'HKG',
        to: 'KMG',
        fromCity: 'Hong Kong',
        toCity: 'Kumming',
        date: '',
        flightNumber: 'UO 1252',
        disabled: false,
        active: false,
      },
    ],
  },
  paymentData: {
    methods: {
      items: [{
          isCurrent: true,
          name: 'Credit Card',
          hasCurrencyOptions: true,
          title: 'Credit / Debit card',
          titleTag: 'h4',
          id: 'creditcardCollapse',
          image: 'creditcard.svg',
          imageSelected: 'creditcard-selected.svg',
          template: '_cc',
        },
        {
          name: 'Alipay',
          hasCurrencyOptions: false,
          title: 'Alipay',
          titleTag: 'h4',
          id: 'alipayCollapse',
          image: 'alipay.svg',
          imageSelected: 'alipay-selected.svg',
          template: '_thirdparty',
        },
        {
          name: 'Union Pay',
          hasCurrencyOptions: false,
          title: 'Union Pay',
          titleTag: 'h4',
          id: 'unionpayCollapse',
          image: 'unionpay.svg',
          imageSelected: 'unionpay-selected.svg',
          template: '_thirdparty',
        },
        {
          name: 'WeChat Pay',
          hasCurrencyOptions: false,
          title: 'WeChat Pay',
          titleTag: 'h4',
          id: 'wechatCollapse',
          image: 'wechat.svg',
          imageSelected: 'wechat-selected.svg',
          template: '_thirdparty',
        },
      ],
      itemsAgent: [{
          isCurrent: true,
          name: 'Credit Card',
          isAgent: true,
          hasCurrencyOptions: true,
          title: 'Credit / Debit card',
          titleTag: 'h4',
          id: 'creditcardCollapse',
          image: 'creditcard.svg',
          imageSelected: 'creditcard-selected.svg',
          template: '_cc',
        },
        {
          name: 'Alipay',
          hasCurrencyOptions: false,
          title: 'Alipay',
          titleTag: 'h4',
          id: 'alipayCollapse',
          image: 'alipay.svg',
          imageSelected: 'alipay-selected.svg',
          template: '_thirdparty',
        },
        {
          name: 'Agent account',
          isAgent: true,
          hasCurrencyOptions: false,
          title: 'Agent account',
          titleTag: 'h4',
          id: 'agentAccuntCollapse',
          image: 'agent-account.svg',
          imageSelected: 'agent-account-selected.svg',
          template: '_thirdparty',
        },
        {
          name: 'On Hold',
          isAgent: true,
          hasCurrencyOptions: false,
          title: 'On hold',
          titleTag: 'h4',
          id: 'olnHoldCollapse',
          image: 'on-hold.svg',
          imageSelected: 'on-hold-selected.svg',
          template: '_thirdparty',
        },
      ],
    },
    paymentSummary: {
      title: 'Payment summary',
      body: {
        items: [{
            title: 'Total Price',
            price: '1,238',
          },
          {
            title: 'Total Paid',
            price: '0',
          },
          {
            title: 'Reservation',
            price: '48',
          },
        ],
      },
      footer: {
        items: [{
            title: 'Total Amount Due',
          },
          {
            price: '1,286',
          },
        ],
      }
    },
    methodsAgent: [{
        isCurrent: true,
        name: 'Credit Card',
        hasCurrencyOptions: true,
        title: 'Credit / Debit card',
        titleTag: 'h4',
        id: 'creditcardCollapse',
        image: 'creditcard.svg',
        imageSelected: 'creditcard-selected.svg',
        template: '_cc',
      },
      {
        name: 'Alipay',
        hasCurrencyOptions: false,
        title: 'Alipay',
        titleTag: 'h4',
        id: 'alipayCollapse',
        image: 'alipay.svg',
        imageSelected: 'alipay-selected.svg',
        template: '_thirdparty',
      },
      {
        name: 'Agent account',
        hasCurrencyOptions: false,
        title: 'Agent account',
        titleTag: 'h4',
        id: 'agentAccuntCollapse',
        image: 'agent-account.svg',
        imageSelected: 'agent-account-selected.svg',
        template: '_thirdparty',
      },
      {
        name: 'On Hold',
        hasCurrencyOptions: false,
        title: 'On hold',
        titleTag: 'h4',
        id: 'olnHoldCollapse',
        image: 'on-hold.svg',
        imageSelected: 'on-hold-selected.svg',
        template: '_thirdparty',
      },
    ],
  },
  CreditCardOptions: [{
      label: 'Visa',
      id: 'ccTypeVisa',
      name: 'creditCardType',
      imageURL: 'payment-methods/credit-cards/visa.svg',
      imageAlt: 'Visa payment',
    },
    {
      label: 'Mastercard',
      id: 'ccTypeMasterCard',
      name: 'creditCardType',
      imageURL: 'payment-methods/credit-cards/mastercard.svg',
      imageAlt: '',
    },
    {
      label: 'American express',
      id: 'ccTypeAmerican',
      name: 'creditCardType',
      imageURL: 'payment-methods/credit-cards/american-express.svg',
      imageAlt: '',
    },
    {
      label: 'JCB',
      id: 'ccTypeJCB',
      name: 'creditCardType',
      imageURL: 'payment-methods/credit-cards/JCB.svg',
      imageAlt: '',
    },
  ],
  CreditCardFormData: {
    formGroups: [{
      elements: [{
        elementsGroup: [{
            type: 'inputForm',
            element: {
              label: 'Card number',
            },
          },
          {
            type: 'inputForm',
            element: {
              label: 'Card holder name',
            },
          },
          {
            type: 'selectForm',
            element: {
              label: 'Expiry date',
              id: 'ccExpiryDate',
              options: [{
                  name: 'MM',
                  id: 'ccExpiryDateMM',
                },
                {
                  name: 'YYYY',
                  id: 'ccExpiryDateYYYY',
                },
              ],
            },
          },
          {
            type: 'inputForm',
            element: {
              label: 'CW2 / CV2',
              tooltip: 'Tooltip text goes here',
            },
          },
        ],
      }, ],
    }, ],
  },
  sitemapIBE: [{
      section: 'Booking flow',
      available: true,
      pages: [{
          label: 'Select flight',
          url: 'BF-select_flight.html',
          notAvailable: false,
        },
        {
          label: 'Select flight (one way)',
          url: 'BF-select_flight_OW.html',
          notAvailable: false,
        },
        {
          label: 'Select flight (multicity)',
          url: 'BF-select_flight_multicity.html',
          notAvailable: false,
        },
        {
          label: 'Passenger data',
          url: 'BF-passenger_data.html',
          notAvailable: false,
        },
        {
          label: 'Add extras',
          url: 'BF-services_add.html',
          notAvailable: false,
        },
        {
          label: 'Add extras (one way)',
          url: 'BF-services_add_oneway.html',
          notAvailable: false,
        },
        {
          label: 'Seat selection',
          url: 'BF-seat_selection.html',
          notAvailable: false,
        },
        {
          label: 'Payment',
          url: 'BF-payment.html',
          notAvailable: false,
        },
        {
          label: 'Payment confirmation',
          url: 'BF-payment_confirmation.html',
          notAvailable: false,
        },
        {
          label: 'Payment confirmation (one way)',
          url: 'BF-payment_confirmation_OW.html',
          notAvailable: false,
        },
        {
          label: 'Payment confirmation (QR)',
          url: 'BF-payment_confirmation_QR.html',
          notAvailable: false,
        },
      ],
    },
    {
      section: 'Manage my Booking',
      available: true,
      pages: [{
          label: 'Login',
          url: 'BF-MMB_login.html',
          notAvailable: false,
        },
        {
          label: 'Home',
          url: 'BF-MMB_home.html',
          notAvailable: false,
        },
        {
          label: 'Cancel Flight',
          url: 'BF-MMB_cancel_flight.html',
          notAvailable: false,
        },
        {
          label: 'Change Flight',
          url: 'BF-MMB_change_flight.html',
          notAvailable: false,
        },
        {
          label: 'Guest details',
          url: 'BF-MMB_passenger_data.html',
          notAvailable: false,
        },
      ],
    },
    {
      section: 'Check in',
      available: true,
      pages: [{
          label: 'Login',
          url: 'BF-WCI_login.html',
          notAvailable: false,
        },
        {
          label: 'Home',
          url: 'BF-WCI_home.html',
          notAvailable: false,
        },
        {
          label: 'Pre confirmation',
          url: 'BF-WCI_pre-confirmation.html',
          notAvailable: false,
        },
      ],
    },
    {
      section: 'Agent cabinet',
      available: true,
      pages: [{
          label: 'Login',
          url: 'AC_login.html',
          notAvailable: false,
        },
        {
          label: 'Home',
          url: 'AC_home.html',
          notAvailable: false,
        },
        {
          label: 'Account management',
          url: 'AC_account_management.html',
          notAvailable: false,
        },
        {
          label: 'MMB',
          url: 'AC_mmb.html',
          notAvailable: false,
        },
        {
          label: 'Refunds',
          url: 'AC_refunds.html',
          notAvailable: false,
        },
        {
          label: 'Payment',
          url: 'AC-payment.html',
          notAvailable: false,
        },
        {
          label: 'Payment hold proceed',
          url: 'AC_payment_hold_proceed.html',
          notAvailable: false,
        },
      ],
    },
    {
      section: 'U-fly',
      available: true,
      pages: [{
          label: 'Login',
          url: 'U-FLY_login.html',
          notAvailable: false,
        },
        {
          label: 'Forgot password',
          url: 'U-FLY_forgot_password.html',
          notAvailable: false,
        },
        {
          label: 'Sign up',
          url: 'U-FLY_sign_up.html',
          notAvailable: false,
        },
        {
          label: 'Confirmation',
          url: 'U-FLY_confirmation.html',
          notAvailable: false,
        },
        {
          label: 'Profile update',
          url: 'U-FLY_profile_update.html',
          notAvailable: false,
        },
      ],
    },
    {
      section: 'Error',
      available: true,
      pages: [{
        label: 'Error 404',
        url: '404.html',
        notAvailable: false,
      }, ],
    },
  ],
  sitemapCorp: [{
      section: 'Pages',
      available: true,
      pages: [{
          label: 'Home',
          url: 'CORP-index.html',
          notAvailable: false,
        },
        {
          label: 'Sign up',
          url: 'CORP-sign_up.html',
          notAvailable: false,
        },
        {
          label: 'Sign in',
          url: 'CORP-sign_in.html',
          notAvailable: false,
        },
        {
          label: 'Article with image list',
          url: 'CORP-article_with_img_list.html',
          notAvailable: false,
        },
        {
          label: 'Article with table and note',
          url: 'CORP-article_with_table_note.html',
          notAvailable: false,
        },
        {
          label: 'Article with complex table',
          url: 'CORP-article_with_complex_table.html',
          notAvailable: false,
        },
        {
          label: 'Article with anchors',
          url: 'CORP-fast_navigation_anchors.html',
          notAvailable: false,
        },
        {
          label: 'Destinations guide',
          url: 'CORP-destination-guides.html',
          notAvailable: false,
        },
        {
          label: 'Destination guide (city)',
          url: 'CORP-destination_guides_city.html',
          notAvailable: false,
        },
        {
          label: 'News list',
          url: 'CORP-news_list.html',
          notAvailable: false,
        },
        {
          label: 'News detail',
          url: 'CORP-news_detail.html',
          notAvailable: false,
        },
      ],
    },
    {
      section: 'Landings',
      available: true,
      pages: [{
        label: 'Landing page 01',
        url: 'CORP-LANDING-01.html',
        notAvailable: false,
      }, ],
    },
    {
      section: 'Components (HTML)',
      available: true,
      pages: [{
          label: 'Header (home)',
          url: 'COMP-CORP-header-home.html',
          notAvailable: false,
        },
        {
          label: 'Header (inner pages)',
          url: 'COMP-CORP-header-page.html',
          notAvailable: false,
        },
        {
          label: 'Main banner',
          url: 'COMP-CORP-main_banner.html',
          notAvailable: false,
        },
        {
          label: 'Multiple panel',
          url: 'COMP-CORP-multiple-panel.html',
          notAvailable: false,
        },
        {
          label: 'Destinations offers',
          url: 'COMP-CORP-destinations-offers.html',
          notAvailable: false,
        },
        {
          label: 'Main offers',
          url: 'COMP-CORP-main-offers.html',
          notAvailable: false,
        },
        {
          label: 'Footer',
          url: 'COMP-CORP-main_footer.html',
          notAvailable: false,
        },
        {
          label: 'Sign in',
          url: 'COMP-CORP-sign-in.html',
          notAvailable: false,
        },
      ],
    },
    {
      section: 'UI-Elements (HTML)',
      available: true,
      pages: [{
          label: 'Accordion',
          url: 'COMP-CORP-accordion.html',
          notAvailable: false,
        },
        {
          label: 'Headings',
          url: 'COMP-CORP-headings.html',
          notAvailable: false,
        },
        {
          label: 'Paragraphs',
          url: 'COMP-CORP-paragraphs.html',
          notAvailable: false,
        },
        {
          label: 'Tables',
          url: 'COMP-CORP-tables.html',
          notAvailable: false,
        },
        {
          label: 'Lists',
          url: 'COMP-CORP-lists.html',
          notAvailable: false,
        },
        {
          label: 'Images',
          url: 'COMP-CORP-image.html',
          notAvailable: false,
        },
        {
          label: 'Pagination',
          url: 'COMP-CORP-pagination.html',
          notAvailable: false,
        },
      ],
    },
  ],
  sitemapMD: [{
      section: 'Components (HTML)',
      available: true,
      pages: [{
          label: 'Flight search',
          url: 'COMP-flight_search.html',
          notAvailable: false,
        },
        {
          label: 'Booking steps',
          url: 'COMP-booking_steps.html',
          notAvailable: false,
        },
        {
          label: 'Summary',
          url: 'COMP-summary.html',
          notAvailable: false,
        },
        {
          label: 'Footer (ibe)',
          url: 'COMP-footer.html',
          notAvailable: false,
        },
        {
          label: 'Payment details table',
          url: 'COMP-payment-detail-tables.html',
          notAvailable: false,
        },
      ],
    },
    {
      section: 'UI-Elements (HTML)',
      available: true,
      pages: [{
          label: 'Grid',
          url: 'COMP-grid.html',
          notAvailable: false,
        },
        {
          label: 'Colors',
          url: 'COMP-colors.html',
          notAvailable: true,
        },
        {
          label: 'Font icons',
          url: '../src/assets/fonts/icons/demo.html',
          notAvailable: false,
        },
        {
          label: 'Headings (h1,h2,h3...)',
          url: 'COMP-headings.html',
          notAvailable: false,
        },
        {
          label: 'Buttons',
          url: 'COMP-buttons.html',
          notAvailable: false,
        },
        {
          label: 'Form elements',
          url: 'COMP-form_elements.html',
          notAvailable: false,
        },
        {
          label: 'Modals',
          url: 'COMP-modals.html',
          notAvailable: false,
        },
      ],
    },
  ],
  searchFlight: {
    routes: {
      journeys: [
        {
          caption: 'Choose an origin',
          way: 'departure',
          inputForm: {
            groupId: 'searchOutbound',
            placeholder: 'Origin',
            label: 'From',
            id: 'searchOutbound_input',
            value: 'HKE',
            valueSmall: 'Hong Kong',
            idCollapse: 'searchOutbound_options',
          },
        },
        {
          caption: 'Choose a destination',
          way: 'arrival',
          inputForm: {
            groupId: 'searchReturn',
            placeholder: 'Destination',
            label: 'To',
            id: 'searchReturn_input',
            value: 'ZUM',
            valueSmall: 'Zumming',
            idCollapse: 'searchReturn_options',
          },
        },
      ],
      multicityJourneys: [{
          way: 'departure',
          inputForm: {
            groupId: 'searchOutbound-mc',
            placeholder: 'Origen',
            label: 'From',
            id: 'searchOutbound-mc_input',
            value: 'ZUM',
            valueSmall: 'Zumming',
            idCollapse: 'searchOutbound-mc_options',
          },
        },
        {
          way: 'arrival',
          inputForm: {
            groupId: 'searchReturn-mc',
            placeholder: 'Destino',
            label: 'To',
            id: 'searchReturn-mc_input',
            value: 'SIM',
            valueSmall: 'Siem Reap',
            idCollapse: 'searchReturn-mc_options',
          },
        },
      ],
    },
    dates: {
      journeys: [{
          way: 'departure',
          title: 'Choose your departure date',
          inputForm: {
            groupId: 'searchDatesOutbound',
            placeholder: 'Dates',
            label: 'Departure date',
            id: 'searchDatesOutbound_input',
            value: '16 Jan',
            valueSmall: 'Thursday',
            valueDate: {
              day: '27',
              month: 'Sep',
              week: 'Wednesday',
            },
            idCollapse: 'searchDatesOutbound_options',
          },
        },
        {
          way: 'arrival',
          title: 'Choose your return date',
          inputForm: {
            groupId: 'searchDatesInbound',
            placeholder: '',
            label: 'Return date',
            id: 'searchDatesInbound_input',
            value: '24 Jan',
            valueSmall: 'Monday',
            valueDate: {
              day: '7',
              month: 'Oct',
              week: 'Monday',
            },
            idCollapse: 'searchDatesInbound_options',
          },
        },
      ],
    },
    triptype: {
      pos2: true,
      style: 'dropdown',
      formControl: {
        label: '',
        id: 'triptypeSelection',
        options: [{
          name: 'Round trip',
          id: '',
        }, ],
      },
    },
    typology: {
      show: true,
      template: 'dropdown',  // styles: dropdown, search-collapsable
      caption: 'Passengers type selection',
      hasLabel: false,
      label: '',
      value: '2 adults, 1 child',
      buttonText: 'Done',
      options: [{
          label: 'Adults',
          labelInfo: '(> 12 years)',
          numericUpdown: {
            id: 'paxAdult',
            value: '2',
          },
        },
        {
          label: 'Children',
          labelInfo: '(2-11 years)',
          numericUpdown: {
            id: 'paxChildren',
            value: '0',
          },
        },
        {
          label: 'Infants',
          labelInfo: '(<2 years)',
          numericUpdown: {
            id: 'paxInfants',
            value: '0',
          },
          tooltip: 'Please note, by selecting the below you must seat your infant in your lap. Should you choose to purchase a seat for your infant, please contact our call center at (852) 3902 0288.',
        },
      ],
    },
    currency: {
      show: true,
      formControl: {
        hasLabel: false,
        label: 'Currency',
        id: 'currencyId',
        options: [{
          name: 'HKE',
          id: '',
        }, ],
      },
    },
    promocode: {
      show: true,
      linkText: '+ Promocode',
      formControl: {
        hasLabel: false,
        label: 'Promocode',
        placeholder: 'Type your promocode',
        name: 'promocode',
        id: 'promocode',
      },
    },
    searchButton: {
      url: 'BF-select_flight.html',
      text: 'Buscar',
    },
  },
  multiplePanel: {
    responsive: {
      collapsable: true
    },
    navItems: [{
        ariaControl: 'multiplePanelTab1_tab',
        id: 'multiplePanelTab1',
        text: 'Book a trip',
        panel: {
          show: true,
          button: {
            icon: 'airplane-flight',
            text: 'Book a trip',
          },
          contentInclude: 'partials/corporative/_multiple-search.njk',
        },
      },
      {
        url: '#',
        ariaControl: 'multiplePanelTab2_tab',
        id: 'multiplePanelTab2',
        tabindex: -1,
        text: 'Check in',
        panel: {
          show: false,
          button: {
            icon: 'computer-check',
            text: 'Check in',
          },
          contentInclude: '',
        },
      },
      {
        url: '#',
        ariaControl: 'multiplePanelTab3_tab',
        id: 'multiplePanelTab3',
        tabindex: -1,
        text: 'Manage my booking',
        panel: {
          show: false,
          button: {
            icon: 'settings-single',
            text: 'Manage my booking',
          },
          contentInclude: '',
        },
      },
    ],
  },
  multipleSearch: {
    show: true,
    active: 1,
    items: [{
        link: '',
        label: 'Search for flights',
        icon: 'icon--flights',
        controls: 'multipleSearchFlights_tab',
        id: 'multipleSearchFlights',
        tabindex: -1,
        contentInclude: 'partials/components/IBE/flight-search/_flight-search.njk',
      },
      {
        link: {
          url: 'https://www.agoda.com/hkexpress',
          title: 'Open in new window',
          target: '_blank',
        },
        label: 'Search for Hotels',
        icon: 'icon--hotels',
        controls: 'multipleSearchHotels_tab',
        id: 'multipleSearchHotels',
        tabindex: -1,
      },
      {
        link: {
          url: 'https://www.uflyholidays.com/en/',
          title: '',
        },
        label: 'Search for Flights and Hotels',
        icon: 'icon--flightshotels',
        controls: 'multipleSearchHotelsFlights_tab',
        id: 'multipleSearchHotelsFlights',
        tabindex: -1,
      },
    ],
  },
  mainHeader: {
    navSecondary: {
      links: [{
          linkIcon: 'user',
          url: '#',
          dataType: 'user',
        },
        {
          linkIcon: 'language',
          url: '#',
          dataType: 'languages',
          dropdownList: [{
              title: '香港特別行政區 (繁體)',
              url: '#',
            },
            {
              title: '中国大陆 (简体)',
              url: '#',
            },
            {
              title: '中國台灣 (繁體)',
              url: '#',
            },
            {
              title: '日本 (日本語)',
              url: '#',
            },
            {
              title: '한국(한국어)',
              url: '#',
            },
            {
              title: 'Cambodia (EN)',
              url: '#',
            },
            {
              title: 'Thailand (EN)',
              url: '#',
            },
            {
              title: 'Vietnam (EN)',
              url: '#',
            },
            {
              title: 'United States (EN)',
              url: '#',
            },
          ],
        },
      ],
    },
    navPrimary: [{
        text: 'Plan',
        submenus: [{
            items: [{
                title: 'Flight with us',
                url: '#',
              },
              {
                title: 'Safety',
                url: '#',
              },
              {
                title: 'Our Fleet',
                url: '#',
              },
              {
                title: 'Our loyalty program',
                url: '#',
              },
              {
                title: 'Group travel',
                url: '#',
              },
            ],
          },
          {
            items: [{
              title: 'Book',
              submenuList: [{
                  text: 'Booking via our website',
                  url: '#',
                },
                {
                  text: 'U-Fly pass',
                  url: '#',
                },
                {
                  text: 'Baggage',
                  url: '#',
                },
                {
                  text: 'U-First',
                  url: '#',
                },
                {
                  text: 'U-connect',
                  url: '#',
                },
                {
                  text: 'Other fees',
                  url: '#',
                },
                {
                  text: 'Fuel Surcharge',
                  url: '#',
                },
                {
                  text: 'Government imposed fees',
                  url: '#',
                },
              ],
            }, ],
          },
          {
            items: [{
              title: 'Travel information',
              submenuList: [{
                  text: 'Baggage guide',
                  submenuListN4: [{
                      text: 'Checked Baggage',
                      url: '#',
                    },
                    {
                      text: 'Carry on Baggage',
                      url: '#',
                    },
                    {
                      text: 'Oversized baggage / sports equipment',
                      url: '#',
                    },
                    {
                      text: 'Smart baggage',
                      url: '#',
                    },
                  ],
                },
                {
                  text: 'Inflight entertainment',
                  url: '#',
                },
                {
                  text: 'Special Assistance',
                  url: '#',
                },
                {
                  text: 'Mainland connection',
                  url: '#',
                },
                {
                  text: 'Travel documents',
                  url: '#',
                },
              ],
            }, ],
          },
          {
            items: [{
              title: 'Special offers',
              submenuList: [{
                  text: 'Inflight specials',
                  url: '#',
                },
                {
                  text: 'Duty Free',
                  url: '#',
                },
                {
                  text: 'Travel Insurance',
                  url: '#',
                },
                {
                  text: 'Activities',
                  url: '#',
                },
                {
                  text: 'Car rentals',
                  url: '#',
                },
                {
                  text: 'Hotel offers',
                  url: '#',
                },
                {
                  text: 'Package offers',
                  url: '#',
                },
              ],
            }, ],
          },
        ],
      },
      {
        text: 'Your trips',
        submenus: [{
            items: [{
                title: 'Manage your booking',
                url: '#',
              },
              {
                title: 'Check-in',
                submenuList: [{
                    text: 'Online Check-in',
                    url: '#',
                  },
                  {
                    text: 'Airport Check-in',
                    url: '#',
                  },
                  {
                    text: 'Intown Check-in at Hong Kong',
                    url: '#',
                  },
                  {
                    text: 'Check-in at PRD / Macau',
                    url: '#',
                  },
                ],
              },
            ],
          },
          {
            items: [{
              title: 'Important travel notice',
              url: '#',
            }, ],
          },
        ],
      },
      {
        text: 'Explore',
        url: '#',
        submenus: [{
            items: [{
              title: 'Route map',
              url: '#',
            }, ],
          },
          {
            isLargeColumn: true,
            items: [{
              title: 'Destinations',
              hasColumns: true,
              submenuList: [
                [{
                    text: 'Hong Kong SAR',
                    submenuListN4: [{
                      text: 'Hong Kong',
                      url: '#',
                    }, ],
                  },
                  {
                    text: 'Korea',
                    submenuListN4: [{
                        text: 'Seoul',
                        url: '#',
                      },
                      {
                        text: 'Busan',
                        url: '#',
                      },
                      {
                        text: 'Jeju',
                        url: '#',
                      },
                    ],
                  },
                ],
                [{
                    text: 'U.S. Territories',
                    submenuListN4: [{
                      text: 'Saipan',
                      url: '#',
                    }, ],
                  },
                  {
                    text: 'Japan',
                    submenuListN4: [{
                        text: 'Tokyo',
                        url: '#',
                      },
                      {
                        text: 'Osaka',
                        url: '#',
                      },
                      {
                        text: 'Nagoya',
                        url: '#',
                      },
                      {
                        text: 'Hiroshima',
                        url: '#',
                      },
                      {
                        text: 'Takamatsu',
                        url: '#',
                      },
                      {
                        text: 'Fukuoka',
                        url: '#',
                      },
                      {
                        text: 'Kagoshima',
                        url: '#',
                      },
                      {
                        text: 'Kumamoto',
                        url: '#',
                      },
                      {
                        text: 'Ishigaki',
                        url: '#',
                      },
                    ],
                  },
                ],
                [{
                    text: 'Cambodia',
                    submenuListN4: [{
                      text: 'Siem Reap',
                      url: '#',
                    }, ],
                  },
                  {
                    text: 'China',
                    submenuListN4: [{
                        text: 'Kunming',
                        url: '#',
                      },
                      {
                        text: 'Ningbo',
                        url: '#',
                      },
                    ],
                  },
                ],
                [{
                    text: 'Thailand',
                    submenuListN4: [{
                        text: 'Chiang Mai',
                        url: '#',
                      },
                      {
                        text: 'Chiang Rai',
                        url: '#',
                      },
                      {
                        text: 'Phuket',
                        url: '#',
                      },
                    ],
                  },
                  {
                    text: 'Vietnam',
                    submenuListN4: [{
                        text: 'Da Nang',
                        url: '#',
                      },
                      {
                        text: 'Nha Trang',
                        url: '#',
                      },
                    ],
                  },
                ],
                [{
                  text: 'Taiwan',
                  submenuListN4: [{
                      text: 'Taichung',
                      url: '#',
                    },
                    {
                      text: 'Hualien',
                      url: '#',
                    },
                  ],
                }, ],
              ],
            }, ],
          },
        ],
      },
    ],
  },
  mainFooter: {
    logo: {
      show: true,
    },
    flyPrograms: {
      show: true,
    },
    newsletter: {
      show: true,
    },
    bottom: {
      copyright: {
        show: true,
      },
    },
    socialmedia: {
      show: true,
    },
    nav: {
      show: true,
      isCollapsible: false,
      items: [{
          title: 'Acerca de nosotros',
          subItems: [{
              caption: 'Quiénes somos',
            },
            {
              caption: 'Prensa',
            },
            {
              caption: 'Flota',
            },
            {
              caption: 'Destinos',
            },
            {
              caption: 'Revista',
            },
            {
              caption: 'Contacto',
            },
            {
              caption: 'Empleo',
            },
            {
              caption: 'Mapa del sítio',
            },
          ],
        },
        {
          title: 'Información legal',
          subItems: [{
              caption: 'Contrato de transporte',
            },
            {
              caption: 'Términos y condiciones',
            },
            {
              caption: 'Política de privacidad',
            },
            {
              caption: 'Política de cookies',
            },
          ],
        },
      ],
    },
  },
  socialmedias: {
    title: '',
    hasFlatColors: true,
    items: [{
        title: 'facebook',
        name: 'facebook',
        url: '#',
      },
      {
        title: 'instagram',
        name: 'instagram',
        url: '#',
      },
      {
        title: 'weibo',
        name: 'weibo',
        url: '#',
      },
      {
        title: 'wechat',
        name: 'wechat',
        url: '#',
      },
    ],
  },
  languageBar: {
    label: 'Language:',
    selectedText: '香港特別行政區 (繁體)',
    id: 'collapseLanguage',
    items: [{
        title: '香港特別行政區 (繁體)',
        url: '',
      },
      {
        title: '中国大陆 (简体)',
        url: '',
      },
      {
        title: '中國台灣 (繁體)',
        url: '',
      },
      {
        title: '日本 (日本語)',
        url: '',
      },
      {
        title: '한국(한국어)',
        url: '',
      },
      {
        title: 'Cambodia (EN)',
        url: '',
      },
    ],
  },
  currencyBar: {
    label: 'Currency:',
    selectedText: 'HKD',
    id: 'collapseCurrency',
    items: [{
        title: 'Hong Kong Dollar (HKD)',
        url: '',
      },
      {
        title: 'UAE Dirham (AED)',
        url: '',
      },
      {
        title: 'Argentinean Peso (ARS)',
        url: '',
      },
    ],
  },
  copyright: {
    text: 'Copyright &copy; 2018 Hong Kong Express Airways Limited.',
  },
  mainOffers: {
    gridCols: 'grid-col col-12 col-xs-6 col-md-3',
    offers: [{
        isOfferDeal: true,
        image: 'offers/discount-activity-tickets.jpg',
        imageBrandLight: 'agoda_logo-white.svg',
        imageBrand: 'agoda_logo-color.svg',
        caption: 'Selected hotels up to 75% off. Book now!',
      },
      {
        isOfferDeal: true,
        image: 'offers/discount-activity-tickets.jpg',
        imageBrandLight: 'zurich_logo-white.svg',
        imageBrand: 'zurich_logo-color.svg',
        caption: 'Stay protected with travel insurance.',
      },
      {
        isOfferDeal: true,
        image: 'offers/discount-activity-tickets.jpg',
        imageBrandLight: 'rentalcars-white.svg',
        imageBrand: 'rentalcars-color.svg',
        caption: 'Earn reward-U points with our loyalty program',
      },
      {
        isOfferDeal: true,
        image: 'offers/discount-activity-tickets.jpg',
        imageBrandLight: 'klook_logo-white.svg',
        imageBrand: 'klook_logo-color.svg',
        caption: 'Up to 50% Discount Activity tickets',
      },
    ],
  },
  destinationOffersPrice: {
    title: 'Offers from',
    selector: 'Hong Kong',
    selectorOptions: [{
        name: 'Hiroshima',
      },
      {
        name: 'Kumming',
      },
      {
        name: 'Siem Reap',
      },
      {
        name: 'Tokio',
      },
    ],
    destinationOptionTitle: 'Destinations offers',
    showAllOffers: true,
    destinationOptions: {
      items: [{
          labelFrom: 'From',
          text: '',
          country: 'Siem Reap',
          price: '328',
          promoPrice: '',
          link: {
            url: '#',
            title: 'Obtenga los mejores vuelos',
            label: '',
          },
        },
        {
          labelFrom: 'From',
          text: '',
          country: 'Siem Reap',
          price: '328',
          promoPrice: '',
          link: {
            url: '#',
            title: 'Obtenga los mejores vuelos',
            label: '',
          },
        },
        {
          labelFrom: 'From',
          text: '',
          country: 'Siem Reap',
          price: '328',
          promoPrice: '',
          link: {
            url: '#',
            title: 'Obtenga los mejores vuelos',
            label: '',
          },
        },
      ],
    },
  },
  destinationOffers: {
    main: {
      items: [{
        isMain: true,
        city: 'Kunming',
        price: 328,
        img: 'kumming',
      }, ],
    },
    aside: {
      items: [{
        isMain: false,
        city: 'Nha Trang',
        price: 328,
        img: 'hiroshima',
      }, ],
    },
    allOffersLink: {
      caption: 'See more offers',
      href: '#',
    },
  },
  destinations: {
    items: [{
        countryName: 'Hong Kong',
        cities: [{
          name: 'Hong Kong, Hong Kong',
          airportcode: 'HKG',
        }, ],
      },
      {
        countryName: 'Mainland China',
        cities: [{
            name: 'Kunming, Changshui',
            airportcode: 'KMG',
          },
          {
            name: 'Ningbo, Ningbo Lishe',
            airportcode: 'NGB',
          },
          {
            name: 'Zhangjiajie, Hehua',
            airportcode: 'DYG',
          },
        ],
      },
      {
        countryName: 'Cambodia',
        cities: [{
          name: 'Siem Reap, Siem Reap',
          airportcode: 'REP',
        }, ],
      },
      {
        countryName: 'Japan',
        cities: [{
            name: 'Fukuoka, Fukuoka',
            airportcode: 'FUK',
          },
          {
            name: 'Hiroshima, Hiroshima',
            airportcode: 'HIJ',
          },
          {
            name: 'Ishigaki, Ishigaki',
            airportcode: 'ISG',
          },
          {
            name: 'Kagoshima,Kagoshima',
            airportcode: 'KOJ',
          },
          {
            name: 'Kumamoto, Aso',
            airportcode: 'KMJ',
          },
          {
            name: 'Nagoya, Chubu',
            airportcode: 'NGO',
          },
          {
            name: 'Osaka, Kansai',
            airportcode: 'KIX',
          },
          {
            name: 'Takamatsu, Takamatsu',
            airportcode: 'TAK',
          },
          {
            name: 'Tokyo, Tokyo (All Airports)',
            airportcode: 'TYO',
          },
          {
            name: 'Tokyo, Haneda',
            airportcode: 'HND',
          },
          {
            name: 'Tokyo, Narita',
            airportcode: 'NRT',
          },
        ],
      },
      {
        countryName: 'Korea',
        cities: [{
            name: 'Busan, Gimhae',
            airportcode: 'PUS',
          },
          {
            name: 'Jeju, Jeju',
            airportcode: 'CJU',
          },
          {
            name: 'Seoul, Incheon',
            airportcode: 'ICN',
          },
        ],
      },
      {
        countryName: 'Taiwan',
        cities: [{
            name: 'Taichung, Ching-Chuan-Kang',
            airportcode: 'RMQ',
          },
          {
            name: 'Hualien, Hualien',
            airportcode: 'HUN',
          },
        ],
      },
      {
        countryName: 'Thailand',
        cities: [{
            name: 'Chiang Mai, Chiang Mai',
            airportcode: 'CNX',
          },
          {
            name: 'Chiang Rai, Chiang Rai',
            airportcode: 'CEI',
          },
          {
            name: 'Phuket, Phuket',
            airportcode: 'HKT',
          },
        ],
      },
      {
        countryName: 'U.S. Territories',
        cities: [{
          name: 'aipan, Saipan',
          airportcode: 'SPN',
        }, ],
      },
      {
        countryName: 'Vietnam',
        cities: [{
            name: 'Da Nang, Da Nang',
            airportcode: 'DAD',
          },
          {
            name: 'Nha Trang, Cam Ranh',
            airportcode: 'CXR',
          },
        ],
      },
    ],
  },
  newsletter: {
    title: '',
    intro: '',
    buttonText: 'Send',
    canBeClosed: false,
    closeLabel: '',
    form: {
      label: 'Subscribe to our newsletter',
      placeholder: 'E-mail',
      name: 'newsletterInput',
      id: 'newsletterInput',
    },
  },
}
$(document).ready(function () {
    var W = $(window).outerWidth();
    var body = $('body');
    var stickyMainHeader, stickyCombinedBar;

    // imports
    // ----------------------------------------------------
    //## SEARCH 
    // ----------------------------------------------------
    searchInteraction(false, 640, -180);
    stickyMainHeader = 60; // add class is-fixedheader main header
    navResponsiveBottom();
    weekSelector(W, 640)
    faresSelectionCollapse();
    tripSelector();
    summaryCollapse();
    overlaySummaryClose();
    notification(2600, 540);
    compareFaresTrigger();
    connectionsModal();

    // search and header fixed
    stickyCombinedBar = mainHeaderSearchFixed(stickyMainHeader);
    $(window).scroll(function () {
        // fixed combined bar (summary) on scroll
        searchFixed(stickyCombinedBar);
        // fixed main header on scroll
        headerFixed(stickyMainHeader);
    });

    // ## toggle footer nav on mobile
    function footerNavCollapse() {
        var W = $(window).width();
        if (W <= 480) {
            $(".main-footer_nav nav ul").hide();
            $('.main-footer_nav_title a').on('click', function() {
                $(this).attr('aria-expanded') == 'true' ?  $(this).attr('aria-expanded', false) : $(this).attr('aria-expanded', true);
                $(this).removeClass('collapsed');
                $(this).parent().siblings().css({'max-height':'inherit', 'overflow-y': 'auto', 'transition': 'inherit'});
                $(this).parent().siblings().slideToggle();
            });
        } else {
            $(".main-footer_nav nav ul").show();
            $('.main-footer_nav_title a').attr('aria-expanded', true).addClass('collapsed');
        }
    }
    footerNavCollapse();
});
